<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class ZabbixStatWorkerCommand extends WorkerCommand {

    public $tube = 'zabbixStat';

    private $_workstations = array();
    private $_connection;

    private $_hostIds = array();

    const TIME_REQUEST = 30;



    public function init() {
        parent::init();
        $this->_workstations = Device::model()->with(array('params'))->findAll('type=:type AND server_api="zabbix"', array(':type' => DeviceType::WORKSTATION));
        $this->_connection = Yii::app()->zabbix;
    }

    public function getWorkerId() {
        return 3;
    }

    public function run($args) {
        while($this->runtimeChecks())
        {
            $start = time();
            $this->work($args);

            $used = time() - $start;
            $this->report("Used: {$used} sec");

            $wait = static::TIME_REQUEST - $used;
            if ($wait > 0) {
                $this->report("wait {$wait} seconds...");
                sleep($wait);
                $this->report("return to job ");
            }
        }
    }

    public function getHostId($name) {
        if( !isset($this->_hostIds[$name]) ) {
            $this->_hostIds[$name] = $this->_connection->createCommand("SELECT hostid FROM hosts WHERE host=:hostname")->queryScalar(array(':hostname' => $name));
        }

        return $this->_hostIds[$name];
    }

    public function work($args)
    {

        foreach ( $this->_workstations as $w )
        {

//            echo "host: $w->id ({$w->alias})\n";

            if( $hostId = $this->getHostId($w->alias) ) {

                $select = "SELECT
                    itemid, key_,
                    (select `value` from history h1 WHERE h1.itemid = i.itemid order by h1.clock DESC LIMIT 1)  as his,
                    (select `value` from history_uint hu1 WHERE hu1.itemid = i.itemid order by hu1.clock DESC LIMIT 1 ) as his_uint

                    FROM `items` i WHERE hostid = :hostId AND i.key_ IN('vm.memory.size[free]', 'system.cpu.load[percpu,avg1]');";

                if ( $params = Yii::app()->zabbix->createCommand($select)->queryAll(true, array(':hostId' => $hostId)) )
                {
//                    echo "params found!\n";

                    foreach($params as $param)
                    {
//                        echo "param: {$param['key_']}: {$param['his']} ({$param['his_uint']})\n";

                        $name = '';
                        $value = '';

                        if( $param['key_'] == 'vm.memory.size[free]' )
                        {
                            $name = "summary.quickStats.guestMemoryUsage";
                            $all = $w->getParam('summary.config.memorySize')->value;
                            $value = DiskInfoHelper::getPercent($all, $param['his_uint']);

                        } elseif( $param['key_'] == 'system.cpu.load[percpu,avg1]' )
                        {
                            $name = "summary.quickStats.overallCpuUsage";
                            $value = $param['his']*100;
                        }
                        DeviceParamStat::saveResult($w->id, $name, $value);
                    }
                }
            }

            $params = array(
                "" => true,
                "" => true,
            );

            $params = array(
                "system.uname" => 'summary.config.guestFullName',
                "system.uptime" => 'summary.quickStats.uptimeSeconds',
                "vm.memory.size[free]" => 'summary.config.memoryFree',
                "system.cpu.load[percpu,avg1]" => 'summary.runtime.maxCpuUsage'
            );
        }

        return true;
    }
}