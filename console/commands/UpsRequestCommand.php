<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class UpsRequestCommand extends LockedConsoleCommand {

    public function doWork($args) {
        $start = time();

        foreach(Device::model()->with(array('params'))->findAll('type=:type AND server_api="nut"', array(':type' => DeviceType::UPS) ) as $ups)
        {

            echo "id: " . $ups->id . ", alias: " . $ups->alias . ":\n";

            $output = shell_exec("upsc {$ups->alias}@{$ups->ip}");

            $lines = explode("\n", $output);
            foreach( $lines as $line)
            {
                if( strlen($line) ) {
                    $pairs = explode(": ", $line);
                    $key = $pairs[0];
                    $value = $pairs[1];
                    DeviceParam::updateInfoByModel($ups, $key, trim($value));
                }
            }

            echo "\n";
        }

        echo time() - $start;
    }


} 