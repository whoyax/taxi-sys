<?php

/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */
class PoeRequestCommand extends CConsoleCommand {


    protected $ip;

    public function run($args)
    {
        $dlink = Device::model()->findByPk(58);
        $client = new DlinkClient($dlink);

        if ( $settings = $client->getPoeInfo() )
        {
            $params = array(
                4 => 'power',
                5 => 'voltage',
                6 => 'current',
                8 => 'status',
            );

            foreach ( $settings as $port )
            {
                foreach ( $params as $key => $param )
                {
                    DeviceParam::updateInfoByModel($dlink, "ports.{$port[0]}.{$param}", $port[$key]);
                }
            }
        }

        if ( $info = $client->getDeviceInfo() )
        {
            foreach ( $info as $param => $value )
            {
                DeviceParam::updateInfoByModel($dlink, $param, $value);
            }
        }

    }


}