<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class PingWorkerCommand extends CConsoleCommand {

    public function run($args) {

        $client = Yii::app()->yiinstalk->getClient();

        print "Waiting for job...\n";

        while($job = $client
            ->watch('providerTest')
            ->ignore('default')
            ->reserve()) {

            $this->count++;

            print "Job!...#{$this->count}\n";

            var_dump($job);
            print "\n";


            sleep(3);

            $client->useTube('providerTest')->put('New internal job text');

            var_dump($job->getData());
            echo "\n";

            print "Deleting task...\n";

            $client->delete($job);

        }

    }
} 