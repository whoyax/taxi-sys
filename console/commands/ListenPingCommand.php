<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

use PEAR2\Net\RouterOS;

class ListenPingCommand extends CConsoleCommand {

    const MAX_COUNT = 1000;
    static $count = 0;

    public function run($args) {

        $mikrotik = Yii::app()->mikrotik2;


        $list = Yii::app()->db->createCommand(
            "SELECT DISTINCT ip FROM (SELECT DISTINCT ip FROM device UNION SELECT DISTINCT ip FROM device_ip ) u WHERE ip <> \"0.0.0.0\"")
            ->queryColumn();

        $monitorRequest = new RouterOS\Request('/ping');
        $monitorRequest->setArgument('interval', 5);


        $monitorListener = function ($response) {

            if(++self::$count > self::MAX_COUNT ) {
                echo "\n Max count detected. Shutdown.";
                exit(1);
            }

            if ($response->getType(RouterOS\Response::TYPE_DATA)) {

                // var_dump($response); exit;

                if( $response->getProperty('host') == "192.168.70.1" ) return;

                print $response->getProperty('host');
                print " | status:  ";
                print ($response->getProperty('status')) ;
                print " | received: ";
                print $response->getProperty('received');
                print " | sent: ";
                print $response->getProperty('sent');
                print " | loss: ";
                print $response->getProperty('packet-loss');
                print "\r\n";

                PingStatus::saveResult($response);

            }
        };

        $validator  = new IPValidator();

        foreach($list as $ip) {

            if( $ip && $ip !='0.0.0.0' && $validator->validateValue($ip)) {
                $monitorRequest
                    ->setArgument('address', $ip)
                    ->setTag('ping-'.$ip);

                $mikrotik->sendAsync(
                    $monitorRequest,
                    $monitorListener
                );
            }
        }

        $mikrotik->loop();

    }
} 