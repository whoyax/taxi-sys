<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class InitListsCommand extends CConsoleCommand {

    public function run($args) {
        foreach(Queue::model()->findAll() as $q) {
            if( $q->active
                && $q->activeProvider
                    && $q->getAddresses() )
            {
                if( !NetworkHelper::getAddressesByList($q->activeProvider->provider->alias)) {
                    NetworkHelper::manyToList($q->activeProvider->provider->alias, $q->getAddresses());
                }
            }
        }

    }
} 