<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

use PEAR2\Net\RouterOS;

class ListenBandwidthCommand extends CConsoleCommand {

    public function run($args) {

        $mikrotik = ($args[0] == 1) ? Yii::app()->mikrotik :  Yii::app()->mikrotik2;

        $interfaceListRequest = new RouterOS\Request('/interface print');

        $results = $mikrotik->sendSync($interfaceListRequest);
        $list = array();

        foreach($results as $result) {
            if ($result->getType() == RouterOS\Response::TYPE_DATA) {
                $list[] = ($result('name'));
            }
        }

        $monitorRequest = new RouterOS\Request('/interface/monitor-traffic .proplist=rx-bits-per-second,tx-bits-per-second,name');
        $monitorRequest->setArgument('interval', 5);


        $monitorListener = function ($response) {
            if ($response->getType(RouterOS\Response::TYPE_DATA)) {
                // $insertStatement[1]['tiimestamp'] = date('Y-m-d H:i:s');

                print $response->getProperty('name');
                print " / ";
                print $response->getProperty('rx-bits-per-second');
                print " / ";
                print $response->getProperty('tx-bits-per-second');
                print "\r\n";

                InterfaceStat::saveStat($response);
                // var_dump($response);

            }
        };

        foreach($list as $interface) {
            $monitorRequest
                ->setArgument('interface', $interface)
                ->setTag($interface);

            $mikrotik->sendAsync(
                $monitorRequest,
                $monitorListener
            );
        }

        $mikrotik->loop();

    }
} 