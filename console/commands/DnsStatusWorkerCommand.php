<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class DnsStatusWorkerCommand extends WorkerCommand {

    public $tube = 'dnsStatus';

    const TIME_REQUEST = 60;


    public function getWorkerId() {
        return 6;
    }

    public function run($args) {
        while($this->runtimeChecks())
        {
            $start = time();
            $this->work($args);

            $used = time() - $start;
            $this->report("Used: {$used} sec");

            $wait = static::TIME_REQUEST - $used;
            if ($wait > 0) {
                $this->report("wait {$wait} seconds...");
                sleep($wait);
                $this->report("return to job ");
            }
        }
    }


    public function work($args) {

        $dnsRecords = DnsSettings::model()->findAll();

        foreach( $dnsRecords as $dns)
        {
            if( !$dns->current->ip ) {
                if ( $ip = DnsHelper::getCurrentHostIp($dns) ) {
                    $dns->current->ip = $ip;
                   if(!$dns->current->save() ) {
                       var_dump($dns->current->getErrors());
                   }
                }
            }

            if(  $dns->set_ip && $dns->current->ip  && ($dns->current->ip != $dns->set_ip) ) {
                DnsHelper::changeDns($dns);
            }

        }

        return true;
    }


} 