<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class CleanCommand extends CConsoleCommand {

    public function run($args) {
        $connection = Yii::app()->db;

        $command = $connection->createCommand('DELETE FROM ping WHERE time < (NOW()-600)');
        echo "ping: " . $command->execute() . "\n";

        $command = $connection->createCommand('DELETE FROM device_param_stat WHERE time < (NOW()-300)');
        echo "device param stat: " . $command->execute() . "\n";

        $command = $connection->createCommand('DELETE FROM interface_stat WHERE time < (NOW()-300)');
        echo "interface stat: " . $command->execute() . "\n";
    }
}