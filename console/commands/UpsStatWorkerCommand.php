<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class UpsStatWorkerCommand extends WorkerCommand {

    public $tube = 'upsStat';

    private $_ups = array();


    const TIME_REQUEST = 15;



    public function init() {
        parent::init();
        $this->_ups = Device::model()->with(array('params'))->findAll('type=:type AND server_api="nut"', array(':type' => DeviceType::UPS));
    }

    public function getWorkerId() {
        return 4;
    }

    public function run($args) {
        while($this->runtimeChecks())
        {
            $start = time();
            $this->work($args);

            $used = time() - $start;
            $this->report("Used: {$used} sec");

            $wait = static::TIME_REQUEST - $used;
            if ($wait > 0) {
                $this->report("wait {$wait} seconds...");
                sleep($wait);
                $this->report("return to job ");
            }
        }
    }

    public function work($args)
    {
        $params = array(
            "ups.load",
            "ups.status",
            "input.voltage",
            "output.voltage",
        );

        foreach ( $this->_ups as $ups )
        {
            foreach($params as $param) {
                $value = shell_exec("upsc {$ups->alias}@{$ups->ip} {$param}");
                DeviceParam::updateInfoByModel($ups, $param, trim($value));
                DeviceParamStat::saveResult($ups->id, $param, trim($value));
            }
        }

        return true;
    }
}