<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

use PEAR2\Net\RouterOS;

class ListenResourcesCommand extends CConsoleCommand {

    public function run($args) {

        $mikrotik = ($args[0] == 1) ? Yii::app()->mikrotik :  Yii::app()->mikrotik2;

        $monitorRequest = new RouterOS\Request('/system/resource/monitor');
        $monitorRequest->setArgument('interval', 5);

        $monitorListener = function ($response) use ($args) {
            if ($response->getType(RouterOS\Response::TYPE_DATA)) {
                // $insertStatement[1]['tiimestamp'] = date('Y-m-d H:i:s');

                print $response->getProperty('cpu-used');
                print " / ";
                print $response->getProperty('free-memory');
                print "\r\n";

                DeviceParamStat::saveResult($args[0], 'cpu-used', $response->getProperty('cpu-used'));
                DeviceParamStat::saveResult($args[0], 'free-memory', $response->getProperty('free-memory'));

            }
        };

        $monitorRequest->setTag('resources-listen');

        $mikrotik->sendAsync(
            $monitorRequest,
            $monitorListener
        );

        $mikrotik->loop();

    }
} 