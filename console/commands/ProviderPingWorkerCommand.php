<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */
use PEAR2\Net\RouterOS;

class ProviderPingWorkerCommand extends WorkerCommand
{

    const TIME_REQUEST = 15;

    protected $_client;
    protected $_pingRequest;
    protected $_providers = array();
    protected $_positions = array();
    protected $_queues = array();

    protected $_pppCheckRequest;
    protected $_etherCheckRequest;



    public function init()
    {
        parent::init();

        $this->_client = Yii::app()->mikrotik;
        $this->_pingRequest = new RouterOS\Request('/ping');

        $this->_pppCheckRequest = new RouterOS\Request('/interface/pppoe-client/monitor');
        $this->_etherCheckRequest = new RouterOS\Request('/interface/ethernet/monitor');
        $this->_pppCheckRequest->setArgument('once', '');
        $this->_etherCheckRequest->setArgument('once', '');

        // restart need when provider change
        foreach(Provider::model()->findAll() as $provider) {
            $this->_providers[$provider->id] = $provider;
        }

        // restart need when queue change
        foreach(Queue::model()->with(array('positions'))->findAll() as $queue)
        {
            $this->_queues[$queue->id] = $queue;
            foreach($queue->positions as $position)
            {
                $this->_positions[$queue->id][$position->id] = $position;
            }
        }
    }


    public function run($args) {
        while($this->runtimeChecks())
        {
            $start = time();
            $this->work($args);

            $used = time() - $start;
            $this->report("Used: {$used} sec");

            $wait = static::TIME_REQUEST - $used;
            if ($wait > 0) {
                $this->report("wait {$wait} seconds...");
                sleep($wait);
                $this->report("return to job ");
            }

        }
    }


    protected function getProvider($id)
    {
        if(!empty($this->_providers[$id]))
            return $this->_providers[$id];

        return null;
    }

    protected function getProviderByPosition($queueId, $positionId)
    {
        if(!empty($this->_positions[$queueId]))
            if(!empty($this->_positions[$queueId][$positionId]))
                return $this->getProvider($this->_positions[$queueId][$positionId]->provider_id);

        return null;
    }

    protected function getPosition($queueId, $positionId)
    {
        if(!empty($this->_positions[$queueId]))
        {
            if(!empty($this->_positions[$queueId][$positionId]))
                return $this->_positions[$queueId][$positionId];
        }

        return null;
    }

    protected function getPositionByProvider($queueId, $providerId) {
        if(!empty($this->_positions[$queueId]))
        {
            foreach($this->_positions[$queueId] as $position) {
                if ($position->provider_id==$providerId) {
                    return $position;
                }
            }
        }

        return null;
    }


    public function getWorkerId() {
        return 1;
    }

    protected function testInterface(Provider $provider)
    {
        $request = (strpos($provider->interface, 'ppp'))
            ? $this->_pppCheckRequest
            : $this->_etherCheckRequest;

        // echo "check interface: {$provider->interface}... \n ";

        $request->setArgument('.id', $provider->interface);

        $results = $this->_client->sendSync($request);

        foreach($results as $result) {
            if ($result->getType() == RouterOS\Response::TYPE_DATA)
            {
               // echo " status: {$result('status')} \n";
                $result = $result('status') == 'link-ok' || $result('status') == 'connected';
                // echo "Result: "; var_dump($result);
                return $result;
            }
        }

        return false;
    }


    /**
     * @param Provider $provider
     *
     * @return bool
     */
    protected function pingProvider(Provider $provider)
    {
        if(!$this->testInterface($provider)) {
            Ping::addNotAvailableProviderResult($provider);
            return false;
        }

        // echo "real ping: {$provider->interface}\r\n";

        $this->_pingRequest->setArgument('address', ($provider->ping_host) ? $provider->ping_host : '8.8.8.8');
        $this->_pingRequest->setArgument('count', ($provider->ping_num) ? $provider->ping_num : 3);
        $this->_pingRequest->setArgument('interface', $provider->interface);
        // $this->_pingRequest->setArgument('timeout', (int)$provider->ping_timeout);
        $results = $this->_client->sendSync($this->_pingRequest);

        foreach($results as $result)
        {
            if ($result->getType() == RouterOS\Response::TYPE_DATA) {
                Ping::saveProviderResult($provider, $result);
            }
        }

        return true;
    }

    public function work($args)
    {

        // restart need when provider change ...
        foreach($this->_providers as $provider)
        {
            $this->pingProvider($provider);

            if($provider->status_mode != NetworkStatus::MODE_MANUAL)
            {
                NetworkStatus::updateProviderStatus($provider);
            }

            // optimize restart time
            $this->checkModel();

            // restart need when queue change
            foreach($this->_queues as $q)
            {
                if($q->positions && count($q->positions)>1 ) {

                    if( !$q->active || $this->getProviderByPosition($q->id, $q->active)->getStatus() <= NetworkStatus::POOR )
                    {
                        $best = $q->getBestProvider($this->_providers);
                        if( $best && $best->id != $q->active
                            && ( $this->getProvider($best->provider_id)->getStatus() > $this->getProviderByPosition($q->id, $q->active)->getStatus() )
                        )
                        {
                            $q->toggleTo($best);
                        }
                    }
                    else
                    {
                        foreach($q->positions  as $p)
                        {
                            if( $this->getProvider($p->provider_id)->getStatus() == NetworkStatus::BEST
                                && ($this->getPosition($q->id, $q->active)->position > $p->position) )
                            {
                                $q->toggleTo($p);
                            }
                        }
                    }

                } elseif( count($q->positions)==1 )
                {
                    $ps = $q->positions;
                    $p = array_pop($ps);

                    if(!$q->active || $q->active != $p->id) {
                        $q->toggleTo($p);
                    }
                }
            }
        }
    }
} 