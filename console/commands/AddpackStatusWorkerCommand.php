<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class AddpackStatusWorkerCommand extends WorkerCommand {

    public $tube = 'addpackStatus';

    private $_gates = array();

    const TIME_REQUEST = 15;

    public function init() {
        parent::init();
        $this->_gates = Device::model()->with(array('params'))->findAll('type=:type AND server_api="addpack"', array(':type' => DeviceType::VOIP));
    }

    public function getWorkerId() {
        return 5;
    }

    public function run($args) {
        while($this->runtimeChecks())
        {
            $start = time();
            $this->work($args);

            $used = time() - $start;
            $this->report("Used: {$used} sec");

            $wait = static::TIME_REQUEST - $used;
            if ($wait > 0) {
                $this->report("wait {$wait} seconds...");
                sleep($wait);
                $this->report("return to job ");
            }
        }
    }


    public function work($args) {

        foreach( $this->_gates as $device)
        {

            echo "id: " . $device->id . ", alias: " . $device->alias . ":\n";

            try {
                $telnet = new TelnetClient($device->ip, 23, 10, "#");
                $telnet->connect();
                $telnet->login($device->login, $device->pass);
                $telnet->execute('enable');
                if ($out = $telnet->execute('show gsm sim-card')) {
                    $out = preg_replace("/show gsm sim-card\r\n\r\n/", "", $out);
                    if(strpos($out, 'Unknown command')) {
                        if($out = $telnet->execute('show mobile sim-card')) {
                            $out = preg_replace("/show mobile sim-card\r\n\r\n/", "", $out);
                        }
                    }
                } else {
                    $out = "Unknown error: empty answer detected..";
                }
                $telnet->disconnect();
            }
            catch(Exception $e)
            {
                $out = "Error: " . $e->getMessage();
            }

            DeviceParam::updateInfoByModel($device, "gsm.sim-card", $out);

            echo "\n";
        }

        return true;
    }
}