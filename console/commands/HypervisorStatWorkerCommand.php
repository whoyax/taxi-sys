<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class HypervisorStatWorkerCommand extends WorkerCommand {

    public $tube = 'hyperStat';

    private $_clients = array();
    private $_hosts = array();

    protected $_vm = array();

    const TIME_REQUEST = 30;

    protected function getClient($h) {
        if(! isset($this->_clients[$h->id])) {
            $this->_clients[$h->id] = new Vmwarephp\Vhost("{$h->ip}:{$h->port}", $h->login, $h->pass);
        }

        return $this->_clients[$h->id];
    }

    public function init() {
        parent::init();
        $this->_hosts = Device::model()->with(array('params'))->findAll('type=:type', array(':type' => DeviceType::HYPERVISOR));
    }

    public function getWorkerId() {
        return 2;
    }

    public function run($args) {
        while($this->runtimeChecks())
        {
            $start = time();
            $this->work($args);

            $used = time() - $start;
            $this->report("Used: {$used} sec");

            $wait = static::TIME_REQUEST - $used;
            if ($wait > 0) {
                $this->report("wait {$wait} seconds...");
                sleep($wait);
                $this->report("return to job ");
            }

        }
    }

    protected function getVm($name, $hostId){
        if(empty($this->_vm[$hostId . "#" . $name])) {
            $this->_vm[$hostId . "#" . $name] = Device::findOrCreateVm($name, $hostId);
        }

        return $this->_vm[$hostId . "#" . $name];
    }

    public function work($args) {

        foreach($this->_hosts as $h)
        {
            $client = $this->getClient($h);

            $result = $client->findAllManagedObjects('HostSystem',
                array(
                    'summary.quickStats.overallCpuUsage',
                    'vm',
                ));

            $params = array(
                "summary.quickStats.overallCpuUsage" => true,
            );

            $host = $result[0];

            foreach($params as $param => $save)
            {
                $value = $host;
                $keys = explode('.', $param);
                foreach($keys as $key) {
                    $value = $value->{$key};
                }
                // DeviceParam::updateInfoByModel($h, $param, $value);
                if($save) {
                    DeviceParamStat::saveResult($h->id, $param, $value);
                }
            }

            $params = array(
                "summary.quickStats.overallCpuUsage" => true,
                "summary.quickStats.guestMemoryUsage" => true,
            );

            foreach($host->vm as $vm) {

                $device = $this->getVm($vm->name, $h->id);

                foreach($params as $param => $save)
                {
                    $value = $vm;
                    $keys = explode('.', $param);
                    foreach($keys as $key) {
                        $value = $value->{$key};
                    }
                    DeviceParam::updateInfoByModel($device, $param, $value);
                    if($save) {
                        DeviceParamStat::saveResult($device->id, $param, $value);
                    }
                }
            }
        }

        return true;
    }
}