<?php

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class ChatServerCommand extends CConsoleCommand {

    public function run($args) {

        $chat = new Chat();

//        $server = IoServer::factory(
//            new HttpServer(
//                new WsServer( $chat )
//            ),
//            8080
//        );

        //$redis = new \Predis\Async\Client('tcp://192.168.56.102:6379', array('eventloop' => $server->loop, 'timeout' => 200));

        //$loop = new React\EventLoop\StreamSelectLoop();
        $redis = new \Predis\Client('tcp://192.168.56.102:6379?password=redis4567876');


//        $redis->connect(function ($client) use ($chat) {
//            echo "Connecting.. \n";

        $pubsub = null;

        try
        {
            $pubsub = $redis->pubSubLoop();
            $pubsub->subscribe('tst:channel');

            foreach($pubsub as $event)
            {
                echo "Server: message '{$event->payload}' from {$event->channel}.\n";
                $chat->send($event);
            }

            echo "Running.. \n";

        }
        catch (\Exception $e)
        {
            echo "\nException!\n";
            var_dump($e);
        }

        unset($pubsub);

        //$loop->run();
        //$server->run();
    }
} 