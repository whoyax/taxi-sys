<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class ZabbixRequestCommand extends LockedConsoleCommand {

    public function doWork($args) {
        $start = time();

        $easyParams = array(
            "system.uname" => 'summary.config.guestFullName',
            "system.uptime" => 'summary.quickStats.uptimeSeconds',
            "vm.memory.size[free]" => 'summary.config.memoryFree',
            "vm.memory.size[total]" => 'summary.config.memorySize',
            "system.cpu.load[percpu,avg1]" => 'summary.runtime.maxCpuUsage'
        );

        foreach(Device::model()->with(array('params'))->findAll('type=:type AND server_api="zabbix"', array(':type' => DeviceType::WORKSTATION) ) as $w)
        {

            echo "id: " . $w->id . ", alias: " . $w->alias . ":\n";

            if( $hostId = Yii::app()->zabbix->createCommand("SELECT hostid FROM hosts WHERE host=:hostname")->queryScalar(array(':hostname' => $w->alias)) ) {
                echo "find: " . $hostId . "\n";

                $select = "SELECT
                    itemid, `name`, key_,
                    (select `value` from history h1 WHERE h1.itemid = i.itemid order by h1.clock DESC LIMIT 1)  as his,
                    (select `value` from history_uint hu1 WHERE hu1.itemid = i.itemid order by hu1.clock DESC LIMIT 1 ) as his_uint ,
                    (select `value` from history_str hu2 WHERE hu2.itemid = i.itemid order by hu2.clock DESC LIMIT 1 ) as his_str

                    FROM `items` i WHERE hostid = :hostId;";

                if ( $params = Yii::app()->zabbix->createCommand($select)->queryAll(true, array(':hostId' => $hostId)) ) {

                    $hdCount = array();

                    echo "params request success\n";

                    foreach($params as $param)
                    {

                        echo "param key: {$param['key_']}\n";

                        if( isset($easyParams[$param['key_']]))
                        {
                            echo "identifiaed as easyparam\n";

                            $value = $param['his'];
                            if(!$value) $value = $param['his_uint'];
                            if(!$value) $value = $param['his_str'];
                            $result = DeviceParam::updateInfoByModel($w, $easyParams[$param['key_']], $value);
                            echo "result: " . var_export($result, 1) . "\n";

                        } elseif( preg_match("/vfs.fs.size\[[A-Z]:,(free|total)\]/", $param['key_']) )
                        {

                            echo "identifiaed as diskinfo\n";

                            preg_match("/vfs.fs.size\[([A-Z]:),.*\]/", $param['key_'], $letters);
                            $letter = ($letters[1]);
                            preg_match("/vfs.fs.size\[[A-Z]:,(free|total)\]/", $param['key_'], $types);
                            $type = $types[1];

                            if( isset($hdCount[$letter]) ) {
                                $i = $hdCount[$letter];
                            } else {
                                $i = $hdCount[$letter] = count($hdCount);
                            }

                            $diskParams = array(
                                "total" => "capacity",
                                "free" => "freeSpace",
                            );
                                $result = DeviceParam::updateInfoByModel($w, "datastore.{$i}.diskPath", $letter);
                                $name = "datastore.{$i}.{$diskParams[$type]}";
                                $value = $param['his_uint'];
                                $result = DeviceParam::updateInfoByModel($w, $name, $value);
                                echo "result: " . var_export($result, 1) . "\n";

                        }
                    }
                }

            } else {
                echo "not found\n";
            }

            continue;

        }

        echo time() - $start;
    }


} 