<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

class HypervisorRequestCommand extends LockedConsoleCommand {

    public function doWork($args) {
        $start = time();
        foreach(Device::model()->with(array('params'))->findAll('type=:type', array(':type' => DeviceType::HYPERVISOR) ) as $h)
        {
            $vhost = new Vmwarephp\Vhost("{$h->ip}:{$h->port}", $h->login, $h->pass);

            $result = $vhost->findAllManagedObjects('HostSystem',
                array(
                    'summary.config.name',
                    'summary.config.product',
                    'summary.hardware',
                    'summary.quickStats',
                    'datastore',
                    'vm',
                ));

            $params = array(
                "summary.config.name" => false,
                "summary.config.product.fullName" => false,
                "summary.hardware.cpuModel" => false,
                "summary.hardware.memorySize" => false,
                "summary.hardware.cpuMhz" => false,
                "summary.hardware.numCpuPkgs" => false,
                "summary.hardware.numCpuThreads" => false,
                "summary.runtime.bootTime" => false,
                "summary.quickStats.overallCpuUsage" => true,
                "summary.quickStats.overallMemoryUsage" => false,
                "summary.quickStats.uptime" => false,
            );

            $host = $result[0];

            foreach($params as $param => $save)
            {
                $value = $host;
                $keys = explode('.', $param);
                foreach($keys as $key) {
                    $value = $value->{$key};
                }
                DeviceParam::updateInfoByModel($h, $param, $value);
                if($save) {
                    DeviceParamStat::saveResult($h->id, $param, $value);
                }
            }


            $params = array(
                "name" => false,
                "accessible" => false,
                "capacity" => false,
                "freeSpace" => false,
                "type" => false,
                "uncommitted" => false,
            );

            $i = 1;
            foreach($host->datastore as $datastore) {
                $summary = $datastore->summary;
                foreach($params as $param => $save)
                {
                    $name = "datastore.{$i}.{$param}";
                    $value = $summary->{$param};
                    DeviceParam::updateInfoByModel($h, $name, $value);
                    if($save) {
                        DeviceParamStat::saveResult($h->id, $name, $value);
                    }
                }
                $i++;
            }


            $params = array(
                // "summary.config.name" => false,
                "summary.config.memorySizeMB" => false,
                "summary.config.numCpu" => false,
                "summary.config.guestFullName" => false,

                "summary.runtime.connectionState" => false,
                "summary.runtime.powerState" => false,
                "summary.runtime.maxCpuUsage" => false,
                "summary.runtime.maxMemoryUsage" => false,
                "summary.runtime.bootTime" => false,

                "summary.storage.committed" => false,
//                "summary.storage.uncommitted" => false,
//                "summary.storage.unshared" => false,

                "summary.quickStats.overallCpuUsage" => true,
                "summary.quickStats.guestMemoryUsage" => true,
                "summary.quickStats.uptimeSeconds" => false,
            );


            foreach($host->vm as $vm) {

                $device = Device::findOrCreateVm($vm->name, $h->id);


                foreach($params as $param => $save)
                {
                    $value = $vm;
                    $keys = explode('.', $param);
                    foreach($keys as $key) {
                        $value = $value->{$key};
                    }
                    DeviceParam::updateInfoByModel($device, $param, $value);
                    if($save) {
                        DeviceParamStat::saveResult($device->id, $param, $value);
                    }
                }

                $guest = $vm->guest;

                if($n = $guest->net ) {
                    foreach($n as $net) {
                        if ($adrs = $net->ipAddress) {
                            foreach($adrs as $adr) {
                                $device->updateIpInfo($adr, $net->network);
                            }
                        }
                    }
                }

                $diskParams = array(
                    "diskPath" => false,
                    "capacity" => false,
                    "freeSpace" => false,
                );

                if ($disks = $guest->disk )
                {
                    $i = 1;
                    foreach($disks as $disk) {
                        foreach($diskParams as $diskParam => $save)
                        {
                            $name = "datastore.{$i}.{$diskParam}";
                            $value = $disk->{$diskParam};
                            DeviceParam::updateInfoByModel($device, $name, $value);
                            if($save) {
                                DeviceParamStat::saveResult($device->id, $name, $value);
                            }
                        }
                        $i++;
                    }
                }
            }
        }

        echo time() - $start;
    }


} 