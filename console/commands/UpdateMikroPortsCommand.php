<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */

use PEAR2\Net\RouterOS;

class UpdateMikroPortsCommand extends CConsoleCommand {

    public function run($args) {
        foreach(Device::model()->findAll('type=:type', array(':type' => DeviceType::MIKROTIK)) as $m)
        {
            $mikrotik = ($m->id==1) ? Yii::app()->mikrotik : Yii::app()->mikrotik2;

            $interfaceListRequest = new RouterOS\Request('/interface print');

            $results = $mikrotik->sendSync($interfaceListRequest);
            $list = array();

            $i=0;
            foreach($results as $result) {
                if ($result->getType() == RouterOS\Response::TYPE_DATA) {
                    DeviceParam::updateInfo($m->id, "ports.{$i}.name", $result('name'));
                    $i++;
                }
            }
        }
    }
} 