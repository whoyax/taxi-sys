<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 02.10.15
 * Time: 2:17
 */

class LockedConsoleCommand extends CConsoleCommand {



    public function run($params)
    {
        set_time_limit(0);

        if( !$this->isAlreadyRunned() ) {

            try {
                if( $this->setLock() ) {
                    $this->doWork($params);
                }

                $this->dropLock();

            } catch (Exception $e) {
                $this->dropLock();
                throw $e;
            }

        } else {
            echo "\"{$this->getName()}\" already runned!\n";
            return 1;
        }
        return 0;
    }


    protected function isAlreadyRunned() {
        return file_exists($this->getLockFileName());
    }

    protected function getLockFileName() {
        return Yii::app()->getRuntimePath()
        . DIRECTORY_SEPARATOR
        . $this->getName()
        . '_saferun.lock';
    }

    public function getName() {
        return get_class($this);
    }

    protected function setLock() {
        return file_put_contents($this->getLockFileName(), $this->time());
    }

    protected function dropLock() {
        return unlink($this->getLockFileName());
    }

    protected function time() {
        return date("d.m.Y H:i:s");
    }

} 