<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */


class WorkerCommand extends CConsoleCommand {

    private $_lastChangeTime;

    private $_selfFile;

    protected $_workerModel;

    public function init() {

        $this->report("Starting...");

        $this->_selfFile = __FILE__;
        $this->_lastChangeTime = $this->_getChangeTime();

        $this->checkModel();

        $this->_workerModel->markRun();
    }

    public function getWorkerId() {
        return 0;
    }


    protected function waitAndRestart($time = 30){
        $this->report("Wait {$time} and restart...");
        if($time && $this->_workerModel) {
            $this->_workerModel->markWait();
        }

        sleep($time);

        $this->shutdown("restart need", Worker::STATUS_RESTART);
    }

    protected function shutdown($reason = "unknown", $status = Worker::STATUS_SHUTDOWN) {
        $this->report("Shutdown ({$reason})");
        if($this->_workerModel) {
            $this->_workerModel->setStatus($status);
        }
        exit(1);
    }



    public function run($args) {
        $this->shutdown("nothing to work");
    }


    protected function report($str = "") {
        echo  "{$this->time()} "
            . get_class($this)
            . ": {$str} \n";
    }


    private function _getChangeTime()
    {
        $fp = fopen($this->_selfFile, "r");
        $stat = fstat($fp);
        return $stat['mtime'];
    }

    public function runtimeChecks(){
        return $this->checkChangeTime() && $this->checkModel();
    }

    public function checkModel() {
        $this->_workerModel = Worker::model()->findByPk($this->getWorkerId());

        if(!$this->_workerModel) {
            $this->report("No worker model found...");
            $this->waitAndRestart(120);
        }

        if($this->_workerModel->shutdown) {
            $this->report("Shutdown detected...");
            $this->waitAndRestart(30);
        }

        if($this->_workerModel->restart) {
            $this->report("Restart detected...");
            $this->_workerModel->markRestart();
            $this->waitAndRestart(0);
        }

        if($this->_workerModel->status==Worker::STATUS_SHUTDOWN) {
            $this->report("Shutdown detected...");
            $this->waitAndRestart(30);
        }

        return true;
    }

    public function checkChangeTime()
    {
        if ($this->_getChangeTime() !== $this->_lastChangeTime)
        {
            $this->report("Source file change detected.");
            $this->shutdown("restart need");
        }

        return true;
    }

    protected function time() {
        return date("d.m.Y H:i:s");
    }
} 