<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 6:39
 */


class RecycleWorkerCommand extends CConsoleCommand {

    public $tube;

    private $_lastChangeTime;

    private $_selfFile;


    public function run($args) {

        $this->_selfFile = __FILE__;
        $this->_lastChangeTime = $this->_getChangeTime();

        $client = Yii::app()->yiinstalk->getClient();

        print "Waiting for job...\n";

        while($job = $client
            ->watch($this->tube)
            ->ignore('default')
            ->reserve()) {

            print "Job!...#{$job->getId()}\n";
            print "Data:...{$job->getData()}\n";

            $result = false;
            $start = time();

            try
            {
                $result = $this->doWork($job);

            }
            catch(Exception $e)
            {
                $job->release();
                throw $e;
            }

            if( !$result ) {
                $job->release();
            } else {
                $client->useTube($this->tube)->put($job->getData());
                $client->delete($job);
            }

            $this->checkChangeTime();

            echo "\n" . time() - $start . "\n";
        }
    }


    private function _getChangeTime()
    {
        $fp = fopen($this->_selfFile, "r");
        $stat = fstat($fp);
        return $stat['mtime'];
    }

    public function checkChangeTime()
    {
        $time = $this->_getChangeTime();

        if ($time !== $this->_lastChangeTime)
        {
            echo "File has been changed!";
            exit(1);
        }
    }

} 