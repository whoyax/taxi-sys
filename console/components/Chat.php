<?php

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\WebSocket\Version\RFC6455\Connection;

class Chat implements MessageComponentInterface
{
    /** @var ConnectionInterface[] */
    protected $clients = array();

    protected $params;

    public function __construct(Array $params = array()) {
        $this->params = $params;
    }

    /**
     * @param ConnectionInterface|Connection $conn
     * @return string
     */
    private function getRid(ConnectionInterface $conn)
    {
        return $conn->resourceId;
    }

    /**
     * @param ConnectionInterface|Connection $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        echo "onOpen: \n";
        var_dump($conn);
        echo "\n";

        $this->clients[$this->getRid($conn)] = $conn;
    }

    public function onClose(ConnectionInterface $conn)
    {
        echo "onClose: \n";
        var_dump($conn);
        echo "\n";
        $rid = array_search($conn, $this->clients);
//        if ($user = $this->chm->getUserByRid($rid)) {
//            $chat = $user->getChat();
//            $this->chm->removeUserFromChat($user, $chat);
//            foreach ($chat->getUsers() as $user) {
//                $this->clients[$user->getRid()]->close();
//            }
//        }
        unset($this->clients[$rid]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "Error: \n";
        var_dump($conn);
        var_dump($e);
        echo "\n";

        $conn->close();
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $msg = json_decode($msg, true);
        $rid = array_search($from, $this->clients);
        echo "Message: \n";
        echo var_dump($msg);
        echo "\n";
//        switch ($msg['type']) {
//            case 'request':
//                $chat = $this->chm->findOrCreateChatForUser($rid);
//                if ($chat->getIsCompleted()) {
//                    $msg = json_encode(['type' => 'response']);
//                    foreach ($chat->getUsers() as $user) {
//                        $conn = $this->clients[$user->getRid()];
//                        $conn->send($msg);
//                    }
//                }
//                break;
//            case 'message':
//                if ($chat = $this->chm->getChatByUser($rid)) {
//                    foreach ($chat->getUsers() as $user) {
//                        $conn = $this->clients[$user->getRid()];
//                        $msg['from'] = $conn === $from ? 'me' : 'guest';
//                        $conn->send(json_encode($msg));
//                    }
//                }
//                break;
//        }
    }

    public function send($event)
    {
        foreach ($this->clients as $client) {
            $client->send(json_encode($event->payload));
        }
    }
}