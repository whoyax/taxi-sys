<?php

defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'console');

$localConfig = (file_exists(dirname(__FILE__) . '/.prod')) ? 'prod' : 'dev';

return CMap::mergeArray(
    require(dirname(__FILE__).'/../../common/config/main.php'),
    array(
        'basePath' => realPath(__DIR__ . '/..'),
        'commandMap' => array(
            'migrate' => array(
                'class' => 'system.cli.commands.MigrateCommand',
                'migrationPath' => 'application.migrations'
            )
        ),

        'import' => array(
            'backend.extensions.*',
            'backend.components.*',
            'console.components.*',
            'backend.helpers.*',
            'backend.models.*',
            'backend.widgets.*',
        ),

        'components' => array(

        )
    ),
    require(dirname(__FILE__).'/'. $localConfig .'.php'),
    require(dirname(__FILE__).'/local.php')
);