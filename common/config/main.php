<?php

$localConfig = (file_exists(dirname(__FILE__) . '/.prod')) ? 'prod' : 'dev';

return CMap::mergeArray(
    array(
        'name' => '{APPLICATION NAME}',
        'preload' => array('log'),
        'language' => 'ru',
        'aliases' => array(
            'frontend' => dirname(__FILE__) . '/../..' . '/frontend',
            'common' => dirname(__FILE__) . '/../..' . '/common',
            'backend' => dirname(__FILE__) . '/../..' . '/backend',
            'console' => dirname(__FILE__) . '/../..' . '/console',
            'vendor' => dirname(__FILE__) . '/../..' . '/common/lib/vendor'
        ),
        'import' => array(
            'common.extensions.components.*',
            'common.extensions.IpValidator.*',
            'common.components.*',
            'common.helpers.*',
            'common.models.*',
            'application.controllers.*',
            'application.extensions.*',
            'application.helpers.*',
            'application.models.*',
            'vendor.2amigos.yiistrap.helpers.*',
            'vendor.2amigos.yiiwheels.helpers.*',
        ),
        'components' => array(
            'db'=>array(
                'connectionString' => '',
                'charset' => 'utf8',
            ),

            'zabbix'=>array(
                'class' => 'CDbConnection',
                'connectionString' => '',
                'charset' => 'utf8',
                'emulatePrepare' => false,
                'enableProfiling' => true,
                'enableParamLogging' => true,

            ),

            'era'=>array(
                'class' => 'CDbConnection',
                'emulatePrepare' => false,
                'enableProfiling' => true,
                'enableParamLogging' => true,
                'connectionString' => '',
                'charset' => 'utf8',
            ),

            'cache' => array(
                'class' => 'CRedisCache',
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 0
            ),

            'yiinstalk' => array(
                'class' => 'Yiinstalk',
                'connections' => array(
                    'default' => array(
                        'host' => '127.0.0.1',
                        'port' => 11300,
                    ),
                ),
            ),

            'mikrotik' => array(
                'class' => 'Mikrotik',
                'host' => '',
                'username' => '',
                'password' => '',
            ),

            'mikrotik2' => array(
                'class' => 'Mikrotik',
                'host' => '',
                'username' => '',
                'password' => '',
            ),

            'log' => array(
                'class'  => 'CLogRouter',
                'routes' => array(
                    array(
                        'class'        => 'CDbLogRoute',
                        'connectionID' => 'db',
                        'levels'       => 'error, warning',
                    ),
                ),
            ),
        ),
        // application parameters
        'params' => array(
            // php configuration
            'php.defaultCharset' => 'utf-8',
            'php.timezone'       => 'UTC',
        ),
    ),

    require(dirname(__FILE__).'/'. $localConfig .'.php'),
    require(dirname(__FILE__).'/local.php')
);
