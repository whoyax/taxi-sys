<?php

return array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=',
            'emulatePrepare' => false,
            'enableProfiling' => false,
            'schemaCachingDuration' => 60 * 60,
            'enableParamLogging' => false,
        ),

        'mikrotik' => array(
            'host' => '',
            'username' => '',
            'password' => '',
            'port' => 1234,
        ),
        'mikrotik2' => array(
            'host' => '',
            'username' => '',
            'password' => '',
            'port' => 1234,
        ),

    ),
    'params' => array(
        'yii.debug' => false,
        'yii.traceLevel' => 0,
        'yii.handleErrors' => APP_CONFIG_NAME !== 'test',
    )
);