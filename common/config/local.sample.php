<?php

return array(
    'modules' => array(
    ),
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=',
           // 'emulatePrepare' => false,
            'username' => '',
            'password' => '',
           // 'enableProfiling' => true,
          //  'enableParamLogging' => true,
        ),

        'mikrotik' => array(
            'host' => '',
            'username' => '',
            'password' => '',
            'port' => 0,
        ),

        'mikrotik2' => array(
            'host' => '',
            'username' => '',
            'password' => '',
            'port' => 0,
        ),

    ),
    'params' => array(
        'yii.handleErrors' => true,
        'yii.debug' => true,
        'yii.traceLevel' => 3,
        'users' => array(
            '' => '',
            '' => '',
        ),
        'roles' => array(
            '' => '',
            '' => '',
            '' => '',
        )
    )
);