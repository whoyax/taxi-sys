CREATE TABLE IF NOT EXISTS `worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `last_report` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `restart` tinyint(4) NOT NULL,
  `shutdown` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `worker` (`id`, `name`, `status`, `last_report`, `restart`, `shutdown`) VALUES
(1, 'providerPing', 0, '2015-10-08 04:19:45', 0, 0),
(2, 'hypervisorStat', 0, '2015-10-08 04:33:36', 0, 0);