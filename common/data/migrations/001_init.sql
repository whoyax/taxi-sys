-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 07 2015 г., 03:41
-- Версия сервера: 5.6.20
-- Версия PHP: 5.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `ping`
--

-- --------------------------------------------------------

--
-- Структура таблицы `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) DEFAULT NULL,
  `name` VARCHAR(100) NOT NULL,
  `ip` VARCHAR(16) NOT NULL,
  `queue_id` INT(11) DEFAULT NULL,
  `description` TEXT NOT NULL,
  `login` VARCHAR(50) DEFAULT NULL,
  `pass` VARCHAR(50) DEFAULT NULL,
  `alias` VARCHAR(50) DEFAULT NULL,
  `server_api` VARCHAR(50) DEFAULT NULL,
  `type` SMALLINT(5) NOT NULL DEFAULT '0',
  `port` INT(11) DEFAULT NULL,
  `interface` VARCHAR(100) DEFAULT NULL,
  `parent_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =53;

-- --------------------------------------------------------

--
-- Структура таблицы `device_access`
--

CREATE TABLE IF NOT EXISTS `device_access` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `device_id` INT(11) NOT NULL DEFAULT '1',
  `name` VARCHAR(100) NOT NULL,
  `description` TEXT,
  `login` VARCHAR(50) NOT NULL,
  `pass` VARCHAR(50) NOT NULL,
  `link` VARCHAR(250) DEFAULT NULL,
  `type` INT(11) DEFAULT '1',
  `port` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =52;

-- --------------------------------------------------------

--
-- Структура таблицы `device_ip`
--

CREATE TABLE IF NOT EXISTS `device_ip` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `device_id` INT(11) DEFAULT '1',
  `ip` VARCHAR(16) NOT NULL,
  `name` VARCHAR(50) DEFAULT NULL,
  `description` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =8;


-- --------------------------------------------------------

--
-- Структура таблицы `device_param`
--

CREATE TABLE IF NOT EXISTS `device_param` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `device_id` INT(11) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `value` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_id_2` (`device_id`, `name`),
  KEY `device_id` (`device_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =668;

--
-- Структура таблицы `device_param_stat`
--

CREATE TABLE IF NOT EXISTS `device_param_stat` (
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `device_id` INT(11) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `value` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`time`, `device_id`, `name`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `dns_settings`
--

CREATE TABLE IF NOT EXISTS `dns_settings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `service_name` VARCHAR(100) NOT NULL,
  `service_url` VARCHAR(250) NOT NULL,
  `queue_id` INT(11) DEFAULT NULL,
  `auto_dns` INT(11) NOT NULL,
  `domain_name` VARCHAR(250) NOT NULL,
  `username` VARCHAR(250) NOT NULL,
  `authcode` VARCHAR(250)
  CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =2;


-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` TEXT,
  `queue_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =8;

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id`, `name`, `description`, `queue_id`) VALUES
(1, 'Сетевые устройства', '', NULL),
(2, 'Voip шлюзы', '', 1),
(3, 'Физические серверы', NULL, NULL),
(4, 'Виртуальные серверы', NULL, NULL),
(5, 'Рабочие места', '', 2),
(6, 'Прочее', '', NULL),
(7, 'Видеонаблюдение', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `interface_stat`
--

CREATE TABLE IF NOT EXISTS `interface_stat` (
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interface` VARCHAR(50) NOT NULL,
  `rx` INT(11) NOT NULL,
  `tx` INT(11) NOT NULL,
  `rx_sum` INT(11) NOT NULL,
  `tx_sum` INT(11) NOT NULL,
  KEY `time_2` (`time`),
  KEY `interface` (`interface`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `internet_settings`
--

CREATE TABLE IF NOT EXISTS `internet_settings` (
  `id` INT(11) NOT NULL,
  `active_gate` INT(11) NOT NULL,
  `auto_gate` INT(11) NOT NULL,
  `ping_timeout` INT(11) NOT NULL DEFAULT '1500',
  `ping_num` INT(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

--
-- Дамп данных таблицы `internet_settings`
--

INSERT INTO `internet_settings` (`id`, `active_gate`, `auto_gate`, `ping_timeout`, `ping_num`) VALUES
(1, 1, 0, 1500, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `device_id` INT(11) NOT NULL,
  `text` TEXT NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adr_id` (`device_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =1;

-- --------------------------------------------------------

--
-- Структура таблицы `ping`
--

CREATE TABLE IF NOT EXISTS `ping` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `device_id` INT(11) NOT NULL,
  `provider_id` INT(11) DEFAULT NULL,
  `result` INT(11) NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time_result` INT(11) NOT NULL,
  `status` VARCHAR(100) DEFAULT NULL,
  `loss` INT(11) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =1;

-- --------------------------------------------------------

--
-- Структура таблицы `provider`
--

CREATE TABLE IF NOT EXISTS `provider` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `description` TEXT,
  `gate` VARCHAR(50) NOT NULL,
  `ping_host` VARCHAR(50) NOT NULL,
  `ext_ip` VARCHAR(50) NOT NULL,
  `ping_timeout` INT(11) NOT NULL DEFAULT '1400',
  `ping_num` INT(11) NOT NULL DEFAULT '10',
  `alias` VARCHAR(100) NOT NULL,
  `interface` VARCHAR(50) NOT NULL,
  `status` TINYINT(4) DEFAULT NULL,
  `status_mode` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =5;

-- --------------------------------------------------------

--
-- Структура таблицы `queue`
--

CREATE TABLE IF NOT EXISTS `queue` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `description` TEXT NOT NULL,
  `active` INT(11) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =4;

--
-- Дамп данных таблицы `queue`
--

INSERT INTO `queue` (`id`, `name`, `description`, `active`) VALUES
(1, 'Основная', '', 1),
(2, 'Второстепенная', '', 5),
(3, 'Развлекательная', '', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `queue_provider`
--

CREATE TABLE IF NOT EXISTS `queue_provider` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `queue_id` INT(11) NOT NULL,
  `provider_id` INT(11) NOT NULL,
  `position` INT(11) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =10;


-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` VARCHAR(180) NOT NULL,
  `apply_time` INT(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
)
  ENGINE =MyISAM
  DEFAULT CHARSET =utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(250) NOT NULL,
  `login` VARCHAR(100) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `phone` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =3;


--
-- Структура таблицы `yiilog`
--

CREATE TABLE IF NOT EXISTS `yiilog` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `level` VARCHAR(128) DEFAULT NULL,
  `category` VARCHAR(128) DEFAULT NULL,
  `logtime` INT(11) DEFAULT NULL,
  `message` TEXT,
  PRIMARY KEY (`id`)
)
  ENGINE =MyISAM
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT =2;

--
-- Структура таблицы `yiisession`
--

CREATE TABLE IF NOT EXISTS `yiisession` (
  `id` CHAR(32) NOT NULL,
  `expire` INT(11) DEFAULT NULL,
  `data` BLOB,
  PRIMARY KEY (`id`)
)
  ENGINE =MyISAM
  DEFAULT CHARSET =utf8;

--
-- Ограничения внешнего ключа таблицы `device`
--
ALTER TABLE `device`
ADD CONSTRAINT `device_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`)
  ON DELETE SET NULL
  ON UPDATE SET NULL;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
