
CREATE TABLE IF NOT EXISTS `dns_current` (
  `id` int(11) NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;