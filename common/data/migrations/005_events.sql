
CREATE TABLE IF NOT EXISTS `event_type` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `type_id` int(11) NOT NULL,
    `title` varchar(255) NOT NULL,
    `body` text NOT NULL,
    PRIMARY KEY (`id`),
    KEY (`type_id`),
    KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_comment` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `event_id` int(11) NOT NULL,
    `body` text NOT NULL,
    PRIMARY KEY (`id`),
    KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE  `user` ADD  `role` INT NOT NULL;

ALTER TABLE  `event_comment` ADD  `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE  `event` ADD  `date_create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;