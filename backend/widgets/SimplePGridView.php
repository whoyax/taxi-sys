<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 04.09.15
 * Time: 17:19
 */

class SimplePGridView extends PGridView {

    public $addButton = false;

    public $template = "
    <div class='widget row-fluid '>
        <div class='navbar'>
            <div class='navbar-inner'>
                <h6>{widgetHeader}</h6>
                {createButton}
            </div>
        </div>

        <div class='table-overflow'>
            {items}\n
        </div>
    </div>";

    public $enablePagination = false;

}