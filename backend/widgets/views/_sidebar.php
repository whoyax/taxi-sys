<div id="sidebar">
    <div class="sidebar-tabs">
        <ul class="tabs-nav two-items">
            <li><a href="#general" title=""><i class="icon-reorder"></i></a></li>
            <li><a href="#stuff" title=""><i class="icon-cogs"></i></a></li>
        </ul>

            <?php
                $this->widget('PMenu', array(
                    'containerId' => 'general',
                    'items' =>
                        array(
                            array(
                                'label' => 'Панель',
                                'url' => array('/dashboard/index'),
                                'icon' => 'home'
                            ),
                            array(
                                'label' => 'Журнал событий',
                                'url' => array('/event/admin'),
                                'icon' => 'book'
                            ),
                            array(
                                'label' => 'Серверы',
                                'url' => array('/server/admin'),
                                'icon' => 'hdd'
                            ),
                            array(
                                'label' => 'Рабочие места',
                                'url' => array('/workstation/admin'),
                                'icon' => 'icon-group'
                            ),
                            array(
                                'label' => 'Сеть',
                                'url' => array('/network/admin'),
                                'icon' => 'globe'
                            ),
                            array(
                                'label' => 'ИБП',
                                'url' => array('/ups/admin'),
                                'icon' => 'icon-bolt'
                            ),

                            array(
                                'label' => 'Телефония',
                                'url' => array('/telephony/admin'),
                                'icon' => 'icon-phone'
                            ),

                            array(
                                'label' => 'Видеонаблюдение',
                                'url' => array('/video/admin'),
                                'icon' => 'icon-facetime-video'
                            ),
                            array(
                                'label' => 'Антивирус',
                                'url' => array('/antivirus/admin'),
                                'icon' => 'icon-eye-open'
                            ),
                            array(
                                'label' => 'Коммутация',
                                'url' => array('/commutation/admin'),
                                'icon' => 'icon-random'
                            ),

                            array(
                                'label' => 'Все устройства',
                                'url' => array('/device/admin'),
                                'icon' => 'list-ol'
                            ),
//                            array(
//                                'label' => 'Сообщения <strong>4</strong>' ,
//                                'url' => '#',
//
//                                'items' => array(
//                                    array('label' => 'Home', 'url' => array('/dashboard/index')),
//                                    array('label' => 'Settings', 'url' => array('/dashboard/admin'))
//                                )
//                            ),
                        )
                ));
            ?>
            <?php
            $this->widget('Pmenu', array(
                'containerId' => 'stuff',
                'items' =>
                    array(
                        array(
                            'label' => 'Пользователи',
                            'url' => array('/user/admin'),
                            'icon' => 'group'
                        ),
                        array(
                            'label' => 'Группы устройств',
                            'url' => array('/group/admin'),
                            'icon' => 'folder-close'
                        ),
                        array(
                            'label' => 'Типы событий',
                            'url' => array('/eventType/admin'),
                            'icon' => 'folder-close'
                        ),
//                        array(
//                            'label' => 'Устройства',
//                            'url' => array('/device/admin'),
//                            'icon' => 'list-ol'
//                        ),
                        array(
                            'label' => 'DNS',
                            'url' => array('/dns/admin'),
                            'icon' => 'pushpin'
                        ),
                        array(
                            'label' => 'Провайдеры',
                            'url' => array('/provider/admin'),
                            'icon' => 'globe'
                        ),
                        array(
                            'label' => 'Очереди',
                            'url' => array('/queue/admin'),
                            'icon' => 'globe'
                        ),
                    ),
            ));

            ?>
    </div>
</div>
<!-- /sidebar -->