<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 01.09.15
 * Time: 15:00
 */

class SideMenu extends CWidget {

    public function init() {

    }

    public function run() {

        $this->widget('Pmenu', array(
            'items' =>

                array(
                    array(
                        'label' => 'Панель',
                        'url' => array('/dashboard/index1'),
                        'icon' => 'home'
                    ),

                    array(
                        'label' => 'Устройства',
                        'url' => array('/device/admin'),
                        'icon' => 'list-ol'
                    ),
                    array(
                        'label' => 'Серверы',
                        'url' => array('/server/admin'),
                        'icon' => 'hdd'
                    ),
                    array(
                        'label' => 'Интернет',
                        'url' => array('/network/admin'),
                        'icon' => 'globe'
                    ),

                    array(
                        'label' => 'Сообщения <strong>4</strong>' ,
                        'url' => '#',

                        'items' => array(
                            array('label' => 'Home', 'url' => array('/dashboard/index')),
                            array('label' => 'Settings', 'url' => array('/dashboard/admin'))
                        )
                    ),
                ),
            'htmlOptions' => array(
                'class' => 'navigation widget',
            )
        ));
    }
}