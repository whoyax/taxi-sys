<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 01.09.15
 * Time: 15:00
 */

class SideMenu2 extends CWidget {

    public function init() {

    }

    public function run() {
        echo PnHtml::menu(
            array(
                array(
                    'label' => 'Пользователи',
                    'url' => '/user/admin',
                    'icon' => 'group'
                ),
                array(
                    'label' => 'Группы устройств',
                    'url' => '/group/admin',
                    'icon' => 'folder-close'
                ),
//                array(
//                    'label' => 'Устройства',
//                    'url' => '/device/admin',
//                    'icon' => 'list-ol'
//                ),
//                array(
//                    'label' => 'Серверы',
//                    'url' => '/server/admin',
//                    'icon' => 'hdd'
//                ),
                array(
                    'label' => 'DNS',
                    'url' => '/dns/admin',
                    'icon' => 'pushpin'
                ),
                array(
                    'label' => 'Провайдеры',
                    'url' => '/provider/admin',
                    'icon' => 'globe'
                ),
                array(
                    'label' => 'Очереди',
                    'url' => '/queue/admin',
                    'icon' => 'globe'
                ),
            ),
            array(
                'class' => 'navigation widget',
            )
        );
    }
}