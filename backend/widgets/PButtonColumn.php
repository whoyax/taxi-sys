<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 04.09.15
 * Time: 17:19
 */

class PButtonColumn extends TbButtonColumn {

    public $template = '<ul class="navbar-icons1 table-controls"> <li>{view}</li> <li>{update}</li> <li>{delete}</li></ul>';

    public $updateButtonIcon = 'icon-cog';

    public $buttons = array(
        'view' => array(
            'visible' => 'false',
        ),
        'delete' => array(
            'visible' => 'false',
        ),
    );

    protected function initDefaultButtons() {
        parent::initDefaultButtons();

        if ( !empty($this->buttons['view']) ) {
            $this->buttons['view']['options']['class'] = 'btn tip view';
        }

        if ( !empty($this->buttons['update']) ) {
            $this->buttons['update']['options']['class'] = 'btn tip update';
        }

        if ( !empty($this->buttons['delete']) ) {
            $this->buttons['delete']['options']['class'] = 'btn tip delete';
        }
    }
}