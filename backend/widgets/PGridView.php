<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 04.09.15
 * Time: 17:19
 */

class PGridView extends TbGridView {

    public $widgetHeader = 'items';

    public $htmlOptions = array('class' => 'dataTables_wrapper');

    public $filterPosition = 'header';
    public $filter = null;

    public $addButton = array(
        'url' => 'create',
        'icon' => 'plus',
        'htmlOptions' => array(
            'class' => 'btn pull-right btn-success ',
        )
    );

    public $itemsCssClass = 'table table-striped table-bordered table-checks dataTable items';

    public $template = "
    <div class='widget row-fluid'>
        <div class='navbar'>
            <div class='navbar-inner'>
                <h6>{widgetHeader}</h6>
                {createButton}
            </div>
        </div>

        <div class='table-overflow'>
            {items}\n
            <div class=\"datatable-footer\">
                <div class=\"dataTables_info\">{summary}</div>
                <div class=\"dataTables_paginate paging_full_numbers\">{pager}</div>
            </div>
        </div>
    </div>
        ";


    public function renderWidgetHeader() {
        echo $this->widgetHeader;
    }

    public function renderCreateButton() {
        if( $this->addButton && is_array($this->addButton) )
            echo TbHtml::link(TbHtml::icon($this->addButton['icon']), $this->addButton['url'], $this->addButton['htmlOptions']);
    }
}