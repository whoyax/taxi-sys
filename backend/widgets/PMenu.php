<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 07.09.15
 * Time: 14:06
 */

Yii::import('zii.widgets.CMenu');

class PMenu extends CMenu {

    public $encodeLabel = false;

    public $activateItems=true;

    public $activateParents=true;

    public $activated = false;

    public $containerId = false;

    public $htmlOptions = array('class' => 'navigation widget');

    protected function renderMenuItem($item)
    {
        $options = isset($item['linkOptions']) ? $item['linkOptions'] : array();
        if(isset($item['items'])) {
            TbHtml::addCssClass('expand', $options);
        }

        $label=$this->linkLabelWrapper===null ? $item['label'] : CHtml::tag($this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $item['label']);

        $icon = TbArray::popValue('icon', $item);
        if (!empty($icon)) {
            $label = TbHtml::icon($icon) . ' ' . $label;
        }

        if(isset($item['url']))
        {

            return CHtml::link($label,$item['url'],$options);
        }
        else
            return CHtml::tag('span', $options, $item['label']);
    }

    protected function renderMenu($items)
    {
        echo CHtml::openTag('div', array('id' => $this->containerId));
        if(count($items))
        {
            echo CHtml::openTag('ul',$this->htmlOptions)."\n";
            $this->renderMenuRecursive($items);
            echo CHtml::closeTag('ul');
        }
        echo CHtml::closeTag('div');

        if( $this->containerId !=='general' && $this->activated ) {
            echo "<script>
            $('.sidebar-tabs').easytabs('select', '#{$this->containerId}')
            </script>";
        }
    }


    protected function renderMenuRecursive($items,  $subLevel = false)
    {
        $count=0;
        $n=count($items);
        foreach($items as $item)
        {
            $count++;
            $options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
            $class=array();
            if($item['active'] && $this->activeCssClass!='') {
                if(!$subLevel) {
                    $class[]=$this->activeCssClass;
                } else {
                   // $item['linkOptions'] = array('class' => 'current');
                }
            }

            if($count===1 && $this->firstItemCssClass!==null)
                $class[]=$this->firstItemCssClass;
            if($count===$n && $this->lastItemCssClass!==null)
                $class[]=$this->lastItemCssClass;
            if($this->itemCssClass!==null)
                $class[]=$this->itemCssClass;
            if($class!==array())
            {
                if(empty($options['class']))
                    $options['class']=implode(' ',$class);
                else
                    $options['class'].=' '.implode(' ',$class);
            }

            echo CHtml::openTag('li', $options);

            $menu=$this->renderMenuItem($item);
            if(isset($this->itemTemplate) || isset($item['template']))
            {
                $template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template,array('{menu}'=>$menu));
            }
            else
                echo $menu;

            if(isset($item['items']) && count($item['items']))
            {
                echo "\n".CHtml::openTag('ul',isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions)."\n";
                $this->renderMenuRecursive($item['items'], true);
                echo CHtml::closeTag('ul')."\n";
            }

            echo CHtml::closeTag('li')."\n";
        }
    }

    protected function normalizeItems($items,$route,&$active)
    {
        foreach($items as $i=>$item)
        {
            if(isset($item['visible']) && !$item['visible'])
            {
                unset($items[$i]);
                continue;
            }
            if(!isset($item['label']))
                $item['label']='';

            $encodeLabel = isset($item['encodeLabel']) ? $item['encodeLabel'] : $this->encodeLabel;
            if($encodeLabel)
                $items[$i]['label']=CHtml::encode($item['label']);

            $hasActiveChild=false;
            if(isset($item['items']))
            {
                $items[$i]['items']=$this->normalizeItems($item['items'],$route,$hasActiveChild);
                if(empty($items[$i]['items']) && $this->hideEmptyItems)
                {
                    unset($items[$i]['items']);
                    if(!isset($item['url']))
                    {
                        unset($items[$i]);
                        continue;
                    }
                }
            }
            if(!isset($item['active']))
            {
                if($this->activateParents && $hasActiveChild) {
                    $active=$items[$i]['active']=true;
                    $items[$i]['linkOptions']['id'] = 'current';
                    $this->activated = true;
                } elseif($this->activateItems && $this->isItemActive($item,$route)) {
                    $active=$items[$i]['active']=true;
                    $this->activated = true;
                } else
                    $items[$i]['active']=false;
            }
            elseif($item['active']) {
                $active=true;
                $this->activated = true;
            }

        }
        return array_values($items);
    }

    protected function isItemActive($item,$route)
    {
        $route = preg_replace('/update|create/', 'admin', $route);
//        var_dump($route); exit;
        if(isset($item['url']) && is_array($item['url']) && !strcasecmp(trim($item['url'][0],'/'),$route))
        {
            unset($item['url']['#']);
            if(count($item['url'])>1)
            {
                foreach(array_splice($item['url'],1) as $name=>$value)
                {
                    if(!isset($_GET[$name]) || $_GET[$name]!=$value)
                        return false;
                }
            }
            return true;
        }
        return false;
    }
} 