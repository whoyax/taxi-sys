<?php

/**
 * This is the model class for table "provider".
 *
 * The followings are the available columns in table 'provider':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $gate
 * @property string $ping_host
 * @property string $ext_ip
 * @property integer $ping_timeout
 * @property integer $ping_num
 * @property string $alias
 * @property integer $status
 */
class Provider extends CActiveRecord {

    private $_current;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'provider';
    }

    public function beforeSave()
    {
        if ( $this->status == -1 )
        {
            $this->status_mode = NetworkStatus::MODE_AUTO;
        }
        else
        {
            $this->status_mode = NetworkStatus::MODE_MANUAL;
        }

        return parent::beforeSave();
    }

    public function afterSave()
    {
        NetworkHelper::providerWorkerRestart();

        return parent::afterSave();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, alias, interface', 'required'),
            array('id, ping_timeout, ping_num, status, status_mode', 'numerical', 'integerOnly' => true),
            array('name, gate, ping_host, ext_ip, interface', 'length', 'max' => 50),
            array('alias', 'length', 'max' => 100),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, description, gate, ping_host, ext_ip, ping_timeout, ping_num, alias, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pings' => array(self::HAS_MANY, 'Ping', 'provider_id', 'index' => 'position'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'gate' => 'Шлюз',
            'ping_host' => 'Внешний хост для проверок',
            'ext_ip' => 'Внешний Ip',
            'ping_timeout' => 'Ping таймаут',
            'ping_num' => 'Попытки',
            'alias' => 'Псевдоним',
            'status' => 'Статус',
            'status_mode' => 'Режим статуса',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('gate', $this->gate, true);
        $criteria->compare('ping_host', $this->ping_host, true);
        $criteria->compare('ext_ip', $this->ext_ip, true);
        $criteria->compare('ping_timeout', $this->ping_timeout);
        $criteria->compare('ping_num', $this->ping_num);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Provider the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusText()
    {
        return NetworkStatus::getStatusText($this->getStatus());
    }

    public function getStatusLabel()
    {
        return NetworkStatus::getStatusLabel($this->getStatus());
    }


    public function getPingTimes()
    {
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT time_result FROM ping WHERE provider_id = {$this->id} ORDER BY time DESC LIMIT 20");

        return $command->queryColumn();
    }


    public static function getIds()
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT id FROM provider;");

        return $command->queryColumn();
    }

    public static function getIdsWithPing()
    {
        $providers = self::model()->findAll();
        $res = array();
        foreach ( $providers as $provider )
        {

            $res[] = array(
                'id' => $provider->id,
                'pings' => $provider->getPingTimes(),
                'rx' => NetworkHelper::getStatsRx($provider->interface),
                'tx' => NetworkHelper::getStatsTx($provider->interface),
                'status' => NetworkStatus::getStatusLabel($provider->getStatus(), $provider->status_mode),
                'loss' => $provider->getLoss(),
            );
        }

        return $res;
    }

    public function getLoss()
    {
        return round($this
            ->getDbConnection()
            ->createCommand("SELECT COALESCE(avg(i.loss), -1) FROM
             (SELECT p.loss FROM ping p WHERE p.provider_id = {$this->id} ORDER BY p.time DESC LIMIT 40) i;")
            ->queryScalar(), 2);
    }

    public function getCurrentStatus()
    {
        if ( !$this->_current )
        {
            if ( !$this->_current = ProviderStatus::model()->findByPk($this->id) )
            {
                $this->_current = new ProviderStatus();
                $this->_current->id = $this->id;
                $this->_current->status = NetworkStatus::UNKNOWN;
            }
        }

        return $this->_current;
    }

    /**
     * @param integer $status
     *
     * @return bool
     */
    public function setStatus($status)
    {
        $this->getCurrentStatus()->status = $status;

        return $this->getCurrentStatus()->save();
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return ( $this->status_mode == NetworkStatus::MODE_MANUAL )
            ? $this->status
            : $this->getCurrentStatus()->status;
    }

}
