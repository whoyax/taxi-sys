<?php

/**
 * This is the model class for table "device".
 *
 * The followings are the available columns in table 'device':
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property string $ip
 * @property integer $queue_id
 * @property integer $parent_id
 * @property string $description
 * @property string $login
 * @property string $pass
 * @property string $server_api
 *
 * The followings are the available model relations:
 * @property Group $group
 * @property Message[] $messages
 * @property Ping[] $pings
 */
class Device extends EActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, ip', 'required'),
			array('group_id, queue_id, parent_id, type, port', 'numerical', 'integerOnly'=>true),
			array('name, interface', 'length', 'max'=>100),
			array('ip, alias, login, pass, server_api', 'length', 'max'=>50),
            array('description, parent_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, group_id, name, ip, queue_id, description, login, pass, server_api', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'group' => array(self::BELONGS_TO, 'Group', 'group_id'),
			'queue' => array(self::BELONGS_TO, 'Queue', 'queue_id'),
			'messages' => array(self::HAS_MANY, 'Message', 'device_id'),
			'pings' => array(self::HAS_MANY, 'Ping', 'device_id'),
			'pingStatus' => array(self::BELONGS_TO, 'PingStatus', 'ip'),
			'access' => array(self::HAS_MANY, 'DeviceAccess', 'device_id'),
			'ips' => array(self::HAS_MANY, 'DeviceIp', 'device_id'),
			'params' => array(self::HAS_MANY, 'DeviceParam', 'device_id', 'index' => 'name'),
			'dependents' => array(self::HAS_MANY, 'DeviceDependent', 'device_id', 'index' => 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Группа',
			'parent_id' => 'Главное устройство',
			'type' => 'Тип устройства',
			'name' => 'Название',
			'ip' => 'Ip',
			'port' => 'Порт',
			'queue_id' => 'Очередь',
            'queue.name' => 'Очередь',
			'description' => 'Описание',
			'login' => 'Логин',
			'pass' => 'Пароль',
			'server_api' => 'Server Api',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('queue_id',$this->queue_id);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 100
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Device the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function findOrCreateVm($name, $hostId) {
        $vm = self::model()->with(array('params'))->findByAttributes(array('alias' => $name, 'parent_id' => $hostId));
        if (!$vm) {
            $vm = new self();
            $vm->setAttributes(
                array(
                    'name' => $name,
                    'alias' => $name,
                    'ip' => '0.0.0.0',
                    'group_id' => Group::getVmGroup(),
                    'type' => DeviceType::VIRTUAL,
                    'parent_id' => $hostId
                )
            );
            $vm->save(false);
        }
        return $vm;
    }

    public static function findOrCreateWorkstation($name) {
        $vm = self::model()->with(array('params'))->findByAttributes(array('alias' => $name, 'type' => DeviceType::WORKSTATION));
        if (!$vm) {
            $vm = new self();
            $vm->setAttributes(
                array(
                    'name' => $name,
                    'alias' => $name,
                    'ip' => '0.0.0.0',
                    'group_id' => Group::getWorkstationGroup(),
                    'type' => DeviceType::WORKSTATION,
                )
            );
            $vm->save(false);
        }
        return $vm;
    }

    public function updateIpInfo($ip, $networkName)
    {
        if($this->ip == $ip)
            return true;

        if(!$this->ip || $this->ip=='0.0.0.0')
        {
            $this->ip = $ip;
            return $this->save();
        }

        foreach($this->ips as $adr)
        {
            if ($adr->ip == $ip)
                return true;
        }

        $adr = new DeviceIp();
        $adr->setAttributes(
            array(
                'device_id' => $this->id,
                'ip' => $ip,
                'name' => $networkName
            )
        );

        return $adr->save();
    }

    public function getParam($key)
    {
        if(isset($this->params[$key])) {
            return $this->params[$key];
        }

        $param = new DeviceParam();
        $param->device_id = $this->id;
        $param->name = $key;

        return $param;
    }

    public static function getHStat($id){

        $h = self::model()->findByPk($id);

            $res = array(
//                'id' => $h->id,
                'cpu' => $h->getHCpuStats(),
                'networks' => $h->getNetworksStat(),
//                'rx' =>$h->getStatsRx(),
//                'tx' =>$h->getStatsTx(),
//                'status' => NetworkStatus::getStatusLabel($provider->status, $provider->status_mode),
//                'managementTx' => $provider->getLoss(),
            );

        return $res;
    }

    public function getNetworksStat() {
        $stat = array();
        foreach($this->getNetworks() as $network) {
            $stat[] = array(
                'id' => "#h_{$this->id}_{$network['interface']}_bandwidth",
                'rx' => NetworkHelper::getStatsRx($network['interface']),
                'tx' => NetworkHelper::getStatsTx($network['interface']),
            );
        }
        return $stat;
    }

    public function getHCpuStats(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT value FROM device_param_stat WHERE device_id = {$this->id} AND name='summary.quickStats.overallCpuUsage' ORDER BY time DESC LIMIT 20");
        $ticks = $command->queryColumn();
        $capacity = $this->getCpuCapacity();

        $new = array();

        foreach($ticks as $tick) {
            $new[] = round(($tick/$capacity) * 100, 2);
        }

        return $new;
    }

    public function getVmCpuStat(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT value FROM device_param_stat WHERE device_id = {$this->id} AND name='summary.quickStats.overallCpuUsage' ORDER BY time DESC LIMIT 20");
        return $command->queryColumn();
    }

    public function getVmMemStat(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT value FROM device_param_stat WHERE device_id = {$this->id} AND name='summary.quickStats.guestMemoryUsage' ORDER BY time DESC LIMIT 20");
        return $command->queryColumn();
    }

    public function getUpsLoadStat(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT value FROM device_param_stat WHERE device_id = {$this->id} AND name='ups.load' ORDER BY time DESC LIMIT 20");
        return $command->queryColumn();
    }

    public function getMikroCpuStat(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT UNIX_TIMESTAMP(`time`)*1000 as stamp, value FROM device_param_stat WHERE device_id = {$this->id} AND name='cpu-used' ORDER BY time DESC LIMIT 20");
        return $command->queryAll();

    }

    public function getMikroCpuCurrent(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT value FROM device_param_stat WHERE device_id = {$this->id} AND name='cpu-used' ORDER BY time DESC LIMIT 1");
        return $command->queryScalar();
    }


    public function getMikroMemStat(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT UNIX_TIMESTAMP(`time`)*1000 as stamp, round(100-(value/(128*1024))*100, 2) as value FROM device_param_stat WHERE device_id = {$this->id} AND name='free-memory' ORDER BY time DESC LIMIT 20");
        return $command->queryAll();
    }

    public function getMikroPortLoad(){
        $connection = $this->getDbConnection();
        $command = $connection->createCommand(
            "SELECT
                port.value,
                COALESCE(round(load.rx/1024, 1), 0) as rx,
                COALESCE(round(load.tx/1024,1), 0) as tx,
                COALESCE(round(load.rx/(1024*1024), 2), 0) as rxLoad,
                COALESCE(round(load.tx/(1024*1024), 2), 0) as txLoad
            FROM
              device_param port
            LEFT JOIN
              (select * FROM interface_stat l ORDER BY time DESC, interface ) `load`
            ON port.value=load.interface

              #interface_stat `load` ON port.value=load.interface AND time = (SELECT max(`time`) FROM interface_stat WHERE interface=load.interface)

            WHERE
              device_id = {$this->id} AND name LIKE 'ports.%.name'
            GROUP BY
              port.value
            ORDER BY
              port.value ASC");
        return $command->queryAll();
    }


    public function getMemoryCapacity() {
        return
            (int)($this->getParam("summary.hardware.memorySize")->value / (1024*1024));
    }

    public function getVmMemoryCapacity() {
        return
            (int)($this->getParam("summary.config.memorySizeMB")->value);
    }

    public function getWorkstationMemoryCapacity() {
        return
            (int)($this->getParam("summary.config.memorySize")->value / (1024*1024));
    }

    public function getVmMemoryUse() {
        return
            (int)$this->getParam("summary.quickStats.guestMemoryUsage")->value;
    }

    public function getWorkstationMemoryFree() {
        return
            (int)((int)$this->getParam("summary.config.memoryFree")->value / (1024*1024));
    }

    public function getCpuCapacity() {
        return
            (int)$this->getParam("summary.hardware.cpuMhz")->value
            * (int)$this->getParam("summary.hardware.numCpuPkgs")->value
            * (int)$this->getParam("summary.hardware.numCpuThreads")->value;
    }

    public function getFreeMemoryMb() {
        $free =  $this->getParam("summary.hardware.memorySize")->value
            - ($this->getParam("summary.quickStats.overallMemoryUsage")->value * 1024 * 1024);

        return  round(($free/1048576), 2);
    }

    public function getDatastoresInfo() {
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT name, value FROM device_param WHERE device_id = {$this->id} AND name LIKE 'datastore.%'");
        $params = $command->queryAll();
        $stores = array();
        foreach($params as $param) {
            $keys = explode(".", $param['name']);
            $stores[$keys[1]][$keys[2]] = $param['value'];
        }
        return $stores;
    }

    public function getNetworks() {
        $connection = $this->getDbConnection();
        $command = $connection->createCommand("SELECT name, value FROM device_param WHERE device_id = {$this->id} AND name LIKE 'network.%'");
        $params = $command->queryAll();
        $networks = array();
        foreach($params as $param) {
            $keys = explode(".", $param['name']);
            $networks[$keys[1]][$keys[2]] = $param['value'];
        }
        return $networks;
    }


    public function getUptime() {
        if($t = (int)$this->getParam("summary.quickStats.uptime")->value) {
            $days = floor($t/60*60*24);
            $hours = floor(($t - $days*24*60*60) / 60*60);
            $minuts = floor(($t - $days*24*60*60 - $hours*60*60)/60);
            $seconds = $t - $days*24*60*60 - $hours*60*60 - $minuts*60;
        }
        return "{$days} {$hours}:{$minuts}:{$seconds}";
    }

}
