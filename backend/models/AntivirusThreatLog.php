<?php

/**
 * This is the model class for table "ThreatLog".
 *
 * The followings are the available columns in table 'ThreatLog':
 * @property string $ID
 * @property integer $ReplicateUp
 * @property integer $ClientSectionID
 * @property string $DateReceived
 * @property string $DateOccurred
 * @property string $UserName
 * @property integer $FTScannerID
 * @property string $LogLevel
 * @property integer $FTHThreatObjectHASH
 * @property string $Name
 * @property string $Virus
 * @property integer $FTHActionTakenHASH
 * @property string $Info
 * @property integer $ClientID
 */
class AntivirusThreatLog extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ThreatLog';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ClientSectionID, FTHThreatObjectHASH, FTHActionTakenHASH, ClientID', 'required'),
            array('ReplicateUp, ClientSectionID, FTScannerID, FTHThreatObjectHASH, FTHActionTakenHASH, ClientID', 'numerical', 'integerOnly' => true),
            array('ID', 'length', 'max' => 20),
            array('UserName', 'length', 'max' => 50),
            array('LogLevel', 'length', 'max' => 10),
            array('Name, Info', 'length', 'max' => 255),
            array('Virus', 'length', 'max' => 100),
            array('DateReceived, DateOccurred', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ReplicateUp, ClientSectionID, DateReceived, DateOccurred, UserName, FTScannerID, LogLevel, FTHThreatObjectHASH, Name, Virus, FTHActionTakenHASH, Info, ClientID', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'client' => array(self::BELONGS_TO, 'AntivirusClient', 'ClientID')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'ID',
            'ReplicateUp' => 'Replicate Up',
            'ClientSectionID' => 'Client Section',
            'DateReceived' => 'Дата получения',
            'DateOccurred' => 'Дата происшествия',
            'UserName' => 'Пользователь',
            'FTScannerID' => 'Ftscanner',
            'LogLevel' => 'Log Level',
            'FTHThreatObjectHASH' => 'Fththreat Object Hash',
            'Name' => 'Путь',
            'Virus' => 'Классификация',
            'FTHActionTakenHASH' => 'Fthaction Taken Hash',
            'Info' => 'Информация',
            'ClientID' => 'Client',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID, true);
        $criteria->compare('ReplicateUp', $this->ReplicateUp);
        $criteria->compare('ClientSectionID', $this->ClientSectionID);
        $criteria->compare('DateReceived', $this->DateReceived, true);
        $criteria->compare('DateOccurred', $this->DateOccurred, true);
        $criteria->compare('UserName', $this->UserName, true);
        $criteria->compare('FTScannerID', $this->FTScannerID);
        $criteria->compare('LogLevel', $this->LogLevel, true);
        $criteria->compare('FTHThreatObjectHASH', $this->FTHThreatObjectHASH);
        $criteria->compare('Name', $this->Name, true);
        $criteria->compare('Virus', $this->Virus, true);
        $criteria->compare('FTHActionTakenHASH', $this->FTHActionTakenHASH);
        $criteria->compare('Info', $this->Info, true);
        $criteria->compare('ClientID', $this->ClientID);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->era;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return AntivirusThreatLog the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
