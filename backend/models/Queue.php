<?php

/**
 * This is the model class for table "queue".
 *
 * The followings are the available columns in table 'queue':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $active
 *
 * @property QueueProvider[] $positions\
 * @property QueueProvider $activeProvider
 *
 */
class Queue extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'queue';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, description, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'providers' => array(self::HAS_MANY, 'QueueProvider', 'queue_id', 'index' => 'position'),
            'positions' => array(self::HAS_MANY, 'QueueProvider', 'queue_id', 'index' => 'position'),
            'activeProvider' => array(self::BELONGS_TO, 'QueueProvider', 'active',)
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'active' => 'Активный провайдер',
            'activeProvider.name' => 'Активный провайдер',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Queue the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getOptionList()
    {
        return
            array('' => 'Default')
            + TbHtml::listData(static::model()->findAll(), 'id', 'name');
    }

    /**
     * @param QueueProvider $p
     *
     * @return bool
     */
    public function toggleTo(QueueProvider $p)
    {
        $providerMark = $this->activeProvider->provider->alias;
        NetworkHelper::removeConnectionsForProvider($providerMark);

        $result = NetworkHelper::manyToList($p->provider->alias, $this->getAddresses());

        if ( $result )
        {
            if ( $this->getDbConnection()
                ->createCommand("update queue set active=:active where id = :id")
                ->execute(array(':active' => $p->id, ':id' => $this->id)) )
            {
                $this->active = $p->id;
                if( $dns = DnsSettings::model()->findAllByAttributes(array('queue_id' => $this->id, 'auto_dns' => 1)) ) {
                    foreach($dns as $domain) {
                        DnsHelper::markToChangeIp($domain, $p->provider->ext_ip);
                    }
                }
            }
            else
            {
                $result = false;
            }
        }

        $message = ( $result )
            ? "\r\n Q#{$this->id} '{$this->name}' toggled to #{$p->id}:{$p->provider_id} '{$p->provider->name}' \r\n"
            : "\r\n Error on change active queue provider. Q#{$this->id} '{$this->name}' toggled to #{$p->id}:{$p->provider_id} '{$p->provider->name}' \r\n";

        $title = ( $result )
            ? 'Переключение провайдера в очереди ' . $this->name
            : 'Неудачная попытка переключения провайдера в очереди ' . $this->name;

        Event::storeWatchdogMessage($title, $message, ($result) ? Event::TYPE_INTERNET : Event::TYPE_PROGRAM_ERROR);

        return $result;
    }


    /**
     * @param array $cache
     *
     * @return null|QueueProvider
     */
    public function getBestProvider($cache = array())
    {
        if ( !$cache )
        {
            foreach ( $this->positions as $position )
            {
                $cache[$position->provider_id] = $position->provider;
            }
        }
        $positions = $this->positions;

        $get = function ($status) use ($cache, $positions)
        {
            foreach ( $positions as $p )
            {
                if ( !empty( $cache[$p->provider_id] )
                    && $cache[$p->provider_id]->getStatus() == $status
                )
                {
                    return $p;
                }
            }

            return null;
        };

        foreach ( array(
                      NetworkStatus::BEST,
                      NetworkStatus::FINE,
                      NetworkStatus::GOOD,
                      NetworkStatus::POOR
                  ) as $status )
        {
            if ( $p = $get($status) )
            {
                return $p;
            }
        }

        return null;
    }


    public function getDevices()
    {
        return Device::model()
            ->with('group')
            ->findAll("t.queue_id = {$this->id} OR ((t.queue_id IS NULL) AND group.queue_id = {$this->id})");
    }

    public function getAddresses()
    {
        $devices = array();
        foreach ( $this->getDevices() as $d )
        {
            $devices[] = $d->ip;
        }

        return $devices;
    }
}
