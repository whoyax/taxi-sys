<?php

/**
 * This is the model class for table "device_param_stat".
 *
 * The followings are the available columns in table 'device_param_stat':
 * @property string $time
 * @property integer $device_id
 * @property string $name
 * @property string $value
 */
class DeviceParamStat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_param_stat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('device_id, name, value', 'required'),
			array('device_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('value', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('time, device_id, name, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'time' => 'Time',
			'device_id' => 'Device',
			'name' => 'Name',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('time',$this->time,true);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceParamStat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function saveResult($deviceId, $name, $result) {
        $model = new self();
        $model->setAttributes(
            array(
                'device_id' => $deviceId,
                'name' => $name,
                'value' => $result
            )
        );

        if( !$result = $model->save() ) {
            Yii::log('Cannot save param stat result : '
                . var_export($model->getErrors(), true) . var_export($model->getAttributes(), 1),
                CLogger::LEVEL_ERROR, 'application' );
            Yii::getLogger()->flush(true);
        }

        return $result;
    }
}
