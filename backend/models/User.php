<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $role
 * @property string $email
 * @property string $login
 * @property string $name
 * @property string $password
 * @property string $phone
 */
class User extends EActiveRecord {

    const ROLE_UNKNOWN = 0;
    const ROLE_ADMIN = 1;
    const ROLE_DISPATCHER = 2;

    static $roles = array(
        // self::ROLE_UNKNOWN => 'Неизвестно',
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_DISPATCHER => 'Диспетчер',
    );

    public static function getRolesList() {
        return static::$roles;
    }

    public function getRoleTitle() {
        if (!empty(static::$roles[$this->role])) {
            return static::$roles[$this->role];
        }

        return 'Неизвестно';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, role, login, name, password, phone', 'required'),
            array('email', 'length', 'max' => 250),
            array('email', 'email'),
            array('login, name, password', 'length', 'max' => 100),
            array('phone', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, email, login, name, password, phone', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email' => 'Email',
            'login' => 'Логин',
            'name' => 'Имя',
            'role' => 'Роль',
            'password' => 'Пароль',
            'phone' => 'Телефон',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('phone', $this->phone, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getTitle() {
        return $this->name;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getWatchdogId() {
        return -1;
    }
}
