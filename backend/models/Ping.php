<?php

/**
 * This is the model class for table "ping".
 *
 * The followings are the available columns in table 'ping':
 * @property integer $id
 * @property integer $device_id
 * @property integer $provider_id
 * @property integer $result
 * @property string $time
 * @property integer $time_result
 * @property string $status
 * @property integer $loss
 *
 * The followings are the available model relations:
 * @property Device $device
 */
class Ping extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ping';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('loss', 'required'),
			array('device_id, provider_id, result, time_result, loss', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, device_id, provider_id, result, time, time_result, status, loss', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'device' => array(self::BELONGS_TO, 'Device', 'device_id'),
			'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'device_id' => 'Device',
			'provider_id' => 'Provider',
			'result' => 'Result',
			'time' => 'Time',
			'time_result' => 'Time Result',
			'status' => 'Status',
			'loss' => 'Loss',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('provider_id',$this->provider_id);
		$criteria->compare('result',$this->result);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('time_result',$this->time_result);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('loss',$this->loss);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ping the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @param Provider $provider
     * @param $result
     *
     * @return mixed
     */
    public static function saveProviderResult(Provider $provider, $result) {
        $model = new self();
        $model->setAttributes(
            array(
                'provider_id' => $provider->id,
                'time_result' => str_replace('ms', '', $result->getProperty('time')),
                'status' => $result->getProperty('status'),
                'loss' => $result->getProperty('packet-loss'),
                'result' => (int) ($result->getProperty('received') && ($result->getProperty('received')==$result->getProperty('sent')))
            )
        );

        if( !$result = $model->save() ) {
            Yii::log('Cannot save _provider_ ping result : ' . var_export($model->getErrors(), true), CLogger::LEVEL_ERROR, 'application' );
            Yii::getLogger()->flush(true);
        }

        return $result;
    }

	public static function addNotAvailableProviderResult(Provider $provider) {
		$model = new self();
		$model->setAttributes(
			array(
				'provider_id' => $provider->id,
				'time_result' => 0,
				'status' => null,
				'loss' => 100,
				'result' => 0
			)
		);

		if( !$result = $model->save() ) {
			Yii::log('Cannot save ping result : ' . var_export($model->getErrors(), true), CLogger::LEVEL_ERROR, 'application' );
			Yii::getLogger()->flush(true);
		}

		return $result;
	}
}
