<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property integer $user_id
 * @property integer $type_id
 * @property string $title
 * @property string $body
 */
class Event extends EActiveRecord {

    const TYPE_HARDWARE_ERROR = 1;
    const TYPE_PROGRAM_ERROR = 2;
    const TYPE_INTERNET = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, type_id, title, body', 'required'),
            array('user_id, type_id', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, type_id, title, body, date_create', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'type' => array(self::BELONGS_TO, 'EventType', 'type_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'type_id' => 'Тип',
            'title' => 'Название',
            'body' => 'Описание',
            'date_create' => 'Дата создания',
            'type.title' => 'Тип события',
            'user.title' => 'Пользователь',

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('type_id', $this->type_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('date_create', $this->date_create, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20
            ),
            'sort' => array(
                'defaultOrder' => 'date_create DESC'
            )

        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Event the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param string $title
     * @param string $message
     * @param int $type
     *
     * @return bool
     *
     */
    public static function storeWatchdogMessage($title, $message, $type = self::TYPE_PROGRAM_ERROR ) {
        $event = new self();
        $event->setAttributes(array(
            'user_id' => User::getWatchdogId(),
            'type_id' => $type,
            'title' => $title,
            'body' => $message,
        ));

        $result = $event->save();
        if(!$result) {
            var_dump($event->getErrors());
        }

        return $result;
    }
}
