<?php

/**
 * This is the model class for table "interface_stat".
 *
 * The followings are the available columns in table 'interface_stat':
 * @property string $time
 * @property string $interface
 * @property integer $rx
 * @property integer $tx
 * @property integer $rx_sum
 * @property integer $tx_sum
 */
class InterfaceStat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'interface_stat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('interface, rx, tx', 'required'),
			array('rx, tx, rx_sum, tx_sum', 'numerical', 'integerOnly'=>true),
			array('interface', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('time, interface, rx, tx, rx_sum, tx_sum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'time' => 'Time',
			'interface' => 'Interface',
			'rx' => 'Rx',
			'tx' => 'Tx',
			'rx_sum' => 'Rx Sum',
			'tx_sum' => 'Tx Sum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('time',$this->time,true);
		$criteria->compare('interface',$this->interface,true);
		$criteria->compare('rx',$this->rx);
		$criteria->compare('tx',$this->tx);
		$criteria->compare('rx_sum',$this->rx_sum);
		$criteria->compare('tx_sum',$this->tx_sum);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InterfaceStat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function saveStat($result) {
        $model = new self();
        $model->setAttributes(
            array(
                'interface' => $result->getProperty('name'),
                'rx' => $result->getProperty('rx-bits-per-second'),
                'tx' => $result->getProperty('tx-bits-per-second'),
            )
        );

        if( !$result = $model->save() ) {
            Yii::log('Cannot save bandwidth result : '
                . var_export($model->getErrors(), true) . var_export($model->getAttributes(), 1),
                'application' );
            Yii::getLogger()->flush(true);
        }

        return $result;
    }
}
