<?php

/**
 * This is the model class for table "Client".
 *
 * The followings are the available columns in table 'Client':
 * @property integer $ID
 * @property string $Name
 * @property string $ComputerName
 * @property string $MacAddress
 * @property string $MacPrevious
 * @property string $ClientIID
 * @property string $PrimaryServer
 * @property string $FromServer
 * @property integer $ReplicateUp
 * @property integer $ReplicateDown
 * @property integer $IsForeign
 * @property string $IP
 * @property string $ClientDomain
 * @property integer $DomainType
 * @property string $OSName
 * @property integer $OSNumber
 * @property integer $OSPlatform
 * @property string $OSPlatformName
 * @property integer $HWPlatform
 * @property string $HWPlatformName
 * @property string $ProductLastStarted
 * @property integer $SectionID
 * @property integer $PolicyPrimaryID
 * @property string $PolicyPrimaryServer
 * @property integer $PolicyCRC
 * @property string $PolicyApplyDate
 * @property string $ThreatDBVersionName
 * @property integer $ThreatDBVersionNumber
 * @property integer $ThreatDBVersionBuild
 * @property string $ThreatDBVersionDate
 * @property integer $ThreatEngineType
 * @property string $ProductName
 * @property string $ProductVersion
 * @property string $ClientLastConnected
 * @property string $ClientLastInstalled
 * @property string $Configuration
 * @property string $ConfigurationDate
 * @property string $ConfigurationRDate
 * @property integer $ConfigurationCRC
 * @property string $Information
 * @property string $InformationDate
 * @property string $InformationRDate
 * @property integer $InformationCRC
 * @property string $PluginState
 * @property string $PluginStateDate
 * @property string $PluginStateRDate
 * @property integer $PluginStateCRC
 * @property string $Features
 * @property string $FeaturesDate
 * @property string $FeaturesRDate
 * @property integer $FeaturesCRC
 * @property string $Sysinfo
 * @property string $SysinfoDate
 * @property string $SysinfoRDate
 * @property integer $SysinfoCRC
 * @property string $SysinfoExR
 * @property string $SysinfoExD
 * @property string $PluginStateShort
 * @property integer $PluginStateLevel
 * @property integer $WaitingForRestart
 * @property string $WaitingForRestartDate
 * @property string $LastThreatText
 * @property string $LastFirewallText
 * @property string $LastEventText
 * @property string $LastEventTextLevel
 * @property integer $LastScanScanned
 * @property integer $LastScanInfected
 * @property integer $LastScanCleaned
 * @property string $LastScanDate
 * @property string $TaskText
 * @property integer $MobileUser
 * @property integer $NewClient
 * @property string $LastModifiedUserName
 * @property string $LastModifiedUserPrimaryServer
 * @property string $LastModifiedUserDate
 * @property string $UserComment
 * @property string $IPn
 * @property string $CustomInfo
 * @property string $CustomInfo2
 * @property string $CustomInfo3
 * @property integer $CustomInfoDFlag
 * @property integer $QuarantineCRC
 * @property integer $FlagQuarFilesReq
 * @property string $ProductCode
 * @property integer $RollbackActive
 * @property string $RollbackTime
 * @property string $IPv6nHigh
 * @property string $IPv6nLow
 * @property string $LastThreatDate
 * @property integer $LastApplyPolicyPrimaryID
 * @property string $LastApplyPolicyPrimaryServer
 */
class AntivirusClient extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Name, ComputerName, MacAddress, MacPrevious, ClientIID, PrimaryServer, FromServer, SectionID, PolicyPrimaryServer', 'required'),
			array('ReplicateUp, ReplicateDown, IsForeign, DomainType, OSNumber, OSPlatform, HWPlatform, SectionID, PolicyPrimaryID, PolicyCRC, ThreatDBVersionNumber, ThreatDBVersionBuild, ThreatEngineType, ConfigurationCRC, InformationCRC, PluginStateCRC, FeaturesCRC, SysinfoCRC, PluginStateLevel, WaitingForRestart, LastScanScanned, LastScanInfected, LastScanCleaned, MobileUser, NewClient, CustomInfoDFlag, QuarantineCRC, FlagQuarFilesReq, RollbackActive, LastApplyPolicyPrimaryID', 'numerical', 'integerOnly'=>true),
			array('Name, ComputerName, PrimaryServer, FromServer, IP, PolicyPrimaryServer, ThreatDBVersionName, LastModifiedUserName, LastModifiedUserPrimaryServer, LastApplyPolicyPrimaryServer', 'length', 'max'=>50),
			array('MacAddress, MacPrevious, ClientIID', 'length', 'max'=>12),
			array('ClientDomain, SysinfoExD', 'length', 'max'=>128),
			array('OSName, ProductName', 'length', 'max'=>100),
			array('OSPlatformName, HWPlatformName', 'length', 'max'=>25),
			array('ProductVersion, ProductCode', 'length', 'max'=>30),
			array('Configuration, Information, PluginState, Features, Sysinfo', 'length', 'max'=>1),
			array('SysinfoExR, PluginStateShort, LastThreatText, LastFirewallText, LastEventText, TaskText, UserComment, CustomInfo, CustomInfo2, CustomInfo3', 'length', 'max'=>255),
			array('LastEventTextLevel', 'length', 'max'=>10),
			array('IPn, IPv6nHigh, IPv6nLow', 'length', 'max'=>20),
			array('ProductLastStarted, PolicyApplyDate, ThreatDBVersionDate, ClientLastConnected, ClientLastInstalled, ConfigurationDate, ConfigurationRDate, InformationDate, InformationRDate, PluginStateDate, PluginStateRDate, FeaturesDate, FeaturesRDate, SysinfoDate, SysinfoRDate, WaitingForRestartDate, LastScanDate, LastModifiedUserDate, RollbackTime, LastThreatDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Name, ComputerName, MacAddress, MacPrevious, ClientIID, PrimaryServer, FromServer, ReplicateUp, ReplicateDown, IsForeign, IP, ClientDomain, DomainType, OSName, OSNumber, OSPlatform, OSPlatformName, HWPlatform, HWPlatformName, ProductLastStarted, SectionID, PolicyPrimaryID, PolicyPrimaryServer, PolicyCRC, PolicyApplyDate, ThreatDBVersionName, ThreatDBVersionNumber, ThreatDBVersionBuild, ThreatDBVersionDate, ThreatEngineType, ProductName, ProductVersion, ClientLastConnected, ClientLastInstalled, Configuration, ConfigurationDate, ConfigurationRDate, ConfigurationCRC, Information, InformationDate, InformationRDate, InformationCRC, PluginState, PluginStateDate, PluginStateRDate, PluginStateCRC, Features, FeaturesDate, FeaturesRDate, FeaturesCRC, Sysinfo, SysinfoDate, SysinfoRDate, SysinfoCRC, SysinfoExR, SysinfoExD, PluginStateShort, PluginStateLevel, WaitingForRestart, WaitingForRestartDate, LastThreatText, LastFirewallText, LastEventText, LastEventTextLevel, LastScanScanned, LastScanInfected, LastScanCleaned, LastScanDate, TaskText, MobileUser, NewClient, LastModifiedUserName, LastModifiedUserPrimaryServer, LastModifiedUserDate, UserComment, IPn, CustomInfo, CustomInfo2, CustomInfo3, CustomInfoDFlag, QuarantineCRC, FlagQuarFilesReq, ProductCode, RollbackActive, RollbackTime, IPv6nHigh, IPv6nLow, LastThreatDate, LastApplyPolicyPrimaryID, LastApplyPolicyPrimaryServer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Name' => 'Клиент',
			'ComputerName' => 'Computer Name',
			'MacAddress' => 'Mac Address',
			'MacPrevious' => 'Mac Previous',
			'ClientIID' => 'Client Iid',
			'PrimaryServer' => 'Primary Server',
			'FromServer' => 'From Server',
			'ReplicateUp' => 'Replicate Up',
			'ReplicateDown' => 'Replicate Down',
			'IsForeign' => 'Is Foreign',
			'IP' => 'Ip',
			'ClientDomain' => 'Client Domain',
			'DomainType' => 'Domain Type',
			'OSName' => 'Операционная система',
			'OSNumber' => 'Osnumber',
			'OSPlatform' => 'Osplatform',
			'OSPlatformName' => 'Osplatform Name',
			'HWPlatform' => 'Hwplatform',
			'HWPlatformName' => 'Hwplatform Name',
			'ProductLastStarted' => 'Product Last Started',
			'SectionID' => 'Section',
			'PolicyPrimaryID' => 'Policy Primary',
			'PolicyPrimaryServer' => 'Policy Primary Server',
			'PolicyCRC' => 'Policy Crc',
			'PolicyApplyDate' => 'Policy Apply Date',
			'ThreatDBVersionName' => 'Версия сигнатур',
			'ThreatDBVersionNumber' => 'Threat Dbversion Number',
			'ThreatDBVersionBuild' => 'Threat Dbversion Build',
			'ThreatDBVersionDate' => 'Threat Dbversion Date',
			'ThreatEngineType' => 'Threat Engine Type',
			'ProductName' => 'Продукт',
			'ProductVersion' => 'Product Version',
			'ClientLastConnected' => 'Последний доклад',
			'ClientLastInstalled' => 'Client Last Installed',
			'Configuration' => 'Configuration',
			'ConfigurationDate' => 'Configuration Date',
			'ConfigurationRDate' => 'Configuration Rdate',
			'ConfigurationCRC' => 'Configuration Crc',
			'Information' => 'Information',
			'InformationDate' => 'Information Date',
			'InformationRDate' => 'Information Rdate',
			'InformationCRC' => 'Information Crc',
			'PluginState' => 'Plugin State',
			'PluginStateDate' => 'Plugin State Date',
			'PluginStateRDate' => 'Plugin State Rdate',
			'PluginStateCRC' => 'Plugin State Crc',
			'Features' => 'Features',
			'FeaturesDate' => 'Features Date',
			'FeaturesRDate' => 'Features Rdate',
			'FeaturesCRC' => 'Features Crc',
			'Sysinfo' => 'Sysinfo',
			'SysinfoDate' => 'Sysinfo Date',
			'SysinfoRDate' => 'Sysinfo Rdate',
			'SysinfoCRC' => 'Sysinfo Crc',
			'SysinfoExR' => 'Sysinfo Ex R',
			'SysinfoExD' => 'Sysinfo Ex D',
			'PluginStateShort' => 'Plugin State Short',
			'PluginStateLevel' => 'Plugin State Level',
			'WaitingForRestart' => 'Waiting For Restart',
			'WaitingForRestartDate' => 'Waiting For Restart Date',
			'LastThreatText' => 'Last Threat Text',
			'LastFirewallText' => 'Last Firewall Text',
			'LastEventText' => 'Last Event Text',
			'LastEventTextLevel' => 'Last Event Text Level',
			'LastScanScanned' => 'Last Scan Scanned',
			'LastScanInfected' => 'Last Scan Infected',
			'LastScanCleaned' => 'Last Scan Cleaned',
			'LastScanDate' => 'Последнее сканирование',
			'TaskText' => 'Task Text',
			'MobileUser' => 'Mobile User',
			'NewClient' => 'New Client',
			'LastModifiedUserName' => 'Last Modified User Name',
			'LastModifiedUserPrimaryServer' => 'Last Modified User Primary Server',
			'LastModifiedUserDate' => 'Last Modified User Date',
			'UserComment' => 'User Comment',
			'IPn' => 'Ipn',
			'CustomInfo' => 'Custom Info',
			'CustomInfo2' => 'Custom Info2',
			'CustomInfo3' => 'Custom Info3',
			'CustomInfoDFlag' => 'Custom Info Dflag',
			'QuarantineCRC' => 'Quarantine Crc',
			'FlagQuarFilesReq' => 'Flag Quar Files Req',
			'ProductCode' => 'Product Code',
			'RollbackActive' => 'Rollback Active',
			'RollbackTime' => 'Rollback Time',
			'IPv6nHigh' => 'Ipv6n High',
			'IPv6nLow' => 'Ipv6n Low',
			'LastThreatDate' => 'Last Threat Date',
			'LastApplyPolicyPrimaryID' => 'Last Apply Policy Primary',
			'LastApplyPolicyPrimaryServer' => 'Last Apply Policy Primary Server',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('ComputerName',$this->ComputerName,true);
		$criteria->compare('MacAddress',$this->MacAddress,true);
		$criteria->compare('MacPrevious',$this->MacPrevious,true);
		$criteria->compare('ClientIID',$this->ClientIID,true);
		$criteria->compare('PrimaryServer',$this->PrimaryServer,true);
		$criteria->compare('FromServer',$this->FromServer,true);
		$criteria->compare('ReplicateUp',$this->ReplicateUp);
		$criteria->compare('ReplicateDown',$this->ReplicateDown);
		$criteria->compare('IsForeign',$this->IsForeign);
		$criteria->compare('IP',$this->IP,true);
		$criteria->compare('ClientDomain',$this->ClientDomain,true);
		$criteria->compare('DomainType',$this->DomainType);
		$criteria->compare('OSName',$this->OSName,true);
		$criteria->compare('OSNumber',$this->OSNumber);
		$criteria->compare('OSPlatform',$this->OSPlatform);
		$criteria->compare('OSPlatformName',$this->OSPlatformName,true);
		$criteria->compare('HWPlatform',$this->HWPlatform);
		$criteria->compare('HWPlatformName',$this->HWPlatformName,true);
		$criteria->compare('ProductLastStarted',$this->ProductLastStarted,true);
		$criteria->compare('SectionID',$this->SectionID);
		$criteria->compare('PolicyPrimaryID',$this->PolicyPrimaryID);
		$criteria->compare('PolicyPrimaryServer',$this->PolicyPrimaryServer,true);
		$criteria->compare('PolicyCRC',$this->PolicyCRC);
		$criteria->compare('PolicyApplyDate',$this->PolicyApplyDate,true);
		$criteria->compare('ThreatDBVersionName',$this->ThreatDBVersionName,true);
		$criteria->compare('ThreatDBVersionNumber',$this->ThreatDBVersionNumber);
		$criteria->compare('ThreatDBVersionBuild',$this->ThreatDBVersionBuild);
		$criteria->compare('ThreatDBVersionDate',$this->ThreatDBVersionDate,true);
		$criteria->compare('ThreatEngineType',$this->ThreatEngineType);
		$criteria->compare('ProductName',$this->ProductName,true);
		$criteria->compare('ProductVersion',$this->ProductVersion,true);
		$criteria->compare('ClientLastConnected',$this->ClientLastConnected,true);
		$criteria->compare('ClientLastInstalled',$this->ClientLastInstalled,true);
		$criteria->compare('Configuration',$this->Configuration,true);
		$criteria->compare('ConfigurationDate',$this->ConfigurationDate,true);
		$criteria->compare('ConfigurationRDate',$this->ConfigurationRDate,true);
		$criteria->compare('ConfigurationCRC',$this->ConfigurationCRC);
		$criteria->compare('Information',$this->Information,true);
		$criteria->compare('InformationDate',$this->InformationDate,true);
		$criteria->compare('InformationRDate',$this->InformationRDate,true);
		$criteria->compare('InformationCRC',$this->InformationCRC);
		$criteria->compare('PluginState',$this->PluginState,true);
		$criteria->compare('PluginStateDate',$this->PluginStateDate,true);
		$criteria->compare('PluginStateRDate',$this->PluginStateRDate,true);
		$criteria->compare('PluginStateCRC',$this->PluginStateCRC);
		$criteria->compare('Features',$this->Features,true);
		$criteria->compare('FeaturesDate',$this->FeaturesDate,true);
		$criteria->compare('FeaturesRDate',$this->FeaturesRDate,true);
		$criteria->compare('FeaturesCRC',$this->FeaturesCRC);
		$criteria->compare('Sysinfo',$this->Sysinfo,true);
		$criteria->compare('SysinfoDate',$this->SysinfoDate,true);
		$criteria->compare('SysinfoRDate',$this->SysinfoRDate,true);
		$criteria->compare('SysinfoCRC',$this->SysinfoCRC);
		$criteria->compare('SysinfoExR',$this->SysinfoExR,true);
		$criteria->compare('SysinfoExD',$this->SysinfoExD,true);
		$criteria->compare('PluginStateShort',$this->PluginStateShort,true);
		$criteria->compare('PluginStateLevel',$this->PluginStateLevel);
		$criteria->compare('WaitingForRestart',$this->WaitingForRestart);
		$criteria->compare('WaitingForRestartDate',$this->WaitingForRestartDate,true);
		$criteria->compare('LastThreatText',$this->LastThreatText,true);
		$criteria->compare('LastFirewallText',$this->LastFirewallText,true);
		$criteria->compare('LastEventText',$this->LastEventText,true);
		$criteria->compare('LastEventTextLevel',$this->LastEventTextLevel,true);
		$criteria->compare('LastScanScanned',$this->LastScanScanned);
		$criteria->compare('LastScanInfected',$this->LastScanInfected);
		$criteria->compare('LastScanCleaned',$this->LastScanCleaned);
		$criteria->compare('LastScanDate',$this->LastScanDate,true);
		$criteria->compare('TaskText',$this->TaskText,true);
		$criteria->compare('MobileUser',$this->MobileUser);
		$criteria->compare('NewClient',$this->NewClient);
		$criteria->compare('LastModifiedUserName',$this->LastModifiedUserName,true);
		$criteria->compare('LastModifiedUserPrimaryServer',$this->LastModifiedUserPrimaryServer,true);
		$criteria->compare('LastModifiedUserDate',$this->LastModifiedUserDate,true);
		$criteria->compare('UserComment',$this->UserComment,true);
		$criteria->compare('IPn',$this->IPn,true);
		$criteria->compare('CustomInfo',$this->CustomInfo,true);
		$criteria->compare('CustomInfo2',$this->CustomInfo2,true);
		$criteria->compare('CustomInfo3',$this->CustomInfo3,true);
		$criteria->compare('CustomInfoDFlag',$this->CustomInfoDFlag);
		$criteria->compare('QuarantineCRC',$this->QuarantineCRC);
		$criteria->compare('FlagQuarFilesReq',$this->FlagQuarFilesReq);
		$criteria->compare('ProductCode',$this->ProductCode,true);
		$criteria->compare('RollbackActive',$this->RollbackActive);
		$criteria->compare('RollbackTime',$this->RollbackTime,true);
		$criteria->compare('IPv6nHigh',$this->IPv6nHigh,true);
		$criteria->compare('IPv6nLow',$this->IPv6nLow,true);
		$criteria->compare('LastThreatDate',$this->LastThreatDate,true);
		$criteria->compare('LastApplyPolicyPrimaryID',$this->LastApplyPolicyPrimaryID);
		$criteria->compare('LastApplyPolicyPrimaryServer',$this->LastApplyPolicyPrimaryServer,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->era;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AntivirusClient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
