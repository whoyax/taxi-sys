<?php

/**
 * This is the model class for table "LinkClientQuarantine".
 *
 * The followings are the available columns in table 'LinkClientQuarantine':
 * @property string $ID
 * @property string $PrimaryID
 * @property string $PrimaryServer
 * @property string $FromServer
 * @property integer $ReplicateUp
 * @property integer $ReplicateDown
 * @property integer $IsForeign
 * @property integer $ClientID
 * @property string $QuarantineID
 * @property integer $Hits
 * @property string $DateReceived
 * @property string $DateOccurredFirst
 * @property string $DateOccurredLast
 * @property string $Reason
 * @property string $Inf
 * @property string $FilePath
 */
class AntivirusClientQuarantine extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'LinkClientQuarantine';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('PrimaryServer, FromServer, ClientID, QuarantineID, FilePath', 'required'),
            array('ReplicateUp, ReplicateDown, IsForeign, ClientID, Hits', 'numerical', 'integerOnly' => true),
            array('ID, PrimaryID, QuarantineID', 'length', 'max' => 20),
            array('PrimaryServer, FromServer', 'length', 'max' => 50),
            array('Reason, Inf, FilePath', 'length', 'max' => 255),
            array('DateReceived, DateOccurredFirst, DateOccurredLast', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, PrimaryID, PrimaryServer, FromServer, ReplicateUp, ReplicateDown, IsForeign, ClientID, QuarantineID, Hits, DateReceived, DateOccurredFirst, DateOccurredLast, Reason, Inf, FilePath', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'client' => array(self::BELONGS_TO, 'AntivirusClient', 'ClientID')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'ID',
            'PrimaryID' => 'Primary',
            'PrimaryServer' => 'Primary Server',
            'FromServer' => 'From Server',
            'ReplicateUp' => 'Replicate Up',
            'ReplicateDown' => 'Replicate Down',
            'IsForeign' => 'Is Foreign',
            'ClientID' => 'Client',
            'QuarantineID' => 'Quarantine',
            'Hits' => 'Hits',
            'DateReceived' => 'Дата получения',
            'DateOccurredFirst' => 'Первое происшествие',
            'DateOccurredLast' => 'Последнее происшествие',
            'Reason' => 'Причина',
            'Inf' => 'Информация',
            'FilePath' => 'Путь',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID, true);
        $criteria->compare('PrimaryID', $this->PrimaryID, true);
        $criteria->compare('PrimaryServer', $this->PrimaryServer, true);
        $criteria->compare('FromServer', $this->FromServer, true);
        $criteria->compare('ReplicateUp', $this->ReplicateUp);
        $criteria->compare('ReplicateDown', $this->ReplicateDown);
        $criteria->compare('IsForeign', $this->IsForeign);
        $criteria->compare('ClientID', $this->ClientID);
        $criteria->compare('QuarantineID', $this->QuarantineID, true);
        $criteria->compare('Hits', $this->Hits);
        $criteria->compare('DateReceived', $this->DateReceived, true);
        $criteria->compare('DateOccurredFirst', $this->DateOccurredFirst, true);
        $criteria->compare('DateOccurredLast', $this->DateOccurredLast, true);
        $criteria->compare('Reason', $this->Reason, true);
        $criteria->compare('Inf', $this->Inf, true);
        $criteria->compare('FilePath', $this->FilePath, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100
            ),
            'sort' => array(
                'defaultOrder' => 'DateReceived DESC'
            )
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->era;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return AntivirusClientQuarantine the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
