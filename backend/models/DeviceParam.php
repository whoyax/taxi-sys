<?php

/**
 * This is the model class for table "device_param".
 *
 * The followings are the available columns in table 'device_param':
 * @property integer $id
 * @property integer $device_id
 * @property string $name
 * @property string $value
 */
class DeviceParam extends EActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'device_param';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('device_id, name', 'required'),
            array('device_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 100),
            array('value', 'length', 'max' => 250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, device_id, name, value', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'device_id' => 'Device',
            'name' => 'Name',
            'value' => 'Value',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('device_id', $this->device_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('value', $this->value, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return DeviceParam the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function updateInfo($hostId, $key, $value)
    {
        if ( !$param = static::model()->findByAttributes(array('device_id' => $hostId, 'name' => $key)) )
        {
            $param = new static();
            $param->device_id = $hostId;
            $param->name = $key;
        }

        if ( $param->value != $value )
        {
            $param->value = $value;

            return $param->save();
        }

        return false;
    }

    public static function updateInfoByModel($device, $key, $value)
    {
        $param = $device->getParam($key);

        if ( $param->value != $value )
        {
            $param->value = $value;
            $result = $param->save();

            if(!$result) {
                var_dump($param->getErrors());
            }

            return $result;
        }

        return false;
    }
}
