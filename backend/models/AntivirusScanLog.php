<?php

/**
 * This is the model class for table "ScanLog".
 *
 * The followings are the available columns in table 'ScanLog':
 * @property string $ID
 * @property string $PrimaryID
 * @property integer $ReplicateUp
 * @property integer $ClientSectionID
 * @property string $DateReceived
 * @property string $DateOccurred
 * @property string $UserName
 * @property integer $FTScannerID
 * @property integer $TaskID
 * @property integer $TaskType
 * @property string $Description
 * @property integer $FTHScanStatusHASH
 * @property integer $Scanned
 * @property integer $Infected
 * @property integer $Cleaned
 * @property string $Details
 * @property integer $ClientID
 */
class AntivirusScanLog extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ScanLog';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ClientSectionID, FTHScanStatusHASH, ClientID', 'required'),
            array('ReplicateUp, ClientSectionID, FTScannerID, TaskID, TaskType, FTHScanStatusHASH, Scanned, Infected, Cleaned, ClientID', 'numerical', 'integerOnly' => true),
            array('ID, PrimaryID', 'length', 'max' => 20),
            array('UserName', 'length', 'max' => 50),
            array('Description', 'length', 'max' => 255),
            array('Details', 'length', 'max' => 1),
            array('DateReceived, DateOccurred', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, PrimaryID, ReplicateUp, ClientSectionID, DateReceived, DateOccurred, UserName, FTScannerID, TaskID, TaskType, Description, FTHScanStatusHASH, Scanned, Infected, Cleaned, Details, ClientID', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'client' => array(self::BELONGS_TO, 'AntivirusClient', 'ClientID')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'ID',
            'PrimaryID' => 'Primary',
            'ReplicateUp' => 'Replicate Up',
            'ClientSectionID' => 'Client Section',
            'DateReceived' => 'Date Received',
            'DateOccurred' => 'Дата',
            'UserName' => 'Пользователь',
            'FTScannerID' => 'Ftscanner',
            'TaskID' => 'Task',
            'TaskType' => 'Task Type',
            'Description' => 'Описание',
            'FTHScanStatusHASH' => 'Fthscan Status Hash',
            'Scanned' => 'Просканировано',
            'Infected' => 'Заражено',
            'Cleaned' => 'Очищено',
            'Details' => 'Details',
            'ClientID' => 'Client',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID, true);
        $criteria->compare('PrimaryID', $this->PrimaryID, true);
        $criteria->compare('ReplicateUp', $this->ReplicateUp);
        $criteria->compare('ClientSectionID', $this->ClientSectionID);
        $criteria->compare('DateReceived', $this->DateReceived, true);
        $criteria->compare('DateOccurred', $this->DateOccurred, true);
        $criteria->compare('UserName', $this->UserName, true);
        $criteria->compare('FTScannerID', $this->FTScannerID);
        $criteria->compare('TaskID', $this->TaskID);
        $criteria->compare('TaskType', $this->TaskType);
        $criteria->compare('Description', $this->Description, true);
        $criteria->compare('FTHScanStatusHASH', $this->FTHScanStatusHASH);
        $criteria->compare('Scanned', $this->Scanned);
        $criteria->compare('Infected', $this->Infected);
        $criteria->compare('Cleaned', $this->Cleaned);
        $criteria->compare('Details', $this->Details, true);
        $criteria->compare('ClientID', $this->ClientID);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100
            ),
            'sort' => array(
                'defaultOrder' => 'DateOccurred DESC'
            )
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->era;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return AntivirusScanLog the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
