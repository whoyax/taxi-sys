<?php

/**
 * This is the model class for table "dns_settings".
 *
 * The followings are the available columns in table 'dns_settings':
 * @property integer $id
 * @property string $service_name
 * @property string $service_url
 * @property integer $queue_id
 * @property integer $auto_dns
 * @property string $domain_name
 * @property string $username
 * @property string $current_ip
 * @property string $set_ip
 * @property string $authcode
 */
class DnsSettings extends EActiveRecord {

    private $_current;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'dns_settings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, service_name, domain_name, username, authcode', 'required'),
            array('id, queue_id, auto_dns', 'numerical', 'integerOnly' => true),
            array('service_name', 'length', 'max' => 100),
            array('set_ip', 'length', 'max' => 16),
            array('service_url, domain_name, username, authcode', 'length', 'max' => 250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, service_name, service_url, queue_id, auto_dns, domain_name, username, authcode', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'queue' => array(self::BELONGS_TO, 'Queue', 'queue_id'),
            //       'current' => array(self::HAS_ONE, 'DnsCurrent', 'id')
        );
    }

    public function getCurrent()
    {
        if ( !$this->_current )
        {
            if ( !$this->_current = DnsCurrent::model()->findByPk($this->id) )
            {
                $this->_current = new DnsCurrent();
                $this->_current->id = $this->id;
            }
        }

        return $this->_current;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'service_name' => 'Service Name',
            'service_url' => 'Service Url',
            'queue_id' => 'Очередь',
            'queue.name' => 'Очередь',
            'auto_dns' => 'Автопереключение Ip по провайдеру в очереди',
            'domain_name' => 'Домен',
            'username' => 'Username',
            'authcode' => 'Authcode',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        // $criteria->compare('id', $this->id);
        $criteria->compare('service_name', $this->service_name, true);
        $criteria->compare('service_url', $this->service_url, true);
        $criteria->compare('queue_id', $this->queue_id);
//        $criteria->compare('auto_dns', $this->auto_dns);
//        $criteria->compare('domain_name', $this->domain_name, true);
//        $criteria->compare('username', $this->username, true);
//        $criteria->compare('authcode', $this->authcode, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return DnsSettings the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
