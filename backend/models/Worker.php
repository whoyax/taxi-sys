<?php

/**
 * This is the model class for table "worker".
 *
 * The followings are the available columns in table 'worker':
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $last_report
 * @property integer $restart
 * @property integer $shutdown
 */
class Worker extends CActiveRecord
{
    const STATUS_UNKNOWN = 0;
    const STATUS_RUNNED = 1;
    const STATUS_RESTART = 2;
    const STATUS_WAIT = 3;
    const STATUS_SHUTDOWN = 4;
    const STATUS_ERROR = 5;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'worker';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, last_report', 'required'),
			array('status, restart, shutdown', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, status, last_report, restart', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'status' => 'Status',
			'last_report' => 'Last Report',
			'restart' => 'Restart',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('last_report',$this->last_report,true);
		$criteria->compare('restart',$this->restart);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Worker the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function setStatus($status) {
        if($this->status != $status) {
            return
                $this->updateByPk($this->id, array(
                    'last_report' => date("Y-m-d H:i:s"),
                    'status' => $status
                 ));
        }

        return false;
    }

    public function markRun() {
        return $this->setStatus(static::STATUS_RUNNED);
    }

    public function markRestart() {
        return
            $this->updateByPk($this->id, array(
            'last_report' => date("Y-m-d H:i:s"),
            'status' => static::STATUS_RESTART,
            'restart' => 0
        ));
    }

    public function markWait() {
        return $this->setStatus(static::STATUS_WAIT);
    }

    public function markShutdown() {
        return $this->setStatus(static::STATUS_SHUTDOWN);
    }

    public function markError() {
        return $this->setStatus(static::STATUS_ERROR);
    }
}
