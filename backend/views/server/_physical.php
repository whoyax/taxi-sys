    <div class="span4">
        <?php
            $this->renderPartial("_hypervisor", array('host' => Device::model()->findByPk(11), 'title' => 'Основной ESXi'));
        ?>
    </div>

    <div class="span4">
        <?php
            $this->renderPartial("_hypervisor", array('host' => Device::model()->findByPk(12), 'title' => 'Резервный ESXi'));
        ?>
    </div>

    <div class="span4">
        <?php
            $this->renderPartial("_host", array('host' => Device::model()->findByPk(52), 'title' => 'Сервер видеонаблюдения'));
        ?>
    </div>
<script>

    $(function () {

        // setup plot
        var options = {
            yaxis: { min: 0, max: 100, reserveSpace: false, color: "#ffffff" },
            xaxis: { min: 0, max: 20, tickLength: 0, show: false },
            colors: ["#ffffff"],
            grid: { hoverable: true, clickable: true, tickColor: "rgba(255,255,255,0.16)" },
            series: {
                lines: {
                    lineWidth: 2,
                    fill: false,
                    fillColor: { colors: [ { opacity: 0.5 }, { opacity: 0.2 } ] },
                    show: true
                },
                points: {
                    show: true,
                    radius: 2.5,
                    fill: true,
                    fillColor: "#55bc75",
                    symbol: "circle",
                    lineWidth: 1.1
                }
            }
        };

        function drawBandwidth(divId, rxStat, txStat) {

            divId.html('');

            var max = Math.max.apply(null, rxStat);
            var maxTx = Math.max.apply(null, txStat);
            if (maxTx > max) max = maxTx;

            var rx = [];
            for (var i = 0; i < rxStat.length; ++i) {
                rx.push([i, parseInt(rxStat[i], 10)]);
            }

            var tx = [];
            for (var i = 0; i < txStat.length; ++i) {
                tx.push([i, parseInt(txStat[i], 10)]);
            }

            $.plot(divId,
                [ rx, tx ], {
                    series: {
                        lines: { show: true },
                        points: { show: false }
                    },
                    grid: { hoverable: false, clickable: false },
                    yaxis: { min: 0, max: max + 100},
                    xaxis: { show: false }
                })
        }

        function convertTicks(data) {
            var steps = [];
            for (var i = 0; i < data.length; ++i) {
                steps.push([i, parseInt(data[i], 10)]);
            }
            return steps;
        }

        function getHostStat(hostId) {
            return $.ajax({
                type: "GET",
                url:  '/device/getHStat/' + hostId,
                async: false,
                data: {}
            }).responseText;
        }

        function initHostStat(hostId) {
            var answer = getHostStat(hostId);
            var data = JSON.parse(answer);
            if(!window.cpuPlots) {
                window.cpuPlots = [];
            }
            window.cpuPlots[hostId] = $.plot($("#h-cpu-" + hostId), [ convertTicks(data.cpu) ], options);

            if(!window.cpuPlotTimeouts) { window.cpuPlotTimeouts = [] };
            if(!window.cpuPlotQuries) { window.cpuPlotQuries = [] };

            window.cpuPlotTimeouts[hostId] = function() {

                if(window.cpuPlotQuries[hostId] && window.cpuPlotQuries[hostId].readyState != 4){
                    window.cpuPlotQuries[hostId].abort();
                }

                window.cpuPlotQuries[hostId] = $.ajax({
                    'url': '/device/getHStat/' + hostId,
                    'type': 'get',
                    'data': {},
                    'cache' : false,
                    'success': function(data){

                        var data = JSON.parse(data);
                        window.cpuPlots[hostId].setData([convertTicks(data.cpu)]);
                        window.cpuPlots[hostId].draw();

                        data.networks.forEach(function(network) {
                            drawBandwidth($(network.id), network.rx, network.tx);
                        });

                    },
                    'error': function(request, status, error){
                        console.log(error);
                        // alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });

                setTimeout(window.cpuPlotTimeouts[hostId], 15000);
            }

            window.cpuPlotTimeouts[hostId]();
        }

        initHostStat(11);
        initHostStat(12);
        initHostStat(52);

    });
</script>



