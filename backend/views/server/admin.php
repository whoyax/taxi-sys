
<h5 class="widget-name"><i class="icon-hdd"></i>Физические серверы</h5>
<div class="row-fluid">
    <?php $this->renderPartial("_physical"); ?>
</div>

<h5 class="widget-name"> Виртуальные машины</h5>
<div class="row-fluid">
    <div class="span12">
        <?php $this->renderPartial("_vms", array()); ?>
    </div>
</div>
