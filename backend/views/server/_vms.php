<?php


$device = new Device('search');
$device->unsetAttributes();
$device->type = DeviceType::VIRTUAL;

$this->widget('SimplePGridView', array(
    'id' => 'virtual-grid',
    'dataProvider' => $device->search(),
    'widgetHeader' => '<i class="icon-tasks"></i>Виртуальные машины',

    'columns' => array(
        // 'id',
        array(
            'name' => 'name',
            'htmlOptions' => array('style' => 'width: 10%')
        ),
        array(
            'name' => 'ip',
            'htmlOptions' => array('style' => 'width: 5%')
        ),
        array(
            'name' => 'info',
            'htmlOptions' => array('style' => 'width: 15%'),
            'type' => 'raw',
            'value' => function($data)
                {
                    $t = "<b>" . $data->getParam("summary.config.guestFullName")->value ."</b>";

                    $t .= "<br/>";
                    $t .= $data->getParam("summary.config.numCpu")->value . ' cpu';

                    $t .= "<br/>";
                    $pwr = $data->getParam("summary.runtime.powerState")->value;

                    if ($pwr=='poweredOn')
                    {
                       $t .= "<span class='label label-success'>Работает</span>";
                    }
                    elseif($pwr=='poweredOff')
                    {
                        $t .= "<span class='label '>Выключен</span>";
                    }
                    else {
                        $t .= "<span class='label label-warning'>{$pwr}</span>";
                    }

                    if($uptime = $data->getParam("summary.quickStats.uptimeSeconds")->value) {
                        $t .= "<br/>";
                        $t .= FormatHelper::secToHumanTranslate($uptime);

                        $t .= "<br/><br/> Запущен ";
                        $t .= date("d.m.y H:i:s", strtotime($data->getParam("summary.runtime.bootTime")->value));
                    }

                    return $t;
                }
        ),
        array(
            'name' => 'cpuStat',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width: 5%'),
            'value' => function($data) {
                    $all = $data->getVmMemoryCapacity();
                    $used = $data->getVmMemoryUse();
                    $free = $all - $used;
                    $percent = DiskInfoHelper::getPercent($all, $free);
                    $class = DiskInfoHelper::getProgressClass($percent);
                    return "<div class='align-center ' style='position: relative; display: block; height:100px; width:200px; ' id='vm_cpu_stat_{$data->id}'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>"
                        . "<br/> <div class=\"progress progress-slim progress-{$class}\"><div class=\"bar\" style=\"width: {$percent}%\"></div>
                </div> <i>{$all} Mb / {$used} Mb active / {$free} Mb </i>";
                },
        ),

        array(
            'name' => 'disk',
            'type' => 'raw',
            'htmlOptions' => array('style' => ' width: 10%'),
            'value' => function ($data) {
                    foreach($data->getDatastoresInfo() as $info) {
                        ?>

                        <strong><?= $info['diskPath'] ?></strong>
                        <?php
                        $percent = DiskInfoHelper::getPercent($info['capacity'], $info['freeSpace'] );
                        $class = DiskInfoHelper::getProgressClass($percent);
                        ?>

                        <div class="progress progress-slim progress-<?= $class ?>">
                            <div class="bar" style="width: <?= $percent ?>%"></div>
                        </div>
                        <i> <?= Yii::app()->format->formatSize($info['freeSpace']) ?> / <?= Yii::app()->format->formatSize($info['capacity']) ?></i>
                        <br/>

                    <?php
                    }
                }
        ),

//            array(
//                'name' => 'status',
//                'type' => 'raw',
//                'value' => function ($data) {
//                        return TbHtml::tag('div', array('id' => 'provider_' . $data->id . '_status'),
//                            NetworkStatus::getStatusLabel($data->status, $data->status_mode));
//                    }
//            ),


//            array(
//                'name' => 'ping',
//                'type' => 'raw',
//                'htmlOptions' => array('style' => ' width: 20%'),
//                'value' => function($data) {
//                        return "<div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='provider_{$data->id}_ping'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>";
//                    },
//            ),
//
//            array(
//                'name' => 'bandwidth',
//                'type' => 'raw',
//                'htmlOptions' => array('style' => ' width: 20%'),
//                'value' => function($data) {
//                        return "<div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='provider_{$data->id}_bandwidth'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>";
//                    },
//            ),
//
//            array(
//                'name' => 'loss',
//                'type' => 'raw',
//                'value' => function ($data) {
//                        return TbHtml::tag('div', array('id' => 'provider_' . $data->id . '_loss'),
//                            $data->loss);
//                    }
//            ),

        array(
            'class' => 'PButtonColumn',
            'updateButtonUrl' => 'Yii::app()->controller->createUrl("device/update",array("id"=>$data->primaryKey))',
            'htmlOptions' => array('style' => 'width: 3%')
        ),
    ),
));
?>
<script>
    function drawVmStatsPlot(divId, rxStat, txStat) {

        divId.html('');

        var max = Math.max.apply(null, rxStat);
        var maxTx = Math.max.apply(null, txStat);
        if (maxTx > max) max = maxTx;

        var rx = [];
        for (var i = 0; i < rxStat.length; ++i) {
            rx.push([i, parseInt(rxStat[i], 10)]);
        }

        var tx = [];
        for (var i = 0; i < txStat.length; ++i) {
            tx.push([i, parseInt(txStat[i], 10)]);
        }

        $.plot(divId,
            [ rx, tx ], {
                series: {
                    lines: { show: true },
                    points: { show: false }
                },
                grid: { hoverable: false, clickable: false },
                yaxis: { min: 0, max: max + 100},
                xaxis: { show: false }
            })
    }

    function drawVmStats(vms) {
        vms.forEach(function(vm) {
            var divId = $("#vm_cpu_stat_" + vm.id);
            drawVmStatsPlot(divId, vm.cpu, vm.mem);
        });
    }

    $(function () {

        var vmStatQuery;

        function updateVmStat() {

            if(vmStatQuery && vmStatQuery.readyState != 4){
                vmStatQuery.abort();
            }

            vmStatQuery = $.ajax({
                'url': '/device/getVmStats',
                'type': 'get',
                'data': {},
                'cache' : false,
                'success': function(data){
                    var vms = JSON.parse(data);
                    drawVmStats(vms);
                },
                'error': function(request, status, error){
                    console.log(error);
                }
            });
           setTimeout(updateVmStat, 15000);
        }

        updateVmStat();
    });


</script>
