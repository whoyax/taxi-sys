
<div class="widget">
    <div class="navbar">
        <div class="navbar-inner">
            <h6><i class="icon-list-alt"></i> <?= $title ?></h6>
            <div class="loading pull-right">
                <span><?= $host->getCpuCapacity() ?> MHz / <?= $host->getMemoryCapacity() ?> Mb</span>
            </div>

        </div>
    </div>
    <div class="section-graph body">
        <div class="widget-chart" id="h-cpu-<?= $host->id ?>">
        </div>
    </div>
    <div class="well">
        <div class="section-info">
            <div class="section-title">
                <a href="#">Свободно RAM</a>
                <div class="graph-info">
                    <span><?php echo $host->getFreeMemoryMb() ?></span>
                    <i>Mb</i>
                </div>
            </div>
            <?php
                if($uptime = $host->getParam("summary.quickStats.uptime")->value) {
                    $up = FormatHelper::secToHumanTranslate($uptime);
                    $start = date("d.m.y H:i:s", strtotime($host->getParam("summary.runtime.bootTime")->value));
            ?>
                <div class="section-title row">
                    <a href="#" class="span3">В работе</a>
                    <div class="graph-info span9" >
                        <span class="pull-right" style="font-size: 11px"><?= $up ?></span>
                        <!-- <i>Mb</i> -->
                    </div>
                </div>
                    <div class="clearfix"></div>
                    <div class="row"> </div>
                    <div class="clearfix"></div>
                    <div class="section-title row">
                    <a href="#" class="span4">Запущен</a>
                    <div class="graph-info span8 pull-right">
                        <span class="pull-right"><?= $start ?></span>
                        <!-- <i>Mb</i> -->
                    </div>
                </div>
            <?php } ?>
            <ul class="info-blocks">
                <?php foreach($host->getNetworks() as $network) { ?>
                    <li>
                        <span><i class="icon icon-bolt"></i><?= $network['name'] ?></span>
                    </li>
                    <li>
                        <div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='h_<?= $host->id . "_" . $network['interface'] ?>_bandwidth'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <?php foreach($host->getDatastoresInfo() as $info) { ?>
            <div class="control-group">
                <strong><?= $info['name'] ?></strong>
                <?php
                    $percent = DiskInfoHelper::getPercent($info['capacity'], $info['freeSpace'] );
                    $class = DiskInfoHelper::getProgressClass($percent);
                ?>
                <div class="progress  progress-<?= $class ?>">
                    <div class="bar" style="width: <?= $percent ?>%"></div>
                </div>
                <i><?= $info['type'] ?>  <?= Yii::app()->format->formatSize($info['freeSpace']) ?> / <?= Yii::app()->format->formatSize($info['capacity']) ?> </i>
            </div>
        <?php } ?>
    </div>
</div>