<div class="widget">
    <div class="navbar">
        <div class="navbar-inner">
            <h6><i class="icon-list-alt"></i> <?= $title ?></h6>

            <div class="loading pull-right">
                <span><?= $host->getCpuCapacity() ?> MHz / <?= $host->getMemoryCapacity() ?> Mb</span>
                <img src="/p/img/elements/loaders/4s.gif" alt="">
            </div>

        </div>
    </div>
    <div class="section-graph body">
        <div class="widget-chart" id="h-cpu-52">

        </div>
    </div>
    <div class="well">
        <div class="section-info">
            <div class="section-title">
                <a href="#">Свободно RAM</a>
                <div class="graph-info">
                    <span>???</span>
                    <i>Mb</i>
                </div>

            </div>

            <ul class="info-blocks">
                <?php foreach($host->getNetworks() as $network) { ?>
                    <li>
                        <span><i class="icon icon-bolt"></i> <?= $network['name'] ?></span>
                    </li>
                    <li>
                        <div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='h_<?= $host->id . "_" . $network['interface'] ?>_bandwidth'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <div class="control-group">
            <strong>System</strong>

            <div class="progress progress-success">
                <div class="bar" style="width: 32%"></div>
            </div>
            <i>NTFS 111Gb / 75Gb</i>
            </div>

            <div class="control-group">
                <strong>Video-1</strong>

                <div class="progress progress-danger">
                    <div class="bar" style="width: 99%"></div>
                </div>
                <i>NTFS 2,72Tb / 3,65Gb</i>
            </div>
            <div class="control-group">
                <strong>Video-2</strong>

            <div class="progress progress-danger">
                <div class="bar" style="width: 99%"></div>
            </div>
            <i>NTFS 2,72Tb / 3,65Gb</i>
        </div>
    </div>
</div>