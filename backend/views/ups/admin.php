<h5 class="widget-name"> Источники бесперебойного питания </h5>
<div class="row-fluid">
    <div class="span12">

        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        $device = new Device('search');
        $device->unsetAttributes();
        $device->type = DeviceType::UPS;

        $this->widget('SimplePGridView', array(
            'id' => 'virtual-grid',
            'dataProvider' => $device->search(),
            'widgetHeader' => '<i class="icon-tasks"></i>UPS',

            'columns' => array(
                // 'id',
                array(
                    'name' => 'name',
                    'htmlOptions' => array('style' => 'width: 10%')
                ),
                array(
                    'name' => 'dependent',
                    'htmlOptions' => array('style' => 'width: 5%'),
                    'type' => 'raw',
                    'header' => 'Зависимые устройства',
                    'value' => function ($data) {
                        $t = '';

                        if ( $data->dependents ) {
                            foreach ( $data->dependents as $dependent ) {
                                if ( $device = $dependent->device ) {
                                    $t .= $device->name;
                                    if ( $status = $device->pingStatus ) {
                                        $class = NetworkStatus::getStatusClass($status->result);
                                        $t .= " <span style='border-radius: 30px; height: 10px;' class='label label-{$class}'>&nbsp;</span>";
                                    }
                                    $t .= '<br/>';
                                }
                            }
                        }

                        return $t;
                    }
                ),
                array(
                    'name' => 'info',
                    'htmlOptions' => array('style' => 'width: 15%'),
                    'type' => 'raw',
                    'value' => function ($data) {
                        $pwr = $data->getParam("ups.status")->value;

                        $t = '';

                        if ( $pwr == 'OL' ) {
                            $t .= "<span class='label label-success'>ONLINE</span>";
                        }
                        elseif ( $pwr == 'OB' ) {
                            $t .= "<span class='label label-warning'>От батареи</span>";
                        }
                        elseif ( $pwr == 'LB' ) {
                            $t .= "<span class='label label-important'>Батарея разряжена</span>";
                        }
                        elseif ( $pwr == 'OL CHRG' ) {
                            $t .= "<span class='label label-warning'>Батарея заряжается</span>";
                        }
                        else {
                            $t .= "<span class='label '>UNKNOWN ({$pwr})</span>";
                        }
                        $t .= "<br/>";
                        $t .= "<br/>";

                        $t .= "<b>" . $data->getParam("device.model")->value . "</b>";
                        $t .= "<br/>";
                        $t .= $data->getParam("device.mfr")->value;

                        $t .= "<br/>";
                        $t .= $data->getParam("device.serial")->value;

                        $t .= "<br/>";


                        $t .= "<br/> U входное: ";
                        $t .= $data->getParam("input.voltage")->value;

                        $t .= "<br/> U выходное: ";
                        $t .= $data->getParam("output.voltage")->value;


                        return $t;
                    }
                ),
                array(
                    'name' => 'Load',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'width: 5%'),
                    'value' => function ($data) {
                        $percent = $data->getParam("ups.load")->value;
                        $class = DiskInfoHelper::getProgressClass($percent);

                        return "<div class='align-center ' style='position: relative; display: block; height:100px; width:200px; ' id='ups_load_stat_{$data->id}'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>"
                        . "<br/> <div class=\"progress progress-slim progress-{$class}\"><div class=\"bar\" style=\"width: {$percent}%\"></div>
                </div> <i>Нагрузка: {$percent}% </i>";
                    },
                ),

                array(
                    'name' => 'Battery',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => ' width: 10%'),
                    'value' => function ($data) {
                        ?>

                        <strong>Заряд батареи</strong>
                        <?php
                        $percent = $data->getParam("battery.charge")->value;
                        $class = DiskInfoHelper::getProgressClass(( 100 - $percent ));
                        ?>

                        <div class="progress progress-slim progress-<?= $class ?>">
                            <div class="bar" style="width: <?= $percent ?>%"></div>
                        </div>
                        <i> <?= $percent ?>% </i>
                        <br/>

                        <?php
                    }
                ),

//            array(
//                'name' => 'status',
//                'type' => 'raw',
//                'value' => function ($data) {
//                        return TbHtml::tag('div', array('id' => 'provider_' . $data->id . '_status'),
//                            NetworkStatus::getStatusLabel($data->status, $data->status_mode));
//                    }
//            ),


//            array(
//                'name' => 'ping',
//                'type' => 'raw',
//                'htmlOptions' => array('style' => ' width: 20%'),
//                'value' => function($data) {
//                        return "<div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='provider_{$data->id}_ping'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>";
//                    },
//            ),
//
//            array(
//                'name' => 'bandwidth',
//                'type' => 'raw',
//                'htmlOptions' => array('style' => ' width: 20%'),
//                'value' => function($data) {
//                        return "<div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='provider_{$data->id}_bandwidth'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>";
//                    },
//            ),
//
//            array(
//                'name' => 'loss',
//                'type' => 'raw',
//                'value' => function ($data) {
//                        return TbHtml::tag('div', array('id' => 'provider_' . $data->id . '_loss'),
//                            $data->loss);
//                    }
//            ),

                array(
                    'class' => 'PButtonColumn',
                    'updateButtonUrl' => 'Yii::app()->controller->createUrl("device/update",array("id"=>$data->primaryKey))',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
            ),
        ));
        ?>
    </div>
</div>

<script>
    function drawVmStatsPlot(divId, rxStat) {

        divId.html('');

        var max = Math.max.apply(null, rxStat);

        var rx = [];
        for (var i = 0; i < rxStat.length; ++i) {
            rx.push([i, parseInt(rxStat[i], 10)]);
        }


        $.plot(divId,
            [rx], {
                series: {
                    lines: {show: true},
                    points: {show: false}
                },
                grid: {hoverable: false, clickable: false},
                yaxis: {min: 0, max: max + 100},
                xaxis: {show: false}
            })
    }

    function drawUpsStats(vms) {
        vms.forEach(function (vm) {
            var divId = $("#ups_load_stat_" + vm.id);
            drawVmStatsPlot(divId, vm.load);
        });
    }

    $(function () {

        var upsStatQuery;

        function updateUpsStat() {

            if (upsStatQuery && upsStatQuery.readyState != 4) {
                upsStatQuery.abort();
            }

            upsStatQuery = $.ajax({
                'url': '/device/getUpsStats',
                'type': 'get',
                'data': {},
                'cache': false,
                'success': function (data) {
                    var vms = JSON.parse(data);
                    drawUpsStats(vms);
                },
                'error': function (request, status, error) {
                    console.log(error);
                }
            });
            setTimeout(updateUpsStat, 15000);
        }

        updateUpsStat();
    });


</script>
