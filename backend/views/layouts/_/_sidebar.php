<div id="sidebar">

    <div class="sidebar-tabs">
        <ul class="tabs-nav two-items">
            <li><a href="#general" title=""><i class="icon-reorder"></i></a></li>
            <li><a href="#stuff" title=""><i class="icon-cogs"></i></a></li>
        </ul>

        <div id="general">
            <?php $this->widget('SideMenu'); ?>
        </div>

        <div id="stuff">

            <?php $this->widget('SideMenu2'); ?>

        </div>

    </div>
</div>
<!-- /sidebar -->