

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <title>Taxiplus</title>
    <link href="/p/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if IE 8]><link href="/p/css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>


    <script type="text/javascript" src="/p/js/plugins/charts/excanvas.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/jquery.flot.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/jquery.sparkline.min.js"></script>

    <script type="text/javascript" src="/p/js/plugins/ui/jquery.easytabs.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/ui/jquery.collapsible.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/ui/prettify.js"></script>
    <script type="text/javascript" src="/p/js/plugins/ui/jquery.fancybox.js"></script>


    <script type="text/javascript" src="/p/js/plugins/forms/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="/p/js/plugins/tables/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="/p/js/files/bootstrap.min.js"></script>


</head>

<body style="background-image: none;">

<!-- Fixed top -->
<div id="top">
    <div class="fixed">
        <a href="/" title="" class="logo"><img src="/img/logo.png" alt="" /></a>

    </div>
</div>
<!-- /fixed top -->


<!-- Content container -->
<div id="container">



    <!-- Content -->
    <div id="content">
        <div class="wrapper">
            <div class="page-header ">
            </div>
            <?= $content ?>
        </div>
    </div>
</div>
<!-- /content container -->


<!-- Footer -->
<div id="footer">
    <div class="copyrights">&copy;  2015 Alexander Oleynikov.</div>
    <ul class="footer-links">
        <li><a href="mailto:leto.vkarmane@gmail.com" title=""><i class="icon-cogs"></i>Contact</a></li>
        <li><a href="mailto:leto.vkarmane@gmail.com" title=""><i class="icon-screenshot"></i>Report bug</a></li>
    </ul>
</div>
<!-- /footer -->

</body>
</html>