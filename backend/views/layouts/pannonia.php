<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/p/js/files/bootstrap.min.js"></script>
    <title>Taxiplus</title>
    <link href="/p/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--[if IE 8]><link href="/p/css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>


    <script type="text/javascript" src="/p/js/plugins/charts/excanvas.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/jquery.flot.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/flot/jquery.flot.time.js"></script>
    <script type="text/javascript" src="/p/js/plugins/charts/jquery.sparkline.min.js"></script>

    <script type="text/javascript" src="/p/js/plugins/ui/jquery.easytabs.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/ui/jquery.collapsible.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/ui/prettify.js"></script>
    <script type="text/javascript" src="/p/js/plugins/ui/jquery.fancybox.js"></script>


    <script type="text/javascript" src="/p/js/plugins/forms/jquery.select2.min.js"></script>
    <script type="text/javascript" src="/p/js/plugins/forms/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="/p/js/plugins/tables/jquery.dataTables.min.js"></script>



    <script type="text/javascript" src="/p/js/functions/index.js"></script>

    <!--
    <script type="text/javascript" src="/p/js/charts/graph.js"></script>
    <script type="text/javascript" src="/p/js/charts/chart1.js"></script>
    <script type="text/javascript" src="/p/js/charts/chart2.js"></script>
    <script type="text/javascript" src="/p/js/charts/chart3.js"></script>
 -->
</head>

<body>

<!-- Fixed top -->
<div id="top">
    <div class="fixed">
        <a href="/" title="" class="logo"><img src="/img/logo.png" alt="" /></a>
        <ul class="top-menu">
            <li><a class="fullview"></a></li>
            <li class="dropdown">
                <a class="user-menu" data-toggle="dropdown"><span>Привет, <?= Yii::app()->user->name ?>! <b class="caret"></b></span></a>
                <ul class="dropdown-menu">
                   <!-- <li><a href="/user/profile" title=""><i class="icon-user"></i>Profile</a></li>
                    <li><a href="/message/index" title=""><i class="icon-inbox"></i>Messages<span class="badge badge-info">9</span></a></li>
                    <li><a href="#" title=""><i class="icon-cog"></i>Settings</a></li> -->
                    <li><a href="/site/logout" title=""><i class="icon-remove"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /fixed top -->


<!-- Content container -->
<div id="container">

<!-- Sidebar -->

    <?php $this->widget('Sidebar'); ?>


<!-- Content -->
<div id="content">

<!-- Content wrapper -->
<div class="wrapper">

<!-- Breadcrumbs line ->
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li class="active"><a href="calendar.html" title="">Main</a></li>
    </ul>
</div>
<!-- /breadcrumbs line -->

<!-- Page header -->
<div class="page-header ">

    <!--
    <div class="page-title" style="padding: 20px 0;">
        <h5>Dashboard</h5>
        <span>Общая информация</span>
    </div>
    -->


</div>
<!-- /page header -->

    <!-- Action tabs -->
    <?php require('_actions.php') ?>
    <!-- /action tabs -->

<?= $content ?>


</div>
<!-- /content wrapper -->

</div>
<!-- /content -->

</div>
<!-- /content container -->


<!-- Footer -->
<div id="footer">
    <div class="copyrights">&copy;  2015 Alexander Oleynikov.</div>
    <ul class="footer-links">
        <li><a href="mailto:leto.vkarmane@gmail.com" title=""><i class="icon-cogs"></i>Contact</a></li>
        <li><a href="mailto:leto.vkarmane@gmail.com" title=""><i class="icon-screenshot"></i>Report bug</a></li>
    </ul>
</div>
<!-- /footer -->

</body>
</html>