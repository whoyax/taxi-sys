<div class="actions-wrapper">
    <div class="actions">

        <div id="user-stats">
            <ul class="round-buttons">
                <li><div class="depth"><a href="/device/create" title="Добавить устройство" class="tip"><i class="icon-plus"></i></a></div></li>
                <li><div class="depth"><a href="/network/admin" title="Сеть" class="tip"><i class="icon-signal"></i></a></div></li>
                <li><div class="depth"><a href="/device/admin" title="Список устройств" class="tip"><i class="icon-reorder"></i></a></div></li>
                <li><div class="depth"><a href="/network/wifi" title="Wi-Fi" class="tip"><i class="icon-rss"></i></a></div></li>
                <li><div class="depth"><a href="/server/admin" title="Серверная статистика" class="tip"><i class="icon-tasks"></i></a></div></li>
                <li><div class="depth"><a href="/queue/admin" title="Настройка" class="tip"><i class="icon-cogs"></i></a></div></li>
            </ul>
        </div>

        <div id="quick-actions">
            <ul class="statistics"><!--
                <li>
                    <div class="top-info">
                        <a href="#" title="" class="red-square"><i class="icon-hand-up"></i></a>
                        <strong>0</strong>
                    </div>
                    <div class="progress progress-micro"><div class="bar" style="width: 20%;"></div></div>
                    <span>Посещений сайта</span>
                </li>
                <li>
                    <div class="top-info">
                        <a href="#" title="" class="blue-square"><i class="icon-plus"></i></a>
                        <strong>0</strong>
                    </div>
                    <div class="progress progress-micro"><div class="bar" style="width: 60%;"></div></div>
                    <span>Посещений за неделю</span>
                </li>
                <li>
                    <div class="top-info">
                        <a href="#" title="" class="purple-square"><i class="icon-shopping-cart"></i></a>
                        <strong>0</strong>
                    </div>
                    <div class="progress progress-micro"><div class="bar" style="width: 90%;"></div></div>
                    <span>Заказы ч/з сайт</span>
                </li>
                <li>
                    <div class="top-info">
                        <a href="#" title="" class="green-square"><i class="icon-ok"></i></a>
                        <strong>0</strong>
                    </div>
                    <div class="progress progress-micro"><div class="bar" style="width: 70%;"></div></div>
                    <span>Подключений</span>
                </li>
                <li>
                    <div class="top-info">
                        <a href="#" title="" class="sea-square"><i class="icon-group"></i></a>
                        <strong>0</strong>
                    </div>
                    <div class="progress progress-micro"><div class="bar" style="width: 50%;"></div></div>
                    <span>Пользователей</span>
                </li>
                <li>
                    <div class="top-info">
                        <a href="#" title="" class="dark-blue-square"><i class="icon-bell"></i></a>
                        <strong>0</strong>
                    </div>
                    <div class="progress progress-micro"><div class="bar" style="width: 93%;"></div></div>
                    <span>Сообщений</span>
                </li>
                -->
            </ul>
        </div>

        <ul class="action-tabs">
            <li><a href="#user-stats" title="">Быстрые действия</a></li>
            <li><a href="#quick-actions" title="">Статистика</a></li>
        </ul>
    </div>
</div>