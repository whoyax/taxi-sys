<h5 class="widget-name"> GSM-Voip шлюзы </h5>
<div class="row-fluid">
    <div class="span12">

        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        if(Yii::app()->request->enableCsrfValidation)
        {
            $csrfTokenName = Yii::app()->request->csrfTokenName;
            $csrfToken = Yii::app()->request->csrfToken;
            $csrf = "\n\t\tdata:{ '$csrfTokenName':'$csrfToken' },";
        }
        else
            $csrf = '';


        $device = new Device('search');
        $device->unsetAttributes();
        $device->type = DeviceType::VOIP;

        $this->widget('SimplePGridView', array(
            'id' => 'voice-grid',
            'dataProvider' => $device->search(),
            'widgetHeader' => '<i class="icon-tasks"></i>Addpack',

            'columns' => array(
                // 'id',
                array(
                    'name' => 'name',
                    'htmlOptions' => array('style' => 'width: 10%')
                ),
                array(
                    'name' => 'ip',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'info',
                    'header' => 'Статус',
                    'htmlOptions' => array('style' => 'width: 15%'),
                    'type' => 'raw',
                    'value' => function($data)
                    {
                        $pwr = $data->getParam("gsm.sim-card")->value;

                        $t = '';

                        if ($pwr)
                        {

                            $t .= "<pre> {$pwr}</pre>"; // "<span class='label '>UNKNOWN ({$pwr})</span>";
                        }
                        $t .= "<br/>";
                        return $t;
                    }
                ),


            array(
                'name' => 'ping',
                'header' => 'Сеть',
                'type' => 'raw',
                'htmlOptions' => array('style' => ' width: 20%'),
                'value' => function($data) {
                        if ( $status = $data->pingStatus ) {
                            $class = NetworkStatus::getStatusClass($status->result);
                            return "<span class='label label-{$class}'>&nbsp;</span>";
                        }

                        return "";
                    },
            ),

                array(
                    'class' => 'PButtonColumn',
                    'updateButtonUrl' => 'Yii::app()->controller->createUrl("device/update",array("id"=>$data->primaryKey))',
                    'htmlOptions' => array('style' => 'width: 3%'),
                    'template' => '<ul class="navbar-icons1 table-controls"> <li>{update}</li><li>{test}</li></ul>',
                    'buttons' => array(
                        'view' => array(
                            'visible' => 'false',
                        ),
                        'delete' => array(
                            'visible' => 'false',
                        ),
                        'test' => array(
                            'label' => 'Перезагрузить',
                            'icon' => 'icon-refresh',
                            'url' => 'Yii::app()->controller->createUrl("restart",array("id"=>$data->primaryKey))',
                            'options' => array('class' => 'btn tip restart'),
                            'click' => <<<EOD
function() {
	if(!confirm("Перезагрузить шлюз?")) return false;

	jQuery.ajax({
		type: 'POST',
		url: jQuery(this).attr('href'), {$csrf}
		success: function(data) {
			jQuery('#voice-grid').yiiGridView('update');

		},
		error: function(XHR) {

		}
	});
	return false;
}
EOD
                        )
                    )
                ),
            ),
        ));
        ?>
    </div>
</div>
<script>
    window.updateVoiceGrid = function() {
        // $("#provider-grid").yiiGridView('update');
        $("#voice-grid").yiiGridView('update');
        setTimeout(updateVoiceGrid, 10000);
    }

   setTimeout(updateVoiceGrid, 10000)
</script>
