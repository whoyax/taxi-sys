<?php
/* @var $this DeviceController */
/* @var $model Device */
/* @var $form TbActiveForm */
?>

<div class="well">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'device-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model, null, null, array('class' => 'margin')); ?>


    <div class="tabbable tabs-right">

        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#mainform" data-toggle="tab">
                    <i class="icon-print"></i>
                    Основное
                </a>
            </li>
            <li class="">
                <a href="#accessform" data-toggle="tab">
                    <i class="icon-unlock"></i>
                    Доступы
                </a>
            </li>
            <li class="">
                <a href="#ipform" data-toggle="tab">
                    <i class="icon-globe"></i>
                    Адреса
                </a>
            </li>
            <li class="">
                <a href="#paramform" data-toggle="tab">
                    <i class="icon-cogs"></i>
                    Параметры
                </a>
            </li>
            <li class="">
                <a href="#monitoringform" data-toggle="tab">
                    <i class="icon-signal"></i>
                    Мониторинг
                </a>
            </li>
            <li class="">
                <a href="#dependentform" data-toggle="tab">
                    <i class="icon-random"></i>
                    Зависимости
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="mainform">
                <?php echo $form->dropDownListControlGroup($model,'group_id', TbHtml::listData(Group::model()->findAll(), 'id', 'name'), array('span'=>5)); ?>
                <?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>100)); ?>
                <?php echo $form->textFieldControlGroup($model,'ip',array('span'=>5,'maxlength'=>50)); ?>

                <div class="control-group">
                    <label class="control-label" for="">Очередь</label>
                    <div class="controls">
                        <?php
                        echo PnHtml::buttonGroupRadioField($model, 'queue_id', Queue::getOptionList());
                        ?>
                    </div>
                </div>

                <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6,'span'=>8)); ?>
            </div>
            <div class="tab-pane" id="accessform">

                <?php echo $form->textFieldControlGroup($model,'login',array('span'=>5,'maxlength'=>50)); ?>
                <?php echo $form->passwordFieldControlGroup($model,'pass',array('span'=>5,'maxlength'=>50)); ?>

                <div class="control-group">
                    <label class="control-label" for="">Доступы</label>
                    <div class="controls">
                        <?php

                        $provider = new DeviceAccess('search');
                        $provider->unsetAttributes();
                        $provider->device_id = ($model->getIsNewRecord()) ? -1 : $model->id;


                        $this->widget('SimplePGridView',array(
                            'id'=>'access-grid',
                            'dataProvider'=>$provider->search(),
                            'htmlOptions' => array('class' => 'margin span10'),
                            'addButton' => array(
                                'url' => $this->createUrl('addAccess', array('id' => $model->id)),
                                'icon' => 'plus',
                                'htmlOptions' => array(
                                    'class' => 'btn pull-right btn-success ',
                                    'id' => 'add-access',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#addAccessDialog'
                                )
                            ),

                            'widgetHeader'=>'Доступы',
                            'columns'=>array(
                                // 'id',
                                array(
                                    'name' => 'name',
                                    'htmlOptions' => array('style' => 'width: 10%')
                                ),

                                array(
                                    'name' => 'login',
                                    'htmlOptions' => array('style' => 'width: 20%')
                                ),
                                array(
                                    'name' => 'pass',
                                    'htmlOptions' => array('style' => 'width: 20%')
                                ),
                                array(
                                    'name' => 'port',
                                    'htmlOptions' => array('style' => 'width: 10%')
                                ),
                                array(
                                    'name' => 'link',
                                    'type' => 'url',
                                    'htmlOptions' => array('style' => 'width: 40%', 'class' => 'align-left')
                                ),
                                array(
                                    'class'=>'PButtonColumn',
                                    'deleteButtonUrl'=>'Yii::app()->controller->createUrl("deleteAccess",array("id"=>$data->primaryKey))',
                                    'buttons' => array(
                                        'view' => array(
                                            'visible' => 'false',
                                        ),
                                        'update' => array(
                                            'visible' => 'false',
                                        ),
                                        'delete' => array(
                                            'visible' => 'true',
                                        )

                                    )
                                ),
                            ),
                        )); ?>
                    </div>
                </div>



            </div>


            <div class="tab-pane" id="ipform">

                <div class="control-group">
                    <label class="control-label" for="">Адреса</label>
                    <div class="controls">
                        <?php

                        $ipProvider = new DeviceIp('search');
                        $ipProvider->unsetAttributes();
                        $ipProvider->device_id = ($model->getIsNewRecord()) ? -1 : $model->id;


                        $this->widget('SimplePGridView',array(
                            'id'=>'ip-grid',
                            'dataProvider'=>$ipProvider->search(),
                            'htmlOptions' => array('class' => 'margin span10'),
                            'addButton' => array(
                                'url' => $this->createUrl('addIp', array('id' => $model->id)),
                                'icon' => 'plus',
                                'htmlOptions' => array(
                                    'class' => 'btn pull-right btn-success ',
                                    'id' => 'add-ip',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#addIpDialog'
                                )
                            ),

                            'widgetHeader'=>'Адреса',
                            'columns'=>array(
                                // 'id',
                                array(
                                    'name' => 'name',
                                    'htmlOptions' => array('style' => 'width: 20%')
                                ),

                                array(
                                    'name' => 'ip',
                                    'htmlOptions' => array('style' => 'width: 30%')
                                ),
                                array(
                                    'name' => 'description',
                                    'htmlOptions' => array('style' => 'width: 40%', 'class' => 'align-left')
                                ),
                                array(
                                    'class'=>'PButtonColumn',
                                    'deleteButtonUrl'=>'Yii::app()->controller->createUrl("deleteIp",array("id"=>$data->primaryKey))',
                                    'buttons' => array(
                                        'view' => array(
                                            'visible' => 'false',
                                        ),
                                        'update' => array(
                                            'visible' => 'false',
                                        ),
                                        'delete' => array(
                                            'visible' => 'true',
                                        )

                                    )
                                ),
                            ),
                        )); ?>
                    </div>
                </div>
            </div>


            <div class="tab-pane" id="paramform">

                <div class="control-group">
                    <label class="control-label" for="">Параметры</label>
                    <div class="controls">
                        <?php

                        $paramProvider = new DeviceParam('search');
                        $paramProvider->unsetAttributes();
                        $paramProvider->device_id = ($model->getIsNewRecord()) ? -1 : $model->id;


                        $this->widget('SimplePGridView',array(
                            'id'=>'param-grid',
                            'dataProvider'=>$paramProvider->search(),
                            'htmlOptions' => array('class' => 'margin span10'),
                            'addButton' => array(
                                'url' => $this->createUrl('addParam', array('id' => $model->id)),
                                'icon' => 'plus',
                                'htmlOptions' => array(
                                    'class' => 'btn pull-right btn-success ',
                                    'id' => 'add-param',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#addParamDialog'
                                )
                            ),

                            'widgetHeader'=>'Параметры',
                            'columns'=>array(
                                // 'id',
                                array(
                                    'name' => 'name',
                                    'htmlOptions' => array('style' => 'width: 20%')
                                ),

                                array(
                                    'name' => 'value',
                                    'htmlOptions' => array('style' => 'width: 80%', 'class' => 'align-left')
                                ),

                                array(
                                    'class'=>'PButtonColumn',
                                    'deleteButtonUrl'=>'Yii::app()->controller->createUrl("deleteParam",array("id"=>$data->primaryKey))',
                                    'buttons' => array(
                                        'view' => array(
                                            'visible' => 'false',
                                        ),
                                        'update' => array(
                                            'visible' => 'false',
                                        ),
                                        'delete' => array(
                                            'visible' => 'true',
                                        )

                                    )
                                ),
                            ),
                        )); ?>
                    </div>
                </div>
            </div>


            <div class="tab-pane" id="dependentform">

                <div class="control-group">
                    <label class="control-label" for="">Зависимости</label>
                    <div class="controls">
                        <?php

                        $dependentProvider = new DeviceDependent('search');
                        $dependentProvider ->unsetAttributes();
                        $dependentProvider ->device_id = ($model->getIsNewRecord()) ? -1 : $model->id;

                        $this->widget('SimplePGridView',array(
                            'id'=>'dependent-grid',
                            'dataProvider'=>$dependentProvider->search(),
                            'htmlOptions' => array('class' => 'margin span10'),
                            'addButton' => array(
                                'url' => $this->createUrl('addDependent', array('id' => $model->id)),
                                'icon' => 'plus',
                                'htmlOptions' => array(
                                    'class' => 'btn pull-right btn-success ',
                                    'id' => 'add-param',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#addDependentDialog'
                                )
                            ),

                            'widgetHeader'=>'Зависимые устройства',
                            'columns'=>array(
                                // 'id',
                                array(
                                    'name' => 'device.name',
                                    'htmlOptions' => array('style' => 'width: 20%')
                                ),

                                array(
                                    'name' => 'device.ip',
                                    'htmlOptions' => array('style' => 'width: 80%', 'class' => 'align-left')
                                ),

                                array(
                                    'class'=>'PButtonColumn',
                                    'deleteButtonUrl'=>'Yii::app()->controller->createUrl("deleteDependent",array("id"=>$data->primaryKey))',
                                    'buttons' => array(
                                        'view' => array(
                                            'visible' => 'false',
                                        ),
                                        'update' => array(
                                            'visible' => 'false',
                                        ),
                                        'delete' => array(
                                            'visible' => 'true',
                                        )
                                    )
                                ),
                            ),
                        )); ?>
                    </div>
                </div>
            </div>


            <div class="tab-pane " id="monitoringform">
                <?php echo $form->dropDownListControlGroup($model,'type', DeviceType::getOptions(), array('span'=>5)); ?>
                <?php echo $form->textFieldControlGroup($model,'alias',array('span'=>5,'maxlength'=>100)); ?>
                <?php echo $form->textFieldControlGroup($model,'interface',array('span'=>5,'maxlength'=>100)); ?>
                <?php echo $form->textFieldControlGroup($model,'port',array('span'=>5,'maxlength'=>5)); ?>
                <?php echo $form->textFieldControlGroup($model,'server_api',array('span'=>5,'maxlength'=>50)); ?>

                <div class="control-group">
                    <label class="control-label" for="">Родительское устройство</label>
                    <div class="controls">
                        <?php
                        $this->widget('yiiwheels.widgets.select2.WhSelect2', array(
                          //  'asDropDownList' => false,
                            'name' => 'Device[parent_id]',
                            'data' => array('' => 'Нет') + CHtml::listData(Device::model()->findAll(), 'id', 'name'),
                            'value' => $model->parent_id,
                            'htmlOptions' => array(
                                'class' => 'span5 select2-offscreen'
                            ),
//                            'pluginOptions' => array(
//                                'tags' => array('2amigos','consulting', 'group', 'rocks'),
//                                'placeholder' => 'type 2amigos',
//                                'width' => '40%',
//                                'tokenSeparators' => array(',', ' ')
//                            )
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="form-actions">
    <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array(
        'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
        'size'=>TbHtml::BUTTON_SIZE_LARGE,
        // 'class' => 'pull-right'
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->


<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addAccessDialog',
    'header' => 'Добавить доступ',
    'content' => $this->renderPartial('_access_form', array('model' => $provider), true),
    'onShow' => 'js: function() { $("#DeviceAccess_name, #DeviceAccess_login, #DeviceAccess_pass, #DeviceAccess_link").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveAccessTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addIpDialog',
    'header' => 'Добавить Ip-адрес',
    'content' => $this->renderPartial('_ip_form', array('model' => $ipProvider), true),
    'onShow' => 'js: function() { $("#DeviceIp_name, #DeviceIp_ip, #DeviceAccess_pass, #DeviceIp_description").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveIpTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>


<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addParamDialog',
    'header' => 'Добавить параметр',
    'content' => $this->renderPartial('_param_form', array('model' => $paramProvider), true),
    'onShow' => 'js: function() { $("#DeviceParam_name, #DeviceParam_value").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveParamTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addDependentDialog',
    'header' => 'Добавить устройство',
    'content' => $this->renderPartial('_dependent_form', array('model' => $dependentProvider), true),
    'onShow' => 'js: function() { $("#DeviceDependent_dependent_ip, #DeviceDependent_description").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveDependentTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<script>

    function saveTrigger(formId, callback) {

        var form = $(formId);
        var settings = form.data('settings') ;
        settings.submitting = true ;
        $.fn.yiiactiveform.validate(form, function(messages) {
            $.each(settings.attributes, function () {
                $.fn.yiiactiveform.updateInput(this, messages, form);
            });

            if($.isEmptyObject(messages)) { // If there are no error messages all data are valid
                callback();
            } else {
                settings.submitting = false ;
            }
        });
    }

    function saveAccessTrigger() {
        return saveTrigger("#deviceaccess-form", function(){
            $.post(
                '/device/addAccess/<?= $model->id ?>',
                {
                    "DeviceAccess" : {
                        "name" : $("#DeviceAccess_name").val(),
                        "login" : $("#DeviceAccess_login").val(),
                        "pass" : $("#DeviceAccess_pass").val(),
                        "port" : $("#DeviceAccess_port").val(),
                        "link" : $("#DeviceAccess_link").val(),
                        "description" : $("#DeviceAccess_description").val()
                    }
                },
                function(){
                    $("#access-grid").yiiGridView("update");
                }
            );
            $("#addAccessDialog").modal("hide");
        });
    }

    function saveIpTrigger() {
        return saveTrigger("#deviceip-form", function(){
            $.post(
                '/device/addIp/<?= $model->id ?>',
                {
                    "DeviceIp" : {
                        "name" : $("#DeviceIp_name").val(),
                        "ip" : $("#DeviceIp_ip").val(),
                        "description" : $("#DeviceIp_description").val(),
                    }
                },
                function(){
                    $("#ip-grid").yiiGridView("update");
                }
            );
            $("#addIpDialog").modal("hide");
        });
    }

    function saveParamTrigger() {
        return saveTrigger("#deviceparam-form", function(){
            $.post(
                '/device/addParam/<?= $model->id ?>',
                {
                    "DeviceParam" : {
                        "name" : $("#DeviceParam_name").val(),
                        "value" : $("#DeviceParam_value").val(),
                    }
                },
                function(){
                    $("#param-grid").yiiGridView("update");
                }
            );
            $("#addParamDialog").modal("hide");
        });
    }

    function saveDependentTrigger() {
        return saveTrigger("#devicedependent-form", function(){
            $.post(
                '/device/addDependent/<?= $model->id ?>',
                {
                    "DeviceDependent" : {
                        "dependent_id" : $("#DeviceDependent_dependent_id").val(),
                        "description" : $("#DeviceDependent_description").val(),
                    }
                },
                function(){
                    $("#dependent-grid").yiiGridView("update");
                }
            );
            $("#addDependentDialog").modal("hide");
        });
    }

</script>