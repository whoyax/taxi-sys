<?php
/* @var $this DeviceController */
/* @var $data Device */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
	<?php echo CHtml::encode($data->group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ip')); ?>:</b>
	<?php echo CHtml::encode($data->ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('internet')); ?>:</b>
	<?php echo CHtml::encode($data->internet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login')); ?>:</b>
	<?php echo CHtml::encode($data->login); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pass')); ?>:</b>
	<?php echo CHtml::encode($data->pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tm')); ?>:</b>
	<?php echo CHtml::encode($data->tm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tm_pass')); ?>:</b>
	<?php echo CHtml::encode($data->tm_pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ammy')); ?>:</b>
	<?php echo CHtml::encode($data->ammy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ammy_pass')); ?>:</b>
	<?php echo CHtml::encode($data->ammy_pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vnc')); ?>:</b>
	<?php echo CHtml::encode($data->vnc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vnc_pass')); ?>:</b>
	<?php echo CHtml::encode($data->vnc_pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('server_api')); ?>:</b>
	<?php echo CHtml::encode($data->server_api); ?>
	<br />

	*/ ?>

</div>