<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
/* @var $form TbActiveForm */
?>

<div class="1">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'devicedependent-form',
        'action' => $this->createUrl('addDependent', array('id' => $model->device_id)),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <label class="control-label" for="">Зависимое устройство</label>
        <div class="controls">
            <?php
            $this->widget('yiiwheels.widgets.select2.WhSelect2', array(
                    //  'asDropDownList' => false,
                    'name' => 'DeviceDependent[dependent_id]',
                    'data' => CHtml::listData(Device::model()->findAll(), 'id', 'name'),
                    'value' => $model->dependent_id,
                    'htmlOptions' => array(
                        'class' => 'span5 select2-offscreen'
                    ),
//                            'pluginOptions' => array(
//                                'tags' => array('2amigos','consulting', 'group', 'rocks'),
//                                'placeholder' => 'type 2amigos',
//                                'width' => '40%',
//                                'tokenSeparators' => array(',', ' ')
//                            )
                )
            );
            ?>
        </div>
    </div>
            <?php echo $form->textAreaControlGroup($model,'description',array('span'=>10)); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->