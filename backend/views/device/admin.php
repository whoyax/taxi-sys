<?php
/* @var $this DeviceController */
/* @var $model Device */


$this->breadcrumbs=array(
	'Devices'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Device', 'url'=>array('index')),
	array('label'=>'Create Device', 'url'=>array('create')),
);


?>

<h1>Устройства</h1>


<?php $this->widget('PGridView',array(
	'id'=>'device-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'widgetHeader'=>'',
	'columns'=>array(
		// 'id',
		'group_id' => array(
            'name' => 'group_id',
            'filter' => TbHtml::listData(Group::model()->findAll(), 'id', 'name'),
            'value' => '$data->group->name',
            'header' => 'Группа'
        ),
		'name',
		'ip',
		'queue' => array(
            'name' => 'queue_id',
            'filter' => TbHtml::listData(Queue::model()->findAll(), 'id', 'name'),
            'value' => '($data->queue) ? $data->queue->name : ""',
            'header' => 'Очередь'
        ),
		'description',
		/*
		'login',
		'pass',
		'tm',
		'tm_pass',
		'ammy',
		'ammy_pass',
		'vnc',
		'vnc_pass',
		'server_api',
		*/
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>


