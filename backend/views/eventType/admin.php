<?php
/* @var $this UserController */
/* @var $model User */


$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
);

?>

<h1>Типы событий</h1>

<?php $this->widget('PGridView', array(
    'id' => 'eventtype-grid',
    'widgetHeader' => 'Типы',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'title' => array(
            'name' => 'title',
            'htmlOptions' => array('style' => 'width: 80%')
        ),
        array(
            'class' => 'PButtonColumn',
        ),
    ),
)); ?>
