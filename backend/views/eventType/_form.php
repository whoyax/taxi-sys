<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form TbActiveForm */
?>



    <div class="well">

        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'eventtype-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
    )); ?>

                <?php echo $form->errorSummary($model, null, null, array('class'=>'margin')); ?>

                <?php echo $form->textFieldControlGroup($model,'title',array('span'=>5,'maxlength'=>250)); ?>

            <div class="form-actions">
                <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array(
                    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
                    'size'=>TbHtml::BUTTON_SIZE_LARGE,
                )); ?>
            </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->