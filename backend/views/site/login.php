<?php
$this->pageTitle=Yii::app()->name . ' - Вход';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="row-fluid " style="margin-top: 10%;">
<div class="span4 offset4" >
    <div class="widget form-inline">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Вход</h6>
            </div>
        </div>
        <div class="well ">


            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'login-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'enableAjaxValidation'=>true,
            )); ?>

            <?php echo $form->errorSummary($model, null, null, array('class' => 'margin')); ?>

            <div class="control-group">
            <?php echo $form->textField($model,'username',array('span'=>5, 'placeholder' => 'Логин')); ?>

            <?php echo $form->passwordField($model,'password',array('span'=>5, 'placeholder' => 'Пароль')); ?>
            <?php echo TbHtml::submitButton('Войти!', array(
                'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
                'size'=>TbHtml::BUTTON_SIZE_LARGE,
            )); ?>
            </div>


            <?php $this->endWidget(); ?>
        </div>
    </div>



</div>
</div>