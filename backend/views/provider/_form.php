<?php
/* @var $this ProviderController */
/* @var $model Provider */
/* @var $form TbActiveForm */
?>

<div class="well">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'provider-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php // echo $form->textFieldControlGroup($model,'id',array('span'=>5)); ?>

    <?php echo $form->textFieldControlGroup($model, 'name', array('span' => 5, 'maxlength' => 50)); ?>

    <?php echo $form->textFieldControlGroup($model, 'alias', array('span' => 5, 'maxlength' => 100)); ?>
    <?php echo $form->textFieldControlGroup($model, 'interface', array('span' => 5, 'maxlength' => 50)); ?>

    <?php echo $form->textAreaControlGroup($model, 'description', array('rows' => 6, 'span' => 8)); ?>

    <?php echo $form->textFieldControlGroup($model, 'gate', array('span' => 5, 'maxlength' => 50)); ?>

    <?php echo $form->textFieldControlGroup($model, 'ping_host', array('span' => 5, 'maxlength' => 50)); ?>

    <?php echo $form->textFieldControlGroup($model, 'ext_ip', array('span' => 5, 'maxlength' => 50)); ?>

    <?php echo $form->textFieldControlGroup($model, 'ping_timeout', array('span' => 5)); ?>

    <?php echo $form->textFieldControlGroup($model, 'ping_num', array('span' => 5)); ?>

    <?php // echo $form->dropDownListControlGroup($model, 'status_mode', NetworkStatus::getStatusModeOptions(), array('span' => 5)); ?>

    <div class="control-group">
        <label class="control-label" for="">Статус</label>

        <div class="controls">
            <?php
                echo PnHtml::buttonGroupRadioField($model, 'status', array(-1 => 'Проверять постоянно') + NetworkStatus::getOptionList());
            ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => TbHtml::BUTTON_COLOR_PRIMARY,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->