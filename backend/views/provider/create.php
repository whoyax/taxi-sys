<?php
/* @var $this ProviderController */
/* @var $model Provider */
?>

<?php
$this->breadcrumbs=array(
	'Providers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Provider', 'url'=>array('index')),
	array('label'=>'Manage Provider', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Добавление</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
