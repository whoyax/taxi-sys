<?php
/* @var $this ProviderController */
/* @var $model Provider */


$this->breadcrumbs=array(
	'Providers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Provider', 'url'=>array('index')),
	array('label'=>'Create Provider', 'url'=>array('create')),
);


?>

<h1>Провайдеры</h1>


<?php $this->widget('PGridView',array(
	'id'=>'provider-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'widgetHeader'=>'',
	'columns'=>array(
		// 'id',
		'name',
		'description',
//		'gate',
//		'route',
		'ext_ip',
		/*
		'ping_timeout',
		'ping_num',
		'alias', */
		array(
            'name' => 'status',
            'type' => 'raw',
            'value' => function($data) {
                    return NetworkStatus::getStatusLabel($data->getStatus(), $data->status_mode);                }
        ),

		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>