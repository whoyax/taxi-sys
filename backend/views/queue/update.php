<?php
/* @var $this QueueController */
/* @var $model Queue */
?>

<?php
$this->breadcrumbs=array(
	'Queues'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Queue', 'url'=>array('index')),
	array('label'=>'Create Queue', 'url'=>array('create')),
	array('label'=>'View Queue', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Queue', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Редактирование</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>


