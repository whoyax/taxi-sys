<?php
/* @var $this QueueController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Queues',
);

$this->menu=array(
	array('label'=>'Create Queue','url'=>array('create')),
	array('label'=>'Manage Queue','url'=>array('admin')),
);
?>

<h1>Queues</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>