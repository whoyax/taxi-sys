<?php
/* @var $this QueueController */
/* @var $model Queue */
/* @var $form TbActiveForm */
?>

<div class="well">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'queue-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model, 'name', array('span' => 5, 'maxlength' => 50)); ?>

    <?php echo $form->textAreaControlGroup($model, 'description', array('rows' => 6, 'span' => 8)); ?>

    <?php // echo $form->textFieldControlGroup($model,'active',array('span'=>5)); ?>

    <div class="control-group">
        <label class="control-label" for="">Провайдеры</label>
        <div class="controls">
            <?php

            $provider = new QueueProvider('search');
            $provider->unsetAttributes();
            $provider->queue_id = ($model->getIsNewRecord()) ? -1 : $model->id;


            $this->widget('SimplePGridView',array(
                'id'=>'queue-provider-grid',
                'dataProvider'=>$provider->search(),
                'htmlOptions' => array('class' => 'margin span8'),
                'afterAjaxUpdate' => 'js: function(){ installSortable(); }',
                'rowCssClassExpression'=>'"items[]_{$data->id}"',
                'hideHeader' => true,
                'addButton' => array(
                    'url' => $this->createUrl('addProvider', array('id' => $model->id)),
                    'icon' => 'plus',
                    'htmlOptions' => array(
                        'class' => 'btn pull-right btn-success ',
                        'id' => 'add-provider',
                        'data-toggle' => 'modal',
                        'data-target' => '#addProviderDialog'
                    )
                ),

                'widgetHeader'=>'Провайдеры',
                'columns'=>array(
                    // 'id',
                    array(
                        'name' => 'provider.name',
                        'htmlOptions' => array('style' => 'width: 50%')

                    ),
                    'position',
//                    array(
//                        'class'=>'PButtonColumn',
//                    ),
                ),
            )); ?>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => TbHtml::BUTTON_COLOR_PRIMARY,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addProviderDialog',
    'header' => 'Добавить провайдера',
    'content' => TbHtml::dropDownList('provider_id', null, array()),
    'onShow' => 'js: function() { $("#provider_id").load("/queue/getList/' . $model->id . '") }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveProvider()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<?php
$str_js = "
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };

        function installSortable(){

            $('#queue-provider-grid table.items tbody').sortable({
                forcePlaceholderSize: true,
                forceHelperSize: true,
                items: 'tr',
                update : function () {
                    serial = $('#queue-provider-grid table.items tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                    $.ajax({
                        'url': '" . $this->createUrl('/queue/sort') . "',
                        'type': 'post',
                        'data': serial,
                        'success': function(data){
                            $('#queue-provider-grid').yiiGridView('update');
                        },
                        'error': function(request, status, error){
                           console.log(error);
                        }
                    });
                },
                helper: fixHelper
            }).disableSelection();
        }

        installSortable();
    ";

Yii::app()->clientScript->registerScript('sortable-provider', $str_js);
?>


<script>
    function saveProvider(){

        $.post(
            '/queue/addProvider/<?= $model->id ?>',
            {
                "provider" : $("#provider_id").val()
            },
            function(){
                $("#queue-provider-grid").yiiGridView("update");
            }
        );
        console.log("ok");
        $("#addProviderDialog").modal("hide");
    }
</script>