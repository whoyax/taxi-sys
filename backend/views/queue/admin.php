<?php
/* @var $this QueueController */
/* @var $model Queue */


$this->breadcrumbs=array(
	'Queues'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Queue', 'url'=>array('index')),
	array('label'=>'Create Queue', 'url'=>array('create')),
);


?>

<h1>Очереди</h1>


<?php $this->widget('PGridView',array(
	'id'=>'queue-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'widgetHeader'=>'',
	'columns'=>array(
		// 'id',
		'name',
		'description',
		'activeProvider.provider.name',
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>