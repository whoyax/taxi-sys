<?php
/* @var $this QueueController */
/* @var $model Queue */
?>

<?php
$this->breadcrumbs=array(
	'Queues'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Queue', 'url'=>array('index')),
	array('label'=>'Manage Queue', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Добавление</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
