<h5 class="widget-name"> Видеонаблюдение </h5>

<div class="widget">
    <div class="navbar">
        <div class="navbar-inner">
            <h6> Коммутатор </h6>
        </div>
    </div>
    <div class="table-overflow" id="poe-info">
        <table class="table table-borderd table-stripped">
            <thead>
            <tr>
                <th style="width: 20%">Параметр</th>
                <th>Значение</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Производитель</td>
                <td>D-Link</td>
            </tr>

            <tr>
                <td>Модель</td>
                <td><?= $params['model']->value ?></td>
            </tr>

            <tr>
                <td>Прошивка</td>
                <td><?= $params['firmware']->value ?></td>
            </tr>

            <tr>
                <td>MAC адрес</td>
                <td><?= $params['mac']->value ?></td>
            </tr>

            <tr>
                <td>IP адрес</td>
                <td><a target="_blank" href="http://<?= $dlink->ip ?>"><?= $dlink->ip ?></a></td>
            </tr>

            <tr>
                <td>Мощность POE, Вт</td>
                <td><?= $params['poe.total']->value ?></td>
            </tr>

            <tr>
                <td>Текущее потребление камер </td>
                <td>
                    <b><?= $params['poe.used']->value ?> Вт </b>
                    <br />
                    <div class="progress progress- progress-<?= DiskInfoHelper::getProgressClass($params['poe.percent']->value) ?>">
                        <div class="bar" style="width: <?= $params['poe.percent']->value ?>%"></div>
                    </div>
                    <i> Нагрузка: <?= $params['poe.percent']->value ?> % </i>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">



        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        if(Yii::app()->request->enableCsrfValidation)
        {
            $csrfTokenName = Yii::app()->request->csrfTokenName;
            $csrfToken = Yii::app()->request->csrfToken;
            $csrf = "\n\t\tdata:{ '$csrfTokenName':'$csrfToken' },";
        }
        else
            $csrf = '';




        $this->widget('SimplePGridView', array(
            'id' => 'video-grid',
            'ajaxUpdate'=>'poe-info',
            'dataProvider' => new CArrayDataProvider($ports, array('pagination' => array('pageSize' => 30))),
            'widgetHeader' => '<i class="icon-tasks"></i>Порты',

            'columns' => array(
                // 'id',
                array(
                    'name' => 'n',
                    'header' => '#',
                    'htmlOptions' => array('style' => 'width: 1%')
                ),
                array(
                    'name' => 'ping',
                    'header' => 'Устройство',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => ' width: 20%'),
                    'value' => function($data) {
                        if ( $id = (int)$data['device'] ) {
                            if( $device = Device::model()->findByPk($id) ) {
                                $class = NetworkStatus::getStatusClass($device->pingStatus->result);
                                return "<b>{$device->name}</b> <br /> <span class='label label-{$class}'>&nbsp;</span> <a target='_blank' href='http://{$device->ip}'>{$device->ip}</a>";
                            }
                        }

                        return "";
                    },
                ),

                array(
                    'name' => 'power',
                    'header' => 'Мощность',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'voltage',
                    'header' => 'Напряжение',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'current',
                    'header' => 'Ток',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'status',
                    'header' => 'Статус PoE',
                    'htmlOptions' => array('style' => 'width: 5%'),
                    'type' => 'raw',
                    'value' => function($data)
                    {

                        $t = '';


                        if(!empty($data['status']))
                        {
                            if ( $data['status'] == "POWER ON" )
                            {

                                $t .= "<span class='label label-success '>{$data['status']}</span>";
                            }
                            elseif ( $data['status'] == "POWER OFF" )
                            {
                                $t .= "<span class='label '>{$data['status']}</span>";
                            }
                            else
                            {
                                $t .= "<span class='label label-warning '>{$data['status']}</span>";
                            }
                            $t .= "<br/>";

                        }
                        return $t;
                    }
                ),




                array(
                    'class' => 'PButtonColumn',
                    'htmlOptions' => array('style' => 'width: 6%'),
                    'template' => '<ul class="navbar-icons1 table-controls"><li>{on}</li><li>{off}</li></ul>',
                    'buttons' => array(
                        'view' => array(
                            'visible' => 'false',
                        ),
                        'delete' => array(
                            'visible' => 'false',
                        ),
                        'on' => array(
                            'label' => 'Включить',
                            'icon' => 'icon-bolt',
                            'visible' => ' $data["status"]!="POWER ON"',
                            'url' => 'Yii::app()->controller->createUrl("toggleOn",array("port"=>$data["n"]))',
                            'options' => array('class' => 'btn tip toggleOn'),
                            'click' => <<<EOD
function() {
	if(!confirm("Включить порт??")) return false;

	jQuery.ajax({
		type: 'POST',
		url: jQuery(this).attr('href'), {$csrf}
		success: function(data) {
			jQuery('#video-grid').yiiGridView('update');

		},
		error: function(XHR) {

		}
	});
	return false;
}
EOD
                        ),

                    'off' => array(
                        'label' => 'Выключить',
                        'icon' => 'icon-exclamation-sign',
                        'visible' => ' $data["status"]!= "POWER OFF"',
                        'url' => 'Yii::app()->controller->createUrl("toggleOff",array("port"=>$data["n"]))',
                        'options' => array('class' => 'btn tip toggleOff'),
                        'click' => <<<EOD
function() {
	if(!confirm("Выключить порт?")) return false;

	jQuery.ajax({
		type: 'POST',
		url: jQuery(this).attr('href'), {$csrf}
		success: function(data) {
			jQuery('#video-grid').yiiGridView('update');

		},
		error: function(XHR) {

		}
	});
	return false;
}
EOD
        )
                    )
                )
            ),

        ));
        ?>
    </div>
</div>

<script>
    window.updateVoiceGrid = function() {
        // $("#provider-grid").yiiGridView('update');
        $("#video-grid").yiiGridView('update');
        setTimeout(updateVoiceGrid, 40000);
    }

    setTimeout(updateVoiceGrid, 40000)
</script>