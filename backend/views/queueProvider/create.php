<?php
/* @var $this QueueProviderController */
/* @var $model QueueProvider */
?>

<?php
$this->breadcrumbs=array(
	'Queue Providers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QueueProvider', 'url'=>array('index')),
	array('label'=>'Manage QueueProvider', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Добавление</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
