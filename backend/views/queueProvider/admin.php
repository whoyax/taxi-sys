<?php
/* @var $this QueueProviderController */
/* @var $model QueueProvider */


$this->breadcrumbs=array(
	'Queue Providers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List QueueProvider', 'url'=>array('index')),
	array('label'=>'Create QueueProvider', 'url'=>array('create')),
);


?>

<h1>Manage Queue Providers</h1>


<?php $this->widget('PGridView',array(
	'id'=>'queue-provider-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'widgetHeader'=>'',
	'columns'=>array(
		'id',
		'queue_id',
		'provider_id',
		'position',
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>