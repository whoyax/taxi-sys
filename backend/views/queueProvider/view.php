<?php
/* @var $this QueueProviderController */
/* @var $model QueueProvider */
?>

<?php
$this->breadcrumbs=array(
	'Queue Providers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List QueueProvider', 'url'=>array('index')),
	array('label'=>'Create QueueProvider', 'url'=>array('create')),
	array('label'=>'Update QueueProvider', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete QueueProvider', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QueueProvider', 'url'=>array('admin')),
);
?>

<h1>View QueueProvider #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'queue_id',
		'provider_id',
		'position',
	),
)); ?>