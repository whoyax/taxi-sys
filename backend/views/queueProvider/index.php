<?php
/* @var $this QueueProviderController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Queue Providers',
);

$this->menu=array(
	array('label'=>'Create QueueProvider','url'=>array('create')),
	array('label'=>'Manage QueueProvider','url'=>array('admin')),
);
?>

<h1>Queue Providers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>