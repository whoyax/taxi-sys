<?php
/* @var $this QueueProviderController */
/* @var $model QueueProvider */
?>

<?php
$this->breadcrumbs=array(
	'Queue Providers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List QueueProvider', 'url'=>array('index')),
	array('label'=>'Create QueueProvider', 'url'=>array('create')),
	array('label'=>'View QueueProvider', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage QueueProvider', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Редактирование</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>


