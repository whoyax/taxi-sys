<?php
/* @var $this GroupController */
/* @var $model Group */
?>

<?php
$this->breadcrumbs=array(
	'Groups'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Create Group', 'url'=>array('create')),
	array('label'=>'View Group', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
?>

<div class="widget row-fluid form-horizontal ">
    <div class="navbar">
        <div class="navbar-inner">
            <h6>Редактирование</h6>
        </div>
    </div>

    <?php $this->renderPartial('_form', array('model'=>$model)); ?>

</div>
