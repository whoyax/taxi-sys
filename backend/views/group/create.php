<?php
/* @var $this GroupController */
/* @var $model Group */
?>

<?php
$this->breadcrumbs=array(
	'Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
?>
<div class="widget row-fluid form-horizontal ">
    <div class="navbar">
        <div class="navbar-inner">
            <h6>Добавление</h6>
        </div>
    </div>

    <?php $this->renderPartial('_form', array('model'=>$model)); ?>

</div>