<?php
/* @var $this GroupController */
/* @var $model Group */
/* @var $form TbActiveForm */
?>

<div class="well">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'group-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>100)); ?>

    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6,'span'=>8)); ?>

    <div class="control-group">
        <label class="control-label" for="">Очередь</label>
        <div class="controls">
            <?php
            echo PnHtml::buttonGroupRadioField($model, 'queue_id', Queue::getOptionList());
            ?>
        </div>
    </div>


    <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array(
        'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
        'size'=>TbHtml::BUTTON_SIZE_LARGE,
    )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->