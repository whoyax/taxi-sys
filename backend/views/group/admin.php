<?php
/* @var $this GroupController */
/* @var $model Group */


$this->breadcrumbs=array(
	'Groups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Create Group', 'url'=>array('create')),
);

?>

<h1>Группы устройств</h1>


<?php $this->widget('PGridView',array(
	'id'=>'group-grid',
    'widgetHeader' => 'Группы',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		'name',
		'description',
		'queue.name',
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>