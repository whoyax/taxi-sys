<h5 class="widget-name"> Антивирус </h5>
<div class="row-fluid">
    <div class="span12">

        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        $this->widget('PGridView', array(
            'id' => 'clients-grid',
            'dataProvider' => $clients->search(),
            'widgetHeader' => '<i class="icon-tasks"></i>Рабочие станции',
            'addButton' => false,

            'columns' => array(

                array(
                    'name' => 'Name',
                    'htmlOptions' => array('style' => 'width: 4%')
                ),
                array(
                    'name' => 'IP',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'OSName',
                    'htmlOptions' => array('style' => 'width: 10%')
                ),
                array(
                    'name' => 'ThreatDBVersionName',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'ProductName',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'ClientLastConnected',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'LastScanDate',
                    'htmlOptions' => array('style' => 'width: 3%'),

                ),
            ),
        ));


        $this->widget('PGridView', array(
            'id' => 't-grid',
            'dataProvider' => $threatLog->search(),
            'widgetHeader' => '<i class="icon-tasks"></i>Угрозы',
            'addButton' => false,

            'columns' => array(
                array(
                    'name' => 'DateReceived',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'DateOccurred',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'UserName',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'client.Name',
                    'htmlOptions' => array('style' => 'width: 4%')
                ),
                array(
                    'name' => 'Virus',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'Name',
                    'htmlOptions' => array('style' => 'width: 5%'),

                ),
                array(
                    'name' => 'Info',
                    'htmlOptions' => array('style' => 'width: 5%'),

                ),
            ),
        ));


        $this->widget('PGridView', array(
            'id' => 's-grid',
            'dataProvider' => $scanLog->search(),
            'widgetHeader' => '<i class="icon-tasks"></i>Сканирования',
            'addButton' => false,

            'columns' => array(
                array(
                    'name' => 'DateOccurred',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'client.Name',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'Description',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'Scanned',
                    'htmlOptions' => array('style' => 'width: 1%')
                ),
                array(
                    'name' => 'Infected',
                    'htmlOptions' => array('style' => 'width: 1%'),
                ),
                array(
                    'name' => 'Cleaned',
                    'htmlOptions' => array('style' => 'width: 1%'),

                ),
            ),
        ));


        $this->widget('PGridView', array(
            'id' => 'q-grid',
            'dataProvider' => $quarantine->search(),
            'widgetHeader' => '<i class="icon-tasks"></i>Карантин',
            'addButton' => false,

            'columns' => array(
                array(
                    'name' => 'DateReceived',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'DateOccurredFirst',
                    'htmlOptions' => array('style' => 'width: 10%')
                ),
                array(
                    'name' => 'DateOccurredLast',
                    'htmlOptions' => array('style' => 'width: 3%')
                ),
                array(
                    'name' => 'client.Name',
                    'htmlOptions' => array('style' => 'width: 4%')
                ),
                array(
                    'name' => 'Reason',
                    'htmlOptions' => array('style' => 'width: 5%')
                ),
                array(
                    'name' => 'FilePath',
                    'htmlOptions' => array('style' => 'width: 3%'),

                ),
            ),
        ));

        ?>
    </div>
</div>

<script>

</script>
