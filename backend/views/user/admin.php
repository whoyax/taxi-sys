<?php
/* @var $this UserController */
/* @var $model User */


$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);

?>

<h1>Управление пользователями</h1>

<?php $this->widget('PGridView',array(
	'id'=>'user-grid',
    'widgetHeader' => 'Пользователи',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'email',
		'login',
		'name',
//		'password',
		'phone',
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>
