<?php
/* @var $this DnsController */
/* @var $model DnsSettings */


$this->breadcrumbs=array(
	'Dns Settings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DnsSettings', 'url'=>array('index')),
	array('label'=>'Create DnsSettings', 'url'=>array('create')),
);


?>

<h1>Настройки API DNS</h1>


<?php $this->widget('PGridView',array(
	'id'=>'dns-settings-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'widgetHeader'=>'',
	'columns'=>array(
// 		'id',
        'domain_name',
        'service_name',
        // 'service_url',
        'auto_dns' => array(
            'name' => 'auto_dns',
			'header' => 'Автопереключение IP',
            'value' => '$data->auto_dns ? "Да" : "Нет"'
        ),
        'queue_id' => array(
            'name' => 'queue_id',
            'value' => '$data->queue ? $data->queue->name : ""',
            'filter' => TbHtml::listData(Queue::model()->findAll(), 'id', 'name'),
        ),
		'current.ip',
		/*
		'username',
		'authcode',
		*/
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>