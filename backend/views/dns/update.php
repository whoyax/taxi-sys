<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
?>

<?php
$this->breadcrumbs=array(
	'Dns Settings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DnsSettings', 'url'=>array('index')),
	array('label'=>'Create DnsSettings', 'url'=>array('create')),
	array('label'=>'View DnsSettings', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DnsSettings', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Редактирование</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>


