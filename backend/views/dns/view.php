<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
?>

<?php
$this->breadcrumbs=array(
	'Dns Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DnsSettings', 'url'=>array('index')),
	array('label'=>'Create DnsSettings', 'url'=>array('create')),
	array('label'=>'Update DnsSettings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DnsSettings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DnsSettings', 'url'=>array('admin')),
);
?>

<h1>View DnsSettings #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'service_name',
		'service_url',
		'active_dns',
		'auto_dns',
		'domain_name',
		'username',
		'authcode',
	),
)); ?>