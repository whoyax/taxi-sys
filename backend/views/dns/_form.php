<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
/* @var $form TbActiveForm */
?>

<div class="well">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'dnssettings-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model, 'domain_name', array('span' => 5, 'maxlength' => 250)); ?>


    <?php echo $form->textFieldControlGroup($model, 'service_name', array('span' => 5, 'maxlength' => 100)); ?>

    <?php echo $form->textFieldControlGroup($model, 'service_url', array('span' => 5, 'maxlength' => 250)); ?>

    <?php echo $form->textFieldControlGroup($model, 'username', array('span' => 5, 'maxlength' => 250)); ?>

    <?php echo $form->textFieldControlGroup($model, 'authcode', array('span' => 5, 'maxlength' => 250)); ?>

    <?php echo $form->checkBoxControlGroup($model, 'auto_dns', array('span' => 5)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'queue_id', Queue::getOptionList(), array('span' => 5)); ?>

    <?php // echo $form->dropDownListControlGroup($model, 'set_ip', CHtml::listData(Provider::model()->findAll(), 'ext_ip', 'name'), array('span' => 5)); ?>

    <div class="control-group">
        <label class="control-label" for="">Установить Ip</label>

        <div class="controls">
            <?php
                echo PnHtml::buttonGroupRadioField($model, 'set_ip', array('' => 'Очистить') + CHtml::listData(Provider::model()->findAll(), 'ext_ip', 'name'), array('span' => 5));
            ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => TbHtml::BUTTON_COLOR_PRIMARY,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->