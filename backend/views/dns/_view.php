<?php
/* @var $this DnsController */
/* @var $data DnsSettings */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_name')); ?>:</b>
	<?php echo CHtml::encode($data->service_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_url')); ?>:</b>
	<?php echo CHtml::encode($data->service_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active_dns')); ?>:</b>
	<?php echo CHtml::encode($data->active_dns); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('auto_dns')); ?>:</b>
	<?php echo CHtml::encode($data->auto_dns); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('domain_name')); ?>:</b>
	<?php echo CHtml::encode($data->domain_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('authcode')); ?>:</b>
	<?php echo CHtml::encode($data->authcode); ?>
	<br />

	*/ ?>

</div>