<?php
/* @var $this DnsController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Dns Settings',
);

$this->menu=array(
	array('label'=>'Create DnsSettings','url'=>array('create')),
	array('label'=>'Manage DnsSettings','url'=>array('admin')),
);
?>

<h1>Dns Settings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>