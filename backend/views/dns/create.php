<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
?>

<?php
$this->breadcrumbs=array(
	'Dns Settings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DnsSettings', 'url'=>array('index')),
	array('label'=>'Manage DnsSettings', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Добавление</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
