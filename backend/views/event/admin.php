<h5 class="widget-name"> Журнал событий </h5>

<style>
    .control-group {
        border-top: none;
        padding-top: 10px;
        padding-bottom: 5px;
    }
</style>

<div class="row-fluid">
    <div class="span12">

        <?php
        $this->widget('PGridView', array(
            'id' => 'event-grid',
            'dataProvider' => $model->search(),
            'htmlOptions' => array('class' => 'margin span12'),
            'addButton' => array(
                'url' => $this->createUrl('addEvent'),
                'icon' => 'plus',
                'htmlOptions' => array(
                    'class' => 'btn pull-right btn-success ',
                    'id' => 'add-param',
                    'data-toggle' => 'modal',
                    'data-target' => '#addEventDialog'
                )
            ),
            'filter' => $model,
            'widgetHeader' => 'События',
            'columns' => array(
                // 'id',

                array(
                    'name' => 'date_create',
                    'htmlOptions' => array('style' => 'width: 7%', 'class' => 'align-left'),
                    'header' => 'Дата',
                ),

                array(
                    'name' => 'type_id',
                    'htmlOptions' => array('style' => 'width: 15%'),
                    'header' => 'Тип',
                    'filter' => CHtml::listData(EventType::model()->findAll(), 'id', 'title'),
                    'value' => function($data) {
                        return $data->type->title;
                    }
                ),

                array(
                    'name' => 'title',
                    'htmlOptions' => array('style' => 'width: 75%', 'class' => 'align-left'),
                    'header' => 'Название',

                ),

                array(
                    'name' => 'user_id',
                    'htmlOptions' => array('style' => 'width: 75%', 'class' => 'align-left'),
                    'header' => 'Пользователь',
                    'filter' => CHtml::listData(User::model()->findAll(), 'id', 'title'),
                    'value' => function($data) {
                        return $data->user->title;
                    }

                ),

                array(
                    'class' => 'PButtonColumn',
                    'deleteButtonUrl' => 'Yii::app()->controller->createUrl("deleteCable",array("id"=>$data->primaryKey))',
                    'buttons' => array(
                        'view' => array(
                            'visible' => 'true',
                        ),
                        'update' => array(
                            'visible' => 'false',
                        ),
                        'delete' => array(
                            'visible' => 'false',
                        )

                    )
                ),
            ),
        )); ?>

    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addEventDialog',
    'header' => 'Добавить',
    'content' => $this->renderPartial('_form', array('model' => $model), true),
    'onShow' => 'js: function() { $("#Event_type_id, #Event_title, #Event_body").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveEventTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<script>

    function saveTrigger(formId, callback) {

        var form = $(formId);
        var settings = form.data('settings') ;
        settings.submitting = true ;
        $.fn.yiiactiveform.validate(form, function(messages) {
            $.each(settings.attributes, function () {
                $.fn.yiiactiveform.updateInput(this, messages, form);
            });

            if($.isEmptyObject(messages)) { // If there are no error messages all data are valid
                callback();
            } else {
                settings.submitting = false ;
            }
        });
    }

    function saveEventTrigger() {
        return saveTrigger("#event-form", function(){
            $.post(
                'addEvent',
                {
                    "Event" : {
                        "type_id" : $("#event-form #Event_type_id").val(),
                        "title" : $("#event-form #Event_title").val(),
                        "body" : $("#event-form #Event_body").val(),
                    }
                },
                function(){
                    $("#event-grid").yiiGridView("update");
                }
            );
            $("#addEventDialog").modal("hide");
        });
    }


</script>

<?php
$data = CHtml::listData(Device::model()->findAll(), 'id', 'name');
$ret = array();
foreach ( $data as $id => $title )
{
    $ret[] = array('value' => $id, 'text' => $title);
}
?>
</div>
<script>
    function getDeviceList() {
        return <?= CJSON::encode($ret) ?>;
    }
</script>