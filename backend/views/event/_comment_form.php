<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
/* @var $form TbActiveForm */
?>

<div class="row">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'comment-form',
        'action' => $this->createUrl('addEvent'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textAreaControlGroup($model,'body',array('span'=>12, 'style' => 'width: 100%; height: 12em')); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->