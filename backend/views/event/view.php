<?php
/* @var $this UserController */
/* @var $model User */
?>


<style>
    .control-group {
        border-top: none;
        padding-top: 10px;
        padding-bottom: 5px;
    }
</style>

<?php
$this->breadcrumbs = array(
    'Users' => array('index'),
    // $model->name,
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Update User', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete User', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>

    <h1>Просмотр события #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => array(
        // 'id',
        'date_create',
        'type.title',
        'user.title',
        'title',
        'body',
    ),
)); ?>




<div class="row-fluid">
    <div class="span12">

        <?php

        $eventProvider = new EventComment('search');
        $eventProvider->unsetAttributes();
        $eventProvider->event_id = $model->id;

        $this->widget('SimplePGridView', array(
            'id' => 'comment-grid',
            'dataProvider' => $eventProvider->search(),
            'htmlOptions' => array('class' => 'margin span12'),
            'addButton' => array(
                'url' => $this->createUrl('addComment'),
                'icon' => 'plus',
                'htmlOptions' => array(
                    'class' => 'btn pull-right btn-success ',
                    'id' => 'add-comment',
                    'data-toggle' => 'modal',
                    'data-target' => '#addCommentDialog'
                )
            ),

            'widgetHeader' => 'Комментарии',
            'columns' => array(
                // 'id','columns' => array(
                // 'id',

                array(
                    'name' => 'date_create',
                    'htmlOptions' => array('style' => 'width: 7%', 'class' => 'align-left'),
                    'header' => 'Дата',
                ),
                array(
                    'name' => 'user.title',
                    'htmlOptions' => array('style' => 'width: 10%', 'class' => 'align-left'),
                    'header' => 'Пользователь',
                    'value' => function($data) {
                        return $data->user->title;
                    }

                ),

                array(
                    'name' => 'body',
                    'htmlOptions' => array('style' => 'width: 75%', 'class' => 'align-left'),
                    'header' => 'Комментарий',

                ),

            ),
        )); ?>

    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addCommentDialog',
    'header' => 'Добавить комментарий',
    'content' => $this->renderPartial('_comment_form', array('model' => new EventComment()), true),
    'onShow' => 'js: function() { $("#Comment_body").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveCommentTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<script>

    function saveTrigger(formId, callback) {

        var form = $(formId);
        var settings = form.data('settings') ;
        settings.submitting = true ;
        $.fn.yiiactiveform.validate(form, function(messages) {
            $.each(settings.attributes, function () {
                $.fn.yiiactiveform.updateInput(this, messages, form);
            });

            if($.isEmptyObject(messages)) { // If there are no error messages all data are valid
                callback();
            } else {
                settings.submitting = false ;
            }
        });
    }

    function saveCommentTrigger() {
        return saveTrigger("#comment-form", function(){
            $.post(
                'addComment/<?= $model->id ?>',
                {
                    "EventComment" : {
                        "body" : $("#EventComment_body").val(),
                    }
                },
                function(){
                    $("#comment-grid").yiiGridView("update");
                }
            );
            $("#addCommentDialog").modal("hide");
        });
    }


</script>
