<?php
/* @var $this DnsController */
/* @var $model DnsSettings */
/* @var $form TbActiveForm */
?>

<div class="1">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'cable-form',
        'action' => $this->createUrl('addCable'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>100)); ?>
            <?php echo $form->textFieldControlGroup($model,'num',array('span'=>5,'maxlength'=>50)); ?>
            <?php echo $form->textAreaControlGroup($model,'description',array('span'=>10)); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->