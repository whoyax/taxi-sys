<h5 class="widget-name"> Порты </h5>
<div class="row-fluid">
    <div class="span12">

        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        $this->widget('SimplePGridView', array(
            'id' => 'videoport-grid',
            'dataProvider' => new CArrayDataProvider($videoPorts, array('pagination' => array('pageSize' => 30), 'keyField' => 'n')),
            'widgetHeader' => '<i class="icon-tasks"></i> D-Link DES-1210-28P',

            'columns' => array(
                // 'id',

                array(
                    'name' => 'n',
                    'header' => '#',
                    'htmlOptions' => array('style' => 'width: 1%')
                ),

                array(
                    'name' => 'name',
                    'header' => 'name',
                    'htmlOptions' => array('style' => 'width: 11%')
                ),

                array(
                    'class' => 'yiiwheels.widgets.editable.WhEditableColumn',
                    'name' => 'device',
                    'headerHtmlOptions' => array('class' => 'span3'),
                    'header' => 'Устройство',
                    'sortable' => false,
                    'editable' => array(
                        'url' => $this->createUrl('commutation/setPortDevice', array('main' => 58)),
                        'type' => 'select2',
                        'success' => 'js: function(response, newValue) { jQuery("#videoport-grid").yiiGridView("update"); }',
                        'inputclass' => 'span12',
                        'mode' => 'inline',
                        'source' => 'js: function() { return getDeviceList(); }',
                    )
                ),

                array(
                    'name' => 'ping',
                    'header' => 'Сеть',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => ' width: 20%'),
                    'value' => function ($data)
                    {
                        if ( $id = (int)$data['device'] )
                        {
                            if ( $device = Device::model()->findByPk($id) )
                            {
                                $class = NetworkStatus::getStatusClass($device->pingStatus->result);

                                return " <span class='label label-{$class}'>&nbsp;</span> {$device->ip}";
                            }
                        }

                        return "";
                    },
                ),

                array(
                    'name' => 'status',
                    'header' => 'Статус PoE',
                    'htmlOptions' => array('style' => 'width: 5%'),
                    'type' => 'raw',
                    'value' => function ($data)
                    {

                        $t = '';


                        if(!empty($data['status']))
                        {
                            if ( $data['status'] == "POWER ON" )
                            {

                                $t .= "<span class='label label-success '>{$data['status']}</span>";
                            }
                            elseif ( $data['status'] == "POWER OFF" )
                            {
                                $t .= "<span class='label '>{$data['status']}</span>";
                            }
                            else
                            {
                                $t .= "<span class='label label-warning '>{$data['status']}</span>";
                            }
                            $t .= "<br/>";

                        }

                        return $t;
                    }
                ),
            ),

        ));
        ?>
    </div>
</div>


<div class="row-fluid">
    <div class="span12">


        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        $this->widget('SimplePGridView', array(
            'id' => 'mainport-grid',
            'dataProvider' => new CArrayDataProvider($mainPorts, array('pagination' => array('pageSize' => 30), 'keyField' => 'n')),
            'widgetHeader' => '<i class="icon-tasks"></i> Mikrotik Routerboard ',

            'columns' => array(
                // 'id',

                array(
                    'name' => 'n',
                    'header' => '#',
                    'htmlOptions' => array('style' => 'width: 1%')
                ),

                array(
                    'name' => 'name',
                    'header' => 'name',
                    'htmlOptions' => array('style' => 'width: 10%')
                ),

                array(
                    'class' => 'yiiwheels.widgets.editable.WhEditableColumn',
                    'name' => 'device',
                    'headerHtmlOptions' => array('class' => 'span3'),
                    'header' => 'Устройство',
                    'sortable' => false,
                    'editable' => array(
                        'url' => $this->createUrl('commutation/setPortDevice', array('main' => 1)),
                        'type' => 'select2',
                        'success' => 'js: function(response, newValue) { jQuery("#mainport-grid").yiiGridView("update"); }',
                        'inputclass' => 'span12',
                        'mode' => 'inline',
                        'source' => 'js: function() { return getDeviceList(); }',
                    )
                ),

                array(
                    'name' => 'ping',
                    'header' => 'Сеть',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => ' width: 20%'),
                    'value' => function ($data)
                    {
                        if ( $id = (int)$data['device'] )
                        {
                            if ( $device = Device::model()->findByPk($id) )
                            {
                                $class = NetworkStatus::getStatusClass($device->pingStatus->result);

                                return " <span class='label label-{$class}'>&nbsp;</span> {$device->ip}";
                            }
                        }

                        return "";
                    },
                ),

                array(
                    'name' => 'status',
                    'header' => 'Статус PoE',
                    'htmlOptions' => array('style' => 'width: 5%'),
                    'type' => 'raw',
                    'value' => function ($data)
                    {

                        $t = '';

                        if(!empty($data['status']))
                        {
                            if ( $data['status'] == "POWER ON" )
                            {

                                $t .= "<span class='label label-success '>{$data['status']}</span>";
                            }
                            elseif ( $data['status'] == "POWER OFF" )
                            {
                                $t .= "<span class='label '>{$data['status']}</span>";
                            }
                            else
                            {
                                $t .= "<span class='label label-warning '>{$data['status']}</span>";
                            }
                            $t .= "<br/>";

                        }

                        return $t;
                    }
                ),
            ),

        ));
        ?>
    </div>
</div>


<div class="row-fluid">
    <div class="span12">


        <?php
        /**
         * Created by PhpStorm.
         * User: Tom
         * Date: 01.01.2016
         * Time: 22:45
         */


        $this->widget('SimplePGridView', array(
            'id' => 'switchport-grid',
            'dataProvider' => new CArrayDataProvider($switchPorts, array('pagination' => array('pageSize' => 30), 'keyField' => 'n')),
            'widgetHeader' => '<i class="icon-tasks"></i> Mikrotik Switch ',

            'columns' => array(
                // 'id',

                array(
                    'name' => 'n',
                    'header' => '#',
                    'htmlOptions' => array('style' => 'width: 1%')
                ),

                array(
                    'name' => 'name',
                    'header' => 'name',
                    'htmlOptions' => array('style' => 'width: 10%')
                ),

                array(
                    'class' => 'yiiwheels.widgets.editable.WhEditableColumn',
                    'name' => 'device',
                    'headerHtmlOptions' => array('class' => 'span3'),
                    'header' => 'Устройство',
                    'sortable' => false,
                    'editable' => array(
                        'url' => $this->createUrl('commutation/setPortDevice', array('main' => 2)),
                        'type' => 'select2',
                        'success' => 'js: function(response, newValue) { jQuery("#switchport-grid").yiiGridView("update"); }',
                        'inputclass' => 'span12',
                        'mode' => 'inline',
                        'source' => 'js: function() { return getDeviceList(); }',
                    )
                ),

                array(
                    'name' => 'ping',
                    'header' => 'Сеть',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => ' width: 20%'),
                    'value' => function ($data)
                    {
                        if ( $id = (int)$data['device'] )
                        {
                            if ( $device = Device::model()->findByPk($id) )
                            {
                                $class = "";
                                if( $device->pingStatus ) {
                                    $class = NetworkStatus::getStatusClass($device->pingStatus->result);
                                }
                                return " <span class='label label-{$class}'>&nbsp;</span> {$device->ip}";

                            }
                        }

                        return "";
                    },
                ),

                array(
                    'name' => 'status',
                    'header' => 'Статус PoE',
                    'htmlOptions' => array('style' => 'width: 5%'),
                    'type' => 'raw',
                    'value' => function ($data)
                    {

                        $t = '';


                        if(!empty($data['status']))
                        {
                            if ( $data['status'] == "POWER ON" )
                            {

                                $t .= "<span class='label label-success '>{$data['status']}</span>";
                            }
                            elseif ( $data['status'] == "POWER OFF" )
                            {
                                $t .= "<span class='label '>{$data['status']}</span>";
                            }
                            else
                            {
                                $t .= "<span class='label label-warning '>{$data['status']}</span>";
                            }
                            $t .= "<br/>";

                        }

                        return $t;
                    }
                ),
            ),

        ));
        ?>
    </div>
</div>


<div class="row-fluid">
    <div class="span12">

        <?php

        $cableProvider = new Cable('search');
        $cableProvider->unsetAttributes();
        // $paramProvider->device_id = ( $model->getIsNewRecord() ) ? -1 : $model->id;


        $this->widget('SimplePGridView', array(
            'id' => 'cable-grid',
            'dataProvider' => $cableProvider->search(),
            'htmlOptions' => array('class' => 'margin span10'),
            'addButton' => array(
                'url' => $this->createUrl('addCable'),
                'icon' => 'plus',
                'htmlOptions' => array(
                    'class' => 'btn pull-right btn-success ',
                    'id' => 'add-param',
                    'data-toggle' => 'modal',
                    'data-target' => '#addCableDialog'
                )
            ),

            'widgetHeader' => 'Кабель',
            'columns' => array(
                // 'id',

                array(
                    'name' => 'num',
                    'htmlOptions' => array('style' => 'width: 7%', 'class' => 'align-left')
                ),

                array(
                    'name' => 'name',
                    'htmlOptions' => array('style' => 'width: 15%')
                ),

                array(
                    'name' => 'description',
                    'htmlOptions' => array('style' => 'width: 75%', 'class' => 'align-left')
                ),

                array(
                    'class' => 'PButtonColumn',
                    'deleteButtonUrl' => 'Yii::app()->controller->createUrl("deleteCable",array("id"=>$data->primaryKey))',
                    'buttons' => array(
                        'view' => array(
                            'visible' => 'false',
                        ),
                        'update' => array(
                            'visible' => 'false',
                        ),
                        'delete' => array(
                            'visible' => 'true',
                        )

                    )
                ),
            ),
        )); ?>

    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'addCableDialog',
    'header' => 'Добавить кабель',
    'content' => $this->renderPartial('_cable_form', array('model' => $cableProvider), true),
    'onShow' => 'js: function() { $("#Cable_name, #Cable_num, #Cable_description").val(""); }',
    'footer' => array(
        TbHtml::button(
            'Добавить',
            array(
                //'data-dismiss' => 'modal',
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                'onClick' => 'saveCableTrigger()'
            )
        ),
        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
    ),
)); ?>

<script>

    function saveTrigger(formId, callback) {

        var form = $(formId);
        var settings = form.data('settings') ;
        settings.submitting = true ;
        $.fn.yiiactiveform.validate(form, function(messages) {
            $.each(settings.attributes, function () {
                $.fn.yiiactiveform.updateInput(this, messages, form);
            });

            if($.isEmptyObject(messages)) { // If there are no error messages all data are valid
                callback();
            } else {
                settings.submitting = false ;
            }
        });
    }

    function saveCableTrigger() {
        return saveTrigger("#cable-form", function(){
            $.post(
                'addCable',
                {
                    "Cable" : {
                        "name" : $("#Cable_name").val(),
                        "num" : $("#Cable_num").val(),
                        "description" : $("#Cable_description").val(),
                    }
                },
                function(){
                    $("#cable-grid").yiiGridView("update");
                }
            );
            $("#addCableDialog").modal("hide");
        });
    }


</script>

<?php
$data = CHtml::listData(Device::model()->findAll(), 'id', 'name');
$ret = array();
foreach ( $data as $id => $title )
{
    $ret[] = array('value' => $id, 'text' => $title);
}
?>
</div>
<script>
    function getDeviceList() {
        return <?= CJSON::encode($ret) ?>;
    }
</script>