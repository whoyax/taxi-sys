<?php
/* @var $this CableController */
/* @var $model Cable */
?>

<?php
$this->breadcrumbs=array(
	'Cables'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cable', 'url'=>array('index')),
	array('label'=>'Manage Cable', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Добавление</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
