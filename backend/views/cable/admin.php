<?php
/* @var $this CableController */
/* @var $model Cable */


$this->breadcrumbs=array(
	'Cables'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Cable', 'url'=>array('index')),
	array('label'=>'Create Cable', 'url'=>array('create')),
);


?>

<h1>Manage Cables</h1>


<?php $this->widget('PGridView',array(
	'id'=>'cable-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'widgetHeader'=>'',
	'columns'=>array(
		'id',
		'group_id',
		'name',
		'num',
		'description',
		array(
			'class'=>'PButtonColumn',
		),
	),
)); ?>