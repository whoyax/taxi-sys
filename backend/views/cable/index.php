<?php
/* @var $this CableController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Cables',
);

$this->menu=array(
	array('label'=>'Create Cable','url'=>array('create')),
	array('label'=>'Manage Cable','url'=>array('admin')),
);
?>

<h1>Cables</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>