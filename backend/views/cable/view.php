<?php
/* @var $this CableController */
/* @var $model Cable */
?>

<?php
$this->breadcrumbs=array(
	'Cables'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Cable', 'url'=>array('index')),
	array('label'=>'Create Cable', 'url'=>array('create')),
	array('label'=>'Update Cable', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Cable', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cable', 'url'=>array('admin')),
);
?>

<h1>View Cable #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'group_id',
		'name',
		'num',
		'description',
	),
)); ?>