<?php
/* @var $this CableController */
/* @var $model Cable */
?>

<?php
$this->breadcrumbs=array(
	'Cables'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cable', 'url'=>array('index')),
	array('label'=>'Create Cable', 'url'=>array('create')),
	array('label'=>'View Cable', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Cable', 'url'=>array('admin')),
);
?>

    <div class="widget row-fluid form-horizontal ">
        <div class="navbar">
            <div class="navbar-inner">
                <h6>Редактирование</h6>
            </div>
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>


