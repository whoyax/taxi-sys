

<div class="row-fluid">
    <div class="span8">
        <?php $this->renderPartial("/dashboard/_providers", array('provider' => $provider)); ?>
    </div>
    <div class="span4">
        <?php $this->renderPartial("/dashboard/_queues", array('queue' => $queue, 'provider' => $provider)); ?>
    </div>
</div>

    <div class="row-fluid">
        <div class="span4 ">
            <div class="widget">
                <div class="navbar">
                    <div class="navbar-inner">
                        <h6>Маршрутизатор</h6>
                    </div>
                </div>
                <div class="well body">
                    <ul class="stats-details">
                        <li>
                            <strong>Текущая нагрузка:</strong>
                            <span>(проценты)</span>
                        </li>
                        <li>
                            <div class="number">
                                <span id="mikrotik-1-current"></span>
                            </div>
                        </li>
                    </ul>
                    <div id="mikrotik-1-portload" class="graph" style="padding: 20px 25% 0px" ></div>
                    <div id="mikrotik-1-load" class="graph " style="position: relative; display: block; width: 100%; height: 100px; margin: -5px 0px;"></div>
                </div>

            </div>
        </div>

        <div class="span4 ">
            <div class="widget">
                <div class="navbar">
                    <div class="navbar-inner">
                        <h6>Коммутатор</h6>
                    </div>
                </div>
                <div class="well body">
                    <ul class="stats-details">
                        <li>
                            <strong>Текущая нагрузка:</strong>
                            <span>(проценты)</span>
                        </li>
                        <li>
                            <div class="number">
                                <span id="mikrotik-2-current"></span>
                            </div>
                        </li>
                    </ul>
                    <div id="mikrotik-2-portload" class="graph" style="padding: 20px 15% 0px" ></div>
                    <div id="mikrotik-2-load" class="graph " style="position: relative; display: block; width: 100%; height: 100px; margin: -5px 0px;"></div>
                </div>
            </div>
        </div>

        <div class="span4">
            <?php $this->renderPartial("/dashboard/_dns", array('dns' => $dns)); ?>
        </div>

    </div>
