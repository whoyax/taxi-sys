<?php
$controller = $this;
$provider->unsetAttributes();
$this->widget('SimplePGridView', array(
    'id' => 'provider-grid',
    'dataProvider' => $provider->search(),
    'widgetHeader' => '<i class="icon-globe"></i>Провайдеры',

    'columns' => array(
        // 'id',
        array(
            'name' => 'name',
            'htmlOptions' => array('style' => 'width: 30%')

        ),
//            'ext_ip',
        /*
        'ping_timeout',
        'ping_num',
         */
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => function ($data) {
                    return TbHtml::tag('div', array('id' => 'provider_' . $data->id . '_status'),
                        NetworkStatus::getStatusLabel($data->getStatus(), $data->status_mode));
                }
        ),


        array(
            'name' => 'ping',
            'type' => 'raw',
            'htmlOptions' => array('style' => ' width: 20%'),
            'value' => function($data) {
                    return "<div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='provider_{$data->id}_ping'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>";
                },
        ),

        array(
            'name' => 'bandwidth',
            'type' => 'raw',
            'htmlOptions' => array('style' => ' width: 20%'),
            'value' => function($data) {
                    return "<div class='align-center ' style='position: relative; display: block; height:50px; width:100%; ' id='provider_{$data->id}_bandwidth'><img src='/p/img/elements/loaders/4s.gif' alt='loading' /></div>";
                },
        ),

        array(
            'name' => 'loss',
            'type' => 'raw',
            'value' => function ($data) {
                    return TbHtml::tag('div', array('id' => 'provider_' . $data->id . '_loss'),
                        $data->loss);
                }
        ),

        array(
            'class' => 'PButtonColumn',
            'updateButtonUrl' => 'Yii::app()->controller->createUrl("provider/update",array("id"=>$data->primaryKey))',
            'htmlOptions' => array('style' => 'width: 3%')
        ),
    ),
));
?>
<script>

    function drawProviderPlot(provider) {

        var pings = [];
        for (var i = 0; i < provider.pings.length; ++i) {
            pings.push([i, parseInt(provider.pings[i], 10)]);
        }

        var divId = $("#provider_" + provider.id + "_ping");
        divId.html('');


        var options = {
            yaxis: { min: 0, max: Math.max.apply(null, provider.pings)+50, reserveSpace: false /* , color: "#ffffff" */},
            xaxis: { show: false },
            // colors: ["#ffffff"],
            // grid: { hoverable: true, clickable: true, tickColor: "rgba(255,255,255,0.16)" },
            series: {
                lines: {
                    lineWidth: 2,
                    fill: false,
                    fillColor: { colors: [ { opacity: 0.5 }, { opacity: 0.2 } ] },
                    show: true
                },
                points: {
                    show: false,
                    radius: 2.5,
                    fill: true,
                    fillColor: "#55bc75",
                    symbol: "circle",
                    lineWidth: 1.1
                }
            }
        };
        var plot = $.plot(divId,[ pings], options);
        return plot;
    }

    function drawBandwidth(divId, rxStat, txStat) {

        divId.html('');

        var max = Math.max.apply(null, rxStat);
        var maxTx = Math.max.apply(null, txStat);
        if (maxTx > max) max = maxTx;

        var rx = [];
        for (var i = 0; i < rxStat.length; ++i) {
            rx.push([i, parseInt(rxStat[i], 10)]);
        }

        var tx = [];
        for (var i = 0; i < txStat.length; ++i) {
            tx.push([i, parseInt(txStat[i], 10)]);
        }

        $.plot(divId,
            [ rx, tx ], {
                series: {
                    lines: { show: true },
                    points: { show: false }
                },
                grid: { hoverable: false, clickable: false },
                yaxis: { min: 0, max: max + 100},
                xaxis: { show: false }
            })
    }

    function updateStatus(provider) {
        var divId = $("#provider_" + provider.id + "_status");
        divId.html('');
        divId.html(provider.status);

    }

    function updateLoss(provider) {
        var divId = $("#provider_" + provider.id + "_loss");
        divId.html('');
        divId.html(provider.loss);
    }

    function drawPings(providers) {
        providers.forEach(function(provider) {
            drawProviderPlot(provider);
            updateStatus(provider);
            updateLoss(provider);
            var divId = $("#provider_" + provider.id + "_bandwidth");
            drawBandwidth(divId, provider.rx, provider.tx);
        });
    }

    $(function () {

        var providerQuery;

        function updateProvider() {

            if(providerQuery && providerQuery.readyState != 4){
                providerQuery.abort();
            }

            providerQuery = $.ajax({
                'url': '/provider/getPing',
                'type': 'get',
                'data': {},
                'cache' : false,
                'success': function(data){
                    var providers = JSON.parse(data);
                    drawPings(providers);
                    $('#queue-grid').yiiGridView('update');
                    $('#dns-grid').yiiGridView('update');
                },
                'error': function(request, status, error){
                    console.log(error);
                }
            });
            setTimeout(updateProvider, 15000);
        }
        updateProvider();
    });

</script>