<?php
$this->widget('SimplePGridView', array(
    'id' => 'dns-grid',
    'dataProvider' => $dns->search(),
    'widgetHeader' => '<i class="icon-globe"></i>Dns',
    'columns' => array(
        // 'id',
        'domain_name',
        'queue.name',
        'current.ip',
        array(
            'class' => 'PButtonColumn',
            'updateButtonUrl' => 'Yii::app()->controller->createUrl("dns/update",array("id"=>$data->primaryKey))',
            'htmlOptions' => array('style' => 'width: 3%')
        ),
    ),
));

?>
<script>
    window.updateDnsGrid = function() {
        $("#dns-grid").yiiGridView('update');
        setTimeout(updateDnsGrid, 30000);
    }
    //updateDnsGrid();
</script>