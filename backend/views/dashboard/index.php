<!-- Earnings stats widgets -->
<?php // $this->renderPartial('_stats') ?>

<!-- /earnings stats widgets -->

<div class="row-fluid">
    <div class="span8">
        <?php $this->renderPartial("_providers", array('provider' => $provider)); ?>
    </div>
    <div class="span4">
        <?php $this->renderPartial("_queues", array('queue' => $queue, 'provider' => $provider)); ?>
        <?php $this->renderPartial("_dns", array('dns' => $dns)); ?>
    </div>
</div>



<!-- Media datatable -->
<?php // $this->renderPartial('_mediatable') ?>

<!-- /media datatable -->

<script>



</script>

<?php // Yii::app()->clientScript->registerScript('gridUpdate', '   updateMainGrid();', CClientScript::POS_READY); ?>