<div class="row-fluid">

    <div class="span4">
        <div class="widget">
            <div class="navbar"><div class="navbar-inner"><h6>Earnings statistic</h6></div></div>
            <div class="well body">
                <ul class="stats-details">
                    <li>
                        <strong>Current balance</strong>
                        <span>latest update on 12:39 am</span>
                    </li>
                    <li>
                        <div class="number">
                            <a href="#" title="" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#" title=""><i class="icon-refresh"></i>Reload data</a></li>
                                <li><a href="#" title=""><i class="icon-calendar"></i>Change time period</a></li>
                                <li><a href="#" title=""><i class="icon-cog"></i>Parameters</a></li>
                                <li><a href="#" title=""><i class="icon-download-alt"></i>Download statement</a></li>
                            </ul>
                            <span>$6,458</span>
                        </div>
                    </li>
                </ul>
                <div class="graph" id="chart1"></div>
            </div>
        </div>
    </div>

    <div class="span4">
        <div class="widget">
            <div class="navbar"><div class="navbar-inner"><h6>Visitor statistics</h6></div></div>
            <div class="well body">
                <ul class="stats-details">
                    <li>
                        <strong>Today's visitors</strong>
                        <span>latest update on 4:42 pm</span>
                    </li>
                    <li>
                        <div class="number">
                            <a href="#" title="" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#" title=""><i class="icon-refresh"></i>Reload data</a></li>
                                <li><a href="#" title=""><i class="icon-calendar"></i>Change time period</a></li>
                                <li><a href="#" title=""><i class="icon-cog"></i>Parameters</a></li>
                                <li><a href="#" title=""><i class="icon-download-alt"></i>Download statement</a></li>
                            </ul>
                            <span>+12,127</span>
                        </div>
                    </li>
                </ul>
                <div class="graph" id="chart2"></div>
            </div>
        </div>
    </div>

    <div class="span4">
        <div class="widget">
            <div class="navbar"><div class="navbar-inner"><h6>Click statistics</h6></div></div>
            <div class="well body">
                <ul class="stats-details">
                    <li>
                        <strong>Total clicks</strong>
                        <span>latest update on 3:09 pm</span>
                    </li>
                    <li>
                        <div class="number">
                            <a href="#" title="" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#" title=""><i class="icon-refresh"></i>Reload data</a></li>
                                <li><a href="#" title=""><i class="icon-calendar"></i>Change time period</a></li>
                                <li><a href="#" title=""><i class="icon-cog"></i>Parameters<strong class="badge badge-info">9</strong></a></li>
                                <li><a href="#" title=""><i class="icon-download-alt"></i>Download statement</a></li>
                            </ul>
                            <span>168k+</span>
                        </div>
                    </li>
                </ul>
                <div class="graph" id="chart3"></div>
            </div>
        </div>
    </div>
</div>