<?php
/* @var $this DeviceController */
/* @var $model Device */


$this->breadcrumbs=array(
    'Devices'=>array('index'),
    'Manage',
);



?>



<div class="widget">
    <div class="navbar">
        <div class="navbar-inner">
            <h6>Devices</h6>
        </div>
    </div>

    <div class="table-overflow">
        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'device-grid',
            'dataProvider'=>$model->search(),
            'htmlOptions' => array('class' => 'dataTables_wrapper'),
            'filterPosition' => 'header',
            'itemsCssClass' => 'table table-striped table-bordered table-checks dataTable',
            'template' => "{items}\n<div class=\"datatable-footer\"><div class=\"dataTables_info\">{summary}</div><div class=\"dataTables_paginate paging_full_numbers\">{pager}</div></div>",
            'filter'=>$model,
            'columns'=>array(
                // 'id',
                'group_id',
                'name',
                'adr',
//                'notify',
//                'notify_problems',
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '<ul class="navbar-icons"><li>{view}</li> <li>{update}</li> <li>{delete}</li></ul>'
                ),
            ),
        )); ?>
    </div>
</div>

