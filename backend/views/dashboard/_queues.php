<?php
$provider->unsetAttributes();
$this->widget('SimplePGridView', array(
    'id' => 'queue-grid',
    'dataProvider' => $queue->search(),
    'widgetHeader' => '<i class="icon-cogs"></i>Очереди',
    'columns' => array(
        // 'id',
        'name',
        'activeProvider.provider.name',
        array(
            'class' => 'PButtonColumn',
            'updateButtonUrl' => 'Yii::app()->controller->createUrl("queue/update",array("id"=>$data->primaryKey))',
            'htmlOptions' => array('style' => 'width: 3%')
        ),
    ),
));

?>
<script>
    window.updateQueueGrid = function() {
        // $("#provider-grid").yiiGridView('update');
        $("#queue-grid").yiiGridView('update');
        setTimeout(updateQueueGrid, 15000);
    }
</script>