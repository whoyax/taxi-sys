<div class="widget">
<div class="navbar">
    <div class="navbar-inner">
        <h6>Media table</h6>

        <div class="nav pull-right">
            <a href="#" class="dropdown-toggle navbar-icon" data-toggle="dropdown"><i class="icon-cog"></i></a>
            <ul class="dropdown-menu pull-right">
                <li><a href="#"><i class="icon-plus"></i>Add new option</a></li>
                <li><a href="#"><i class="icon-reorder"></i>View statement</a></li>
                <li><a href="#"><i class="icon-cogs"></i>Parameters</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="table-overflow">
<table class="table table-striped table-bordered table-checks media-table">
<thead>
<tr>
    <th>Image</th>
    <th>Description</th>
    <th>Date</th>
    <th>File info</th>
    <th class="actions-column">Actions</th>
</tr>
</thead>
<tbody>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face1.png" alt=""/></a></td>
    <td><a href="#" title="">Image2 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face2.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face3.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face4.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face5.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face6.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face7.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face8.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face1.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face2.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face3.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
<tr>
    <td><a href="/p/img/demo/big.jpg" title="" class="lightbox"><img src="/p/img/demo/users/face3.png" alt=""/></a></td>
    <td><a href="#" title="">Image1 description</a></td>
    <td>Feb 12, 2012. 12:28</td>
    <td class="file-info">
        <span><strong>Size:</strong> 215 Kb</span>
        <span><strong>Format:</strong> .jpg</span>
        <span><strong>Dimensions:</strong> 120 x 120</span>
    </td>
    <td>
        <ul class="navbar-icons">
            <li><a href="#" class="tip" title="Add new option"><i class="icon-plus"></i></a></li>
            <li><a href="#" class="tip" title="View statistics"><i class="icon-reorder"></i></a></li>
            <li><a href="#" class="tip" title="Parameters"><i class="icon-cogs"></i></a></li>
        </ul>
    </td>
</tr>
</tbody>
</table>
</div>
</div>