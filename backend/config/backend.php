<?php

defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'backend');

$localConfig = (file_exists(dirname(__FILE__) . '/.prod')) ? 'prod' : 'dev';

return CMap::mergeArray(
    require(dirname(__FILE__).'/../../common/config/main.php'),

    array(
        'basePath' => realPath(__DIR__ . '/..'),

        'defaultController' => 'dashboard/index',

        'aliases' => array(
            'bootstrap' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiistrap',
            'yiiwheels' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiiwheels'
        ),

        'import' => array(
            'backend.extensions.*.*',
            'backend.components.*',
            'backend.helpers.*',
            'backend.models.*',
            'backend.widgets.*',
        ),

        'behaviors' => array(),
        'controllerMap' => array(),
        'modules' => array(),

        'components' => array(

            'bootstrap' => array(
                'class' => 'bootstrap.components.TbApi',
            ),

            'yiiwheels' => array(
                'class' => 'yiiwheels.YiiWheels',
            ),

            'clientScript' => array(
                'scriptMap' => array(
                    'bootstrap.min.css' => false,
                    'bootstrap.min.js' => false,
                    'bootstrap-yii.css' => false,

                    'jquery.js' => '',
                    'bootstrap.js' => '',
                    'jquery.min.js' => '',
                    'notify.min.js' => '',
                    'notify.js' => '',
                    'jquery-ui.min.js' => '',
                    'bootstrap.min.css' => '',
                ),
            ),

            'urlManager' => array(
                // uncomment the following if you have enabled Apache's Rewrite module.
                'urlFormat' => 'path',
                'showScriptName' => false,
                'rules' => array(
                    // default rules
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ),
            ),

            'user' => array(
                'class' => 'WebUser',
                'allowAutoLogin' => true,
            ),

            'authManager' => array(
                // ����� ������������ ���� �������� �����������
                'class' => 'PhpAuthManager',
                // ���� �� ���������. ���, ��� �� ������, ���������� � ����� � �����.
                'defaultRoles' => array('guest'),
            ),

            'session' => array(
                'class' => 'CDbHttpSession',
                'connectionID' => 'db',
            ),

            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),

            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(

                    array(
                        'class'=>'CProfileLogRoute',
                        'enabled' => YII_DEBUG || (isset($_GET['_profile_']) && $_GET['_profile_'] == 14),
                    ),

                    array(
                        'class'=>'CWebLogRoute',
                        'categories' => '',
                        'enabled' => YII_DEBUG || (isset($_GET['_weblog_']) && $_GET['_weblog_'] == 14),
                        'levels'=>'error, warning, trace, info',
                        'filter' => array(
                            'class' => 'CLogFilter'
                        ),
                    ),

                ),
            ),
        )
    ),

    require(dirname(__FILE__).'/'. $localConfig .'.php'),
    require(dirname(__FILE__).'/local.php')
);