<?php

/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 12.01.2016
 * Time: 0:06
 */
class DlinkClient {

    protected $gambit;
    protected $device;


    public function __construct(Device $device)
    {
        $this->device = $device;

        if ( !$this->gambit = $this->initGambit() ) {
            throw new Exception("Error while trying to gambit");
        }
    }

    protected function initGambit()
    {
        $url = $this->device->ip . '/homepage.htm';
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "BrowsingPage=index_dlink.htm&Login={$this->device->login}&Password={$this->device->pass}");

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));

        $response = curl_exec($curl);
        $error_code = curl_errno($curl);
        curl_close($curl);

        if ( $error_code !== 0 )
        {
            throw new Exception(517, "Curl error #{$error_code}");
        }

        $matches = array();

        if ( preg_match("/Gambit=(.*)'/", $response, $matches) )
        {
            if ( count($matches) == 2 )
            {
                return $matches[1];
            }
        }

        return null;
    }

    public function getPoeInfo()
    {
        $curl = curl_init($this->device->ip . "/iss/specific/PoEPortSetting.js?Gambit=" . $this->gambit);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Referer: ' . 'http://' . $this->device->ip . '/iss/PoE_Port_Setting.htm?Gambit=' . $this->gambit
        ));

        $response = curl_exec($curl);
        $error_code = curl_errno($curl);
        curl_close($curl);

        $matches = array();
        if ( preg_match("/^var PoE_Port_Setting = \[(.*)\];$/Ums", $response, $matches) )
        {
            if ( count($matches) == 2 )
            {
                if ( $settings = eval( "return [{$matches[1]}];" ) )
                {
                    return $settings;
                }
            }
        }

        return null;
    }



    public function getDeviceInfo()
    {
        $curl = curl_init($this->device->ip . "/iss/specific/Device.js?Gambit=" . $this->gambit);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Referer: ' . 'http://' . $this->device->ip . '/iss/DeviceInfotable.htm?Gambit=' . $this->gambit
        ));

        $response = curl_exec($curl);
        $error_code = curl_errno($curl);
        curl_close($curl);

        $matches = array();
        if ( preg_match("/^var Switch_Status = \[(.*)\];$/Ums", $response, $matches) )
        {
            if ( count($matches) == 2 )
            {
                if ( $info = eval( "return [{$matches[1]}];" ) )
                {
                    $result = array(
                        'model' => $info[0],
                        'firmware' => $info[1],
                        'protocol' => $info[2],
                        'mac' => $info[3],
                        'uptime' => implode(' ', $info[4]),
                    );

                    if ( $poeInfo = $this->getDevicePoeInfo() )
                    {
                        $result = CMap::mergeArray($result, $poeInfo);
                    }

                    return $result;
                }
            }
        }

        return null;
    }



    public function getDevicePoeInfo()
    {
        $curl = curl_init($this->device->ip . "/iss/specific/PoESysSetting.js?Gambit=" . $this->gambit);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Referer: ' . 'http://' . $this->device->ip . '/iss/PoE_Sys_Setting.htm?Gambit=' . $this->gambit
        ));

        $response = curl_exec($curl);
        $error_code = curl_errno($curl);
        curl_close($curl);

        $matches = array();
        if ( preg_match("/^var PoeSys_Status = \[(.*)\];$/Ums", $response, $matches) )
        {
            if ( count($matches) == 2 )
            {
                if ( $info = eval( "return [{$matches[1]}];" ) )
                {
                    return array(
                        'poe.total' => $info[0],
                        'poe.used' => $info[1],
                        'poe.left' => $info[2],
                        'poe.percent' => $info[3],
                    );
                }
            }
        }

        return null;
    }


    public function togglePort($port, $on = true)
    {
        if( $port > 0 ) $port--;

        if ( $port < 10 ) $port = '0' . $port;

        $on = (int) ((bool)$on);

        $url = $this->device->ip . '/iss/specific/PoEPortSetting.js';
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "FormName=portset"
            . "&Gambit={$this->gambit}"
            . "&PDDetect=2"
            . "&PoE_Enable={$on}"
            . ( $on ? "&PowerLimit=1" : "" )
            . "&Priority=2"
            . "&TimeRangeID=0"
            . "&port_f={$port}"
            . "&port_t={$port}"
            . "&post_url=cgi_port."
        );

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Referer: ' . 'http://' . $this->device->ip . '/iss/PoE_Port_Setting.htm?Gambit=' . $this->gambit
        ));

        $response = curl_exec($curl);
        $error_code = curl_errno($curl);
        curl_close($curl);

        if ( $error_code !== 0 )
        {
            throw new Exception(517, "Curl error #{$error_code}");
        }

        return ( strlen($response) == 244 ); // if all ok, Dlink initiate redirect to admin page
    }

    public static function getPortsRawInfo(Array $allParams) {

        $params = array(
            4 => 'power',
            5 => 'voltage',
            6 => 'current',
            8 => 'status',
            0 => 'device',
        );

        $ports = array();

        for ( $i = 1; $i < 25; $i++ )
        {
            $ports[$i]['n'] = $i;
            $ports[$i]['id'] = $i;
            // $ports[$i]['device'] = 0;
            foreach ( $params as $param )
            {
                if(!empty($allParams["ports.{$i}.{$param}"])) {
                    $v = $allParams["ports.{$i}.{$param}"];
                    $ports[$i][$param] = $v->value;
                } elseif($param=='device') {
                    $ports[$i]['device'] = 0;
                }
            }
        }

        return $ports;
    }
}