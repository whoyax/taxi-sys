<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 01.09.15
 * Time: 15:05
 */

class PnHtml extends TbHtml {

    public static function menu(array $items, $htmlOptions = array(), $depth = 0)
    {
        // todo: consider making this method protected.
        if (!empty($items)) {
            $htmlOptions['role'] = 'menu';
            $output = self::openTag('ul', $htmlOptions);
            foreach ($items as $itemOptions) {
                if (is_string($itemOptions)) {
                    $output .= $itemOptions;
                } else {
                    if (isset($itemOptions['visible']) && $itemOptions['visible'] === false) {
                        continue;
                    }
                    // todo: consider removing the support for htmlOptions.
                    $options = TbArray::popValue('htmlOptions', $itemOptions, array());
                    if (!empty($options)) {
                        $itemOptions = TbArray::merge($options, $itemOptions);
                    }
                    $label = TbArray::popValue('label', $itemOptions, '');
                    if (TbArray::popValue('active', $itemOptions, false)) {
                        self::addCssClass('active', $itemOptions);
                    }
                    if (TbArray::popValue('disabled', $itemOptions, false)) {
                        self::addCssClass('disabled', $itemOptions);
                    }
                    if (!isset($itemOptions['linkOptions'])) {
                        $itemOptions['linkOptions'] = array();
                    }
                    $icon = TbArray::popValue('icon', $itemOptions);
                    if (!empty($icon)) {
                        $label = self::icon($icon) . ' ' . $label;
                    }
                    $items = TbArray::popValue('items', $itemOptions, array());
                    $url = TbArray::popValue('url', $itemOptions, false);
                    if (empty($items)) {
                        if (!$url) {
                            $output .= self::menuHeader($label);
                        } else {
                            $itemOptions['linkOptions']['tabindex'] = -1;
                            $output .= self::menuLink($label, $url, $itemOptions);
                        }
                    } else {
                        $output .= self::menuDropdown($label, $url, $items, $itemOptions, $depth);
                    }
                }
            }
            $output .= '</ul>';
            return $output;
        } else {
            return '';
        }
    }

    protected static function menuDropdown($label, $url, $items, $htmlOptions, $depth = 0)
    {
        // self::addCssClass('expand', $htmlOptions);
        TbArray::defaultValue('role', 'menuitem', $htmlOptions);
        $linkOptions = TbArray::popValue('linkOptions', $htmlOptions, array());
        $menuOptions = TbArray::popValue('menuOptions', $htmlOptions, array());
        //self::addCssClass('dropdown-menu', $menuOptions);
        //$menuOptions['role'] = 'menu';
        if ($depth === 0) {
            $defaultId = parent::ID_PREFIX . parent::$count++;
            TbArray::defaultValue('id', $defaultId, $menuOptions);
            $menuOptions['aria-labelledby'] = $menuOptions['id'];
            $menuOptions['role'] = 'menu';

        }
        $output = self::openTag('li', $htmlOptions);
        $output .= self::dropdownToggleMenuLink($label, $url, $linkOptions, $depth);
        $output .= self::menu($items, $menuOptions, $depth + 1);
        $output .= '</li>';
        return $output;
    }

    public static function dropdownToggleMenuLink($label, $url = '#', $htmlOptions = array(), $depth = 0)
    {
        self::addCssClass('expand', $htmlOptions);
        if ($depth === 0) {
            $label .= ' <b class="caret"></b>';
        }
        // $htmlOptions['data-toggle'] = 'dropdown';
        return self::link($label, $url, $htmlOptions);
    }

    public static function buttonGroupRadioField($model, $attribute, $data, $htmlOptions = array()) {

        parent::resolveNameID($model, $attribute, $htmlOptions);

        $value = $model->{$attribute};
        $name = $htmlOptions['id'];

        $buttons = array();

        foreach($data as $v => $label) {
            $buttons[] = array(
                'label' => $label,
                'class' => ($v==$value) ? 'active' : '',
                'data-value' => $v
            );
        }

        echo TbHtml::buttonGroup(
            $buttons,
            array('toggle' => TbHtml::BUTTON_TOGGLE_RADIO, 'color' => TbHtml::BUTTON_COLOR_DEFAULT, 'id' => $name . '_container')
        );

        echo TbHtml::hiddenField(get_class($model) . "[{$attribute}]", $value);

        $script = "
            $('#{$name}_container a.btn').click(function(e){
                $('#{$name}').val($(this).attr('data-value'));
                e.preventDefault();
                //return false;
            });
        ";

        Yii::app()->clientScript->registerScript($name . '_script', $script);
    }

}