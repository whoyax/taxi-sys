<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 06.10.15
 * Time: 14:52
 */

class FormatHelper {

    public static function secToHumanTranslate($time)
    {
        $units = array(
            // "month"   => 7*24*3600,
            30*24*3600 => "{n} месяц|{n} месяца|{n} месяцев|{n} месяца",
            7*24*3600 => "{n} неделя|{n} недели|{n} недель|{n} недели",
            24*3600 => "{n} день|{n} дня|{n} дней|{n} дня",
            3600 => "{n} час|{n} часа|{n} часов|{n} часа",
            60=> "{n} минута|{n} минуты|{n} минут|{n} минуты",
            1 => "{n} секунда|{n} секунды|{n} секунд|{n} секунды"
        );
        // specifically handle zero
        if ( $time == 0 ) return "0 секунд";
        $s = "";

        foreach ( $units as $divisor => $translate )
        {
            if ( $quot = intval($time / $divisor) ) {
                $s .= Yii::t('app', $translate, $quot) . ", ";
                // $s .= "$quot $name";
                // $s .= (abs($quot) > 1 ? "s" : "") . ", ";
                $time -= $quot * $divisor;
            }
        }
        return substr($s, 0, -2);
    }

} 