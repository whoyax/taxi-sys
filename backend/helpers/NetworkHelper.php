<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 17.09.15
 * Time: 2:21
 */

use PEAR2\Net\RouterOS;

class NetworkHelper {

    static $adr = null;
    static $list = null;
    static $ids = null;


    public static function requestLists() {
        $mikrotik = Yii::app()->mikrotik;
        $request = new RouterOS\Request('/ip/firewall/address-list/print');

        $results = $mikrotik->sendSync($request);

        foreach ( $results as $result ) {
            if ($result->getType() == RouterOS\Response::TYPE_DATA)
            {
                static::$adr[$result->getProperty('address')] = $result->getProperty('list');
                static::$ids[$result->getProperty('address')] = $result->getProperty('.id');
                static::$list[$result->getProperty('list')][] = $result->getProperty('address');
            }
        }

        return true;
    }


    public static function getAddressesWithList() {
        if(static::$adr===null) {
            static::requestLists();
        }

        return static::$adr;
    }

    public static function getAddressesByList($list) {
        if(static::$list===null) {
            static::requestLists();
        }

        if(!empty(static::$list[$list]))
            return static::$list[$list];

        return array();
    }

    public static function getIdByAddress($adr) {
        if(static::$ids===null) {
            static::requestLists();
        }

        if(!empty(static::$ids[$adr]))
            return static::$ids[$adr];


        $request = new RouterOS\Request('/ip/firewall/address-list/print', RouterOS\Query::where('address', $adr));
        $results = Yii::app()->mikrotik->sendSync($request);
        foreach ( $results as $result ) {
            if ($result->getType() == RouterOS\Response::TYPE_DATA)
            {
                static::$ids[$result->getProperty('address')] = $result->getProperty('.id');
                return $result->getProperty('.id');
            }
        }

        return null;
    }


    public static function getActualListByAddress($adr) {
        if($adrs = static::getAddressesWithList()) {
            if(!empty($adrs[$adr]))
                return $adrs[$adr];
        }

        return null;
    }


    public static function manyToList($list, $addresses)
    {
        $result = true;
        foreach($addresses as $adr) {
            $result = $result && static::addToList($list, $adr);
        }

        return $result;
    }


    public static function addToList($list, $adr)
    {
        if($actual = static::getActualListByAddress($adr))
        {
            if($actual != $list) {
                static::removeFromList($adr);
            } else {
                return true;
            }
        }

        $addRequest = new RouterOS\Request('/ip/firewall/address-list/add');
        $addRequest->setArgument('address', $adr);
        $addRequest->setArgument('list', $list);

        $results = Yii::app()->mikrotik->sendSync($addRequest);
        foreach ( $results as $result )
        {
            if ($result->getType() == RouterOS\Response::TYPE_FINAL)
            {
                if($id = $result->getProperty('ret')) {
                    static::$ids[$adr] = $id;
                    static::$adr[$adr] = $list;
                    static::$list[$list][] = $adr;
                    return true;
                }
            }
        }

        print "\r\nerror on add adr to list:  '{$adr}' to '{$list}'\r\n";
        return false;
    }

    public static function removeFromList($adr)
    {
        if($id = static::getIdByAddress($adr)) {
            $removeRequest = new RouterOS\Request('/ip/firewall/address-list/remove');
            $removeRequest->setArgument('numbers', $id);
            Yii::app()->mikrotik->sendSync($removeRequest);

        }

        static::cleanId($adr);
        static::cleanLists($adr);
        static::cleanAdr($adr);

    }

    public static function removeConnectionsForProvider($providerMark)
    {
        $mikrotik = Yii::app()->mikrotik;

        $mark = $providerMark . '-connection';

        $interfaceListRequest = new RouterOS\Request('/ip firewall connection print');
        $results = $mikrotik->sendSync($interfaceListRequest);
        $i=0;
        foreach($results as $result) {
            if ($result->getType() == RouterOS\Response::TYPE_DATA) {
                if($result('connection-mark')==$mark)
                {
                    $id = $result('.id');
                    $removeRequest = new RouterOS\Request('/ip/firewall/connection/remove');
                    $removeRequest->setArgument('numbers', $id);
                    $result = Yii::app()->mikrotik->sendSync($removeRequest);

                }
            }
        }
    }
    

    public static function cleanId($adr) {
        if(!empty(static::$ids[$adr])) {
            unset(static::$ids[$adr]);
            return true;
        }

        return false;
    }

    public static function cleanAdr($adr) {
        if(!empty(static::$adr[$adr])) {
            unset(static::$adr[$adr]);
            return true;
        }

        return false;
    }

    public static function cleanLists($adr) {
        foreach(static::$list as $name => $list) {
            foreach($list as $k => $adrs) {
                if($adrs == $adr) {
                    unset(static::$list[$name][$k]);
                    return true;
                }
            }
        }
        return false;
    }


    public static function getStatsRx($interface){
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT rx/(1024) FROM interface_stat WHERE interface = '{$interface}' ORDER BY time DESC LIMIT 20");
        return $command->queryColumn();
    }

    public static function getStatsTx($interface){
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT tx/(1024) FROM interface_stat WHERE interface = '{$interface}' ORDER BY time DESC LIMIT 20");
        return $command->queryColumn();
    }


    public static function providerWorkerRestart() {
        $worker = Worker::model()->findByPk(1);
        $worker->restart = 1;
        return $worker->save();
    }
}