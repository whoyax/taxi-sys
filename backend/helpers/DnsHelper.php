<?php

/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 23.01.2016
 * Time: 18:23
 *
 *
 *
 * var GET_HOST_URL = 'http://dyna.dnsever.com/gethost.php';
 *
 <dnsever>
    <result type="gethost" code="700" numOfHosts="9" msg="Login Success" lang="en">
        <host name="plustaxi.ru" status="ONLINE" ip="213.168.51.62"/>
        <host name="internet1.plustaxi.ru" status="ONLINE" ip="213.168.51.62"/>
    </result>
</dnsever>
 *
 *
 *
 *
 * var GET_IP_URL = 'http://dyna.dnsever.com/getip.php';
 * var UPDATE_URL = "http://dyna.dnsever.com/update.php";
 *
 */
class DnsHelper {

    static $hosts = array();

    public static function changeDns(DnsSettings $settings)
    {
        // $url = 'http://taxiplus:cembrn4567@dyna.dnsever.com/update.php?host[test.plustaxi.ru]='.$ip;

        $answer = self::http_auth_get(
            'dyna.dnsever.com',
            '/update.php?host[' . $settings->domain_name . ']=' . $settings->set_ip,
            $settings->username,
            $settings->authcode
        );

        if(!$answer)
        {
            throw new DnsException("Нет ответа от сервиса DNSEver.");
        }

        $matches = array();
        if (!preg_match("#<dnsever>(.*)</dnsever>#sei", $answer, $matches))
        {
            throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml)");
        }

        $xml = false;

        try
        {
            $xml = new SimpleXMLElement($matches[0]);
        }
        catch(Exception $e) {}

        if(!$xml)
        {
            throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml)");
        }

        $result = $xml->result;

        if(!$result)
        {
            throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml-result)");
        }

        $host = $result->host;

        if( !$host )
        {
            throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml-host)");
        }

        if( $result["numOfSuccess"] == 1
            && $result["numOfFailure"] == 0
            && $result["msg"] == "DDNS Update Success"
            && $host["msg"] == "Update Success")

        {
            $settings->current->ip = $settings->set_ip;
            Event::storeWatchdogMessage('Переключение DNS ' . $settings->domain_name, 'Ip changed to ' . $settings->set_ip, Event::TYPE_INTERNET);
            return $settings->current->save();
        }
        elseif ( $host["msg"] == "Already Updated" ) {
            $settings->current->ip = $settings->set_ip;
            Event::storeWatchdogMessage('Повторное переключение DNS ' . $settings->domain_name, 'Ip changed to ' . $settings->set_ip, Event::TYPE_INTERNET);
            return $settings->current->save();
        }
        else
        {
            $message = "Dns change error: type: {$result['type']}, code: {$result['code']}, msg: {$result['msg']}, host[ code: {$host['code']}, msg: {$host['msg']}]";
            Event::storeWatchdogMessage('Dns ' . $settings->domain_name . 'change error ', $message);
            throw new DnsException($message);
        }
    }

    public static function markToChangeIp(DnsSettings $dns, $ip)
    {
        $dns->set_ip = $ip;
        return $dns->save(false);
    }

    public static function getHostInfo(DnsSettings $settings)
    {
        $all = static::loadHostInfo($settings);
        if(!empty($all[$settings->domain_name])) {
            return $all[$settings->domain_name];
        }

        return false;
    }

    public static function getCurrentHostIp(DnsSettings $dns)
    {
        if( $info = static::getHostInfo($dns)) {
            return $info['ip'];
        }

        return false;
    }


    public static function loadHostInfo(DnsSettings $settings, $force = false)
    {
        if(!static::$hosts || $force)
        {
            // $url = 'http://taxiplus:cembrn4567@dyna.dnsever.com/update.php?host[test.plustaxi.ru]='.$ip;
            $answer = self::http_auth_get(
                'dyna.dnsever.com',
                '/gethost.php',
                $settings->username,
                $settings->authcode
            );

            if ( !$answer )
            {
                throw new DnsException("Нет ответа от сервиса DNSEver.");
            }

            $matches = array();
            if ( !preg_match("#<dnsever>(.*)</dnsever>#sei", $answer, $matches) )
            {
                throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml)");
            }

            $xml = false;

            try
            {
                $xml = new SimpleXMLElement($matches[0]);
            }
            catch ( Exception $e )
            {
            }

            if ( !$xml )
            {
                throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml)");
            }

            $result = $xml->result;

            if ( !$result )
            {
                throw new DnsException("Невозможно разобрать ответ от сервиса DNSEver. (xml-result)");
            }

            static::$hosts = array();

            foreach ( $result->host as $host )
            {
                $name = (string)$host['name'];
                static::$hosts[$name] = array(
                    'name' => (string)$host['name'],
                    'ip' => (string)$host['ip'],
                    'status' => (string)$host['status'],
                );
            }
        }

        return static::$hosts;
    }

    public static function http_auth_get($host, $path, $login, $password)
    {
        $out = false;
        try
        {
            $header = 'GET '.$path.' HTTP/1.1'."\r\n".
                'Host: '.$host."\r\n".
                'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2 (.NET CLR 3.5.30729)'."\r\n".
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'."\r\n".
                'Accept-Language: ru,en-us;q=0.7,en;q=0.3'."\r\n".
                'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7'."\r\n".
                'Keep-Alive: 115'."\r\n".
                'Connection: keep-alive'."\r\n";
            $header .= 'Authorization: Basic '.base64_encode($login.':'.$password)."\r\n";
            $header .= 'Cache-Control: max-age=0'."\r\n"."\r\n";
            $f = fsockopen($host, 80);
            fputs($f, $header);
            $out = '';
            while(!feof($f))
            {
                $out .= fread($f, 128);
            }
        } catch (Exception $e)
        {
            $out = false;
        }

        return $out;
    }

}

class DnsException extends CException {}