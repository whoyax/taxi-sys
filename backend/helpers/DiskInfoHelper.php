<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 04.10.15
 * Time: 5:37
 */

class DiskInfoHelper {

    public static function getPercent($all, $free) {
        $used = $all - $free;
        return ($all!=0) ? round(($used/$all)*100, 0) : 0;
    }

    public static function getProgressClass($percent) {
        $class = '';
        switch(true) {
            case $percent>=90:
                $class ='danger';
                break;
            case ($percent < 90 && $percent > 75):
                $class = 'warning';
                break;
            case ($percent <= 75 && $percent > 50):
                $class = 'info';
                break;
            case ($percent <= 50 && $percent > 0):
                $class = 'success';
                break;
            default:
                $class = 'success';
        }

        return $class;
    }


} 