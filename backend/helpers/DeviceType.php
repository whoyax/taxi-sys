<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 22.09.15
 * Time: 5:40
 */

class DeviceType {

    const UNKNOWN = 0;
    const SIMPLE = 1;
    const WORKSTATION = 2;
    const SERVER_WIN = 3;
    const SERVER_LINUX = 4;
    const VIRTUAL = 5;
    const MIKROTIK = 6;
    const UPS = 7;
    const CAM = 8;
    const VOIP = 9;
    const HYPERVISOR = 10;

    static $textPresent = array(
        self::UNKNOWN => 'Неопределено',
        self::SIMPLE => 'Простое',
        self::SERVER_WIN => 'Сервер Windows',
        self::SERVER_LINUX => 'Сервер Linux',
        self::WORKSTATION => 'Рабочая станция',
        self::VIRTUAL => 'Виртуальный сервер',
        self::MIKROTIK => 'Микротик',
        self::HYPERVISOR => 'Гипервизор',
        self::CAM => 'Камера',
        self::VOIP => 'Voip шлюз',
        self::UPS => 'Источник бесперебойного питания',
    );

    public static function getOptions() {
        return self::$textPresent;
    }

} 