<?php

/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 09.09.15
 * Time: 18:01
 */
class NetworkStatus {

    const UNKNOWN = 0;
    const LOST = 1;
    const POOR = 2;
    const GOOD = 3;
    const FINE = 4;
    const BEST = 5;

    const MODE_UNKNOWN = 0;
    const MODE_AUTO = 1;
    const MODE_MANUAL = 2;

    public static $stateStrings = array(
        self::UNKNOWN => 'Неизвестно',
        self::LOST => 'Нет связи',
        self::POOR => 'Едва жив',
        self::GOOD => 'Нестабильно',
        self::FINE => 'Отлично',
        self::BEST => 'Идеально',
    );

    private static $statusModeStrings = array(
        self::MODE_UNKNOWN => 'Неизвестно',
        self::MODE_AUTO => 'Автоматически',
        self::MODE_MANUAL => 'Вручную',
    );

    public static $stateColors = array(
        self::UNKNOWN => '',
        self::LOST => 'important',
        self::POOR => 'important',
        self::GOOD => 'warning',
        self::FINE => 'success',
        self::BEST => 'success',
    );

    public static $stateClasses = array(
        self::UNKNOWN => 'info',
        self::LOST => 'important',
        self::POOR => 'important',
        self::GOOD => 'warning',
        self::FINE => 'success',
        self::BEST => 'success',
    );

    /**
     * @param int $status
     *
     * @return string
     */
    public static function getStatusText($status)
    {
        $state = 'Внутренняя ошибка';
        if ( isset( static::$stateStrings[$status] ) )
        {
            $state = static::$stateStrings[$status];
        }

        return $state;
    }

    /**
     * @param int $status
     *
     * @return string
     */
    public static function getStatusColor($status)
    {
        $color = 'inverse';
        if ( isset( static::$stateColors[$status] ) )
        {
            $color = static::$stateColors[$status];
        }

        return $color;
    }

    /**
     * @param int $status
     *
     * @return string
     */
    public static function getStatusClass($status)
    {
        $class = '';
        if ( isset( static::$stateClasses[$status] ) )
        {
            $class = static::$stateClasses[$status];
        }

        return $class;
    }

    /**
     * @param int $status
     * @param int $mode
     *
     * @return string
     */
    public static function getStatusLabel($status, $mode = self::MODE_AUTO)
    {
        $t = TbHtml::tag(
            'span',
            array('class' => 'label label-' . static::getStatusClass($status)),
            static::getStatusText($status)
        );

        if ( $mode == static::MODE_MANUAL )
        {
            $t .= ' ' . TbHtml::tag(
                    'span',
                    array('class' => 'label label-inverse'),
                    'Вручную'
                );
        }

        return $t;
    }

    public static function getStatusByLoss($loss)
    {
        switch ( true )
        {
            case $loss >= 95:
                $status = static::LOST;
                break;
            case ( $loss < 95 && $loss > 15 ):
                $status = static::POOR;
                break;
            case ( $loss <= 15 && $loss > 5 ):
                $status = static::GOOD;
                break;
            case ( $loss <= 5 && $loss > 0 ):
                $status = static::FINE;
                break;
            case $loss == 0:
                $status = static::BEST;
                break;
            default:
                $status = static::UNKNOWN;
        }

        return $status;
    }

    public static function getOptionList()
    {
        return static::$stateStrings;
    }

    public static function getStatusModeOptions()
    {
        return static::$statusModeStrings;
    }

    /**
     * @param Provider $provider
     *
     * @return bool
     */
    public static function updateProviderStatus(Provider $provider)
    {
        $status = static::getStatusByLoss((int)$provider->getLoss());

        if ( $provider->status_mode != NetworkStatus::MODE_MANUAL && $provider->getStatus() != $status )
        {
            if ( $provider->setStatus($status) )
            {
                $message = "Сетевой статус провайдера {$provider->name} изменен на '{$provider->getStatusText()}'";
                Event::storeWatchdogMessage($message, $message, Event::TYPE_INTERNET);

                return true;
            }
            else
            {
                $message = "Ошибка при смене сетевого статуса провайдера {$provider->name} на '{$provider->getStatusText()}'";
                Event::storeWatchdogMessage($message, $message, Event::TYPE_PROGRAM_ERROR);
            }
        }

        return false;
    }

} 