<?php
use PEAR2\Net\RouterOS;

class DashboardController extends Controller {

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $params = array(
            'provider' => new Provider('search'),
            'queue' => new Queue('search'),
            'dns' => new DnsSettings('search'),

        );

        if(isset($_GET['ajax'])
            && $_GET['ajax'] == 'queue-grid'
        ) {
            return $this->renderPartial('_queues', $params);
        }

        if ( isset( $_GET['ajax'] )
            && $_GET['ajax'] == 'dns-grid'
        )
        {
            return $this->renderPartial('_dns', $params);
        }

        $this->render('index', $params);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Device('search');
        $model->unsetAttributes(); // clear any default values
        if ( isset($_GET['Device']) ) {
            $model->attributes = $_GET['Device'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }
}