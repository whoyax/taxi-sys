<?php

class ProviderController extends Controller
{
	public $modelName='Provider';

    public function accessRules()
    {
        return CMap::mergeArray(
            array(
                array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('getPing','test'),
                    'users'=>array('@'),
                ),
                array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('getPing','test', 'sort'),
                    'roles'=>array('admin'),
                ),
            ),
            parent::accessRules()
        );
    }

    public function actionGetPing() {
        echo CJSON::encode(Provider::getIdsWithPing());
        Yii::app()->end();
    }
}