<?php

class VideoController extends Controller {

    public $modelName = 'Device';

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','toggleOff', 'toggleOn'),
                'roles'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionAdmin()
    {
        $dlink = Device::model()->findByPk(58);

        $all = $dlink->params;
        $ports = DlinkClient::getPortsRawInfo($all);

        $this->render('admin', array(
            'ports' => $ports,
            'params' => $all,
            'dlink' => $dlink
        ));
    }

    public function actionToggleOn($port)
    {
        if ( Yii::app()->request->isPostRequest )
        {

            if ( $device = Device::model()->findByPk(58) )
            {
                try
                {
                    $client = new DlinkClient($device);
                    if ( $client->togglePort($port, true) )
                    {
                        DeviceParam::updateInfoByModel($device, "ports.{$port}.status", "Попытка включения...");
                    }
                }
                catch ( Exception $e )
                {
                    DeviceParam::updateInfoByModel($device, "ports.{$port}.status", get_class($e) . ': ' . $e->getMessage());
                }
            };

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if ( !isset( $_GET['ajax'] ) )
            {
                $this->redirect(isset( $_POST['returnUrl'] ) ? $_POST['returnUrl'] : array('admin'));
            }
        }
        else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionToggleOff($port)
    {
        if ( Yii::app()->request->isPostRequest )
        {

            if ( $device = Device::model()->findByPk(58) )
            {
                try
                {
                    $client = new DlinkClient($device);
                    if ( $client->togglePort($port, false) )
                    {
                        DeviceParam::updateInfoByModel($device, "ports.{$port}.status", "Выключение...");
                    }
                }
                catch ( Exception $e )
                {
                    DeviceParam::updateInfoByModel($device, "ports.{$port}.status", get_class($e) . ': ' . $e->getMessage());
                }
            };

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if ( !isset( $_GET['ajax'] ) )
            {
                $this->redirect(isset( $_POST['returnUrl'] ) ? $_POST['returnUrl'] : array('admin'));
            }
        }
        else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }
}