<?php

class AntivirusController extends Controller {
    public $modelName = 'Device';

    public function actionAdmin()
    {
        $clients = new AntivirusClient('search');
        $clients->unsetAttributes();

        $quarantine = new AntivirusClientQuarantine('search');
        $quarantine->unsetAttributes();

        $threatLog = new AntivirusThreatLog('search');
        $threatLog->unsetAttributes();

        $scanLog = new AntivirusScanLog('search');
        $scanLog ->unsetAttributes();

        $this->render('admin',
            array(
                'clients' => $clients,
                'quarantine' => $quarantine,
                'threatLog' => $threatLog,
                'scanLog' => $scanLog
            )
        );
    }
}