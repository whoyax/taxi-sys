<?php

class TelephonyController extends Controller {
    public $modelName = 'Device';

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','restart'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionAdmin()
    {
        $this->render('admin');
    }

    public function actionRestart($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
           if ($device = $this->loadModel($id)) {
               try {
                   $telnet = new TelnetClient($device->ip, 23, 10, "#");
                   $telnet->connect();
                   $telnet->login($device->login, $device->pass);
                   $telnet->execute('enable');
                   $telnet->execute('reboot', ' [confirm]');
                   $telnet->execute('', '#');
                   $telnet->disconnect();
                   DeviceParam::updateInfoByModel($device, "gsm.sim-card", "Reboot...");
               }
               catch(Exception $e)
               {
                   DeviceParam::updateInfoByModel($device, "gsm.sim-card", $e->getMessage());
               }
           };

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        }
        else {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }
}