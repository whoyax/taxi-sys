<?php

class DeviceController extends Controller
{
	public $modelName='Device';

    public function accessRules()
    {
        return CMap::mergeArray(
            array(
                array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>
                        array(
                            'getHStat',
                            'getVmStats',
                            'getWorkstationStats',
                            'getUpsStats',
                            'getMicroStats'
                        ),
                    'users'=>array('@'),
                ),
                array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>
                        array(
                            'addAccess',
                            'deleteAccess',
                            'addIp',
                            'addDependent',
                            'deleteDependent',
                            'addParam',
                            'deleteIp',
                        ),
                    'roles'=>array('admin'),
                ),
            ),
            parent::accessRules()
        );
    }

    protected function getAfterCreateRedirect($model) {
        return array('update','id'=>$model->id);
    }

    protected function getAfterUpdateRedirect($model) {
        return array('update','id'=>$model->id);
    }

    public function actionAddAccess($id)
    {
        $model = new DeviceAccess();
        $model->device_id = $id;

        $this->performAjaxValidation($model);

        if (isset($_POST['DeviceAccess'])) {
            $model->attributes=$_POST['DeviceAccess'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    public function actionDeleteAccess($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id, 'DeviceAccess')->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionAddIp($id)
    {
        $model = new DeviceIp('insert');
        $model->device_id = $id;

        $this->performAjaxValidation($model);

        if (isset($_POST['DeviceIp'])) {
            $model->attributes=$_POST['DeviceIp'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    public function actionAddDependent($id)
    {
        $model = new DeviceDependent('insert');
        $model->device_id = $id;

        $this->performAjaxValidation($model);

        if (isset($_POST['DeviceDependent'])) {
            $model->attributes=$_POST['DeviceDependent'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    public function actionAddParam($id)
    {
        $model = new DeviceParam('insert');
        $model->device_id = $id;

        $this->performAjaxValidation($model);

        if (isset($_POST['DeviceParam'])) {
            $model->attributes=$_POST['DeviceParam'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    public function actionDeleteIp($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id, 'DeviceIp')->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionDeleteDependent($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id, 'DeviceDependent')->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionGetHStat($id)
    {
        echo CJSON::encode(Device::getHStat($id));
        Yii::app()->end();
    }

    public function actionGetVmStats() {

        $stats = array();

        foreach(Device::model()->findAll("type=:type", array(':type' => DeviceType::VIRTUAL)) as $vm)
        {
             $stats[] = array(
                 'id' => $vm->id,
                 'cpu' => $vm->getVmCpuStat(),
                 'mem' => $vm->getVmMemStat(),
             );
        }

        echo CJSON::encode($stats);
        Yii::app()->end();
    }

    public function actionGetMicroStats() {
        $stats = array();
        foreach(Device::model()->findAll("type=:type", array(':type' => DeviceType::MIKROTIK)) as $m)
        {
            $stats[] = array(
                'id' => (int)$m->id,
                'cpu' => $m->getMikroCpuStat(),
                'mem' => $m->getMikroMemStat(),
                'current' => $m->getMikroCpuCurrent(),
                'ports' => $m->getMikroPortLoad()
            );
        }

        echo CJSON::encode($stats);
        Yii::app()->end();
    }

    public function actionGetWorkstationStats() {

        $stats = array();

        foreach(Device::model()->findAll("type=:type", array(':type' => DeviceType::WORKSTATION)) as $vm)
        {
            $stats[] = array(
                'id' => $vm->id,
                'cpu' => $vm->getVmCpuStat(),
                'mem' => $vm->getVmMemStat(),
            );
        }

        echo CJSON::encode($stats);
        Yii::app()->end();
    }


    public function actionGetUpsStats() {

        $stats = array();

        foreach(Device::model()->findAll("type=:type", array(':type' => DeviceType::UPS)) as $vm)
        {
            $stats[] = array(
                'id' => $vm->id,
                'load' => $vm->getUpsLoadStat(),
            );
        }

        echo CJSON::encode($stats);
        Yii::app()->end();
    }
}