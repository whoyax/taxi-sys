<?php

class QueueController extends Controller
{
	public $modelName='Queue';

    public function accessRules()
    {
        return CMap::mergeArray(
            array(
                array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('getList'),
                    'users'=>array('@'),
                ),
                array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('addProvider', 'sort'),
                    'roles'=>array('admin'),
                ),
            ),
            parent::accessRules()
        );
    }

    protected function getAfterCreateRedirect($model) {
        return array('update','id'=>$model->id);
    }

    protected function getAfterUpdateRedirect($model) {
        return array('update','id'=>$model->id);
    }

    public function actionGetList($id)
    {
        $data = TbHtml::listData(
            Provider::model()->findAll("id NOT IN(SELECT provider_id FROM queue_provider WHERE queue_id = {$id})"),
            'id', 'name');

        foreach( $data as $v => $name)
        {
            echo CHtml::tag('option', array('value' => $v), CHtml::encode($name), true);
        }
    }

    public function actionAddProvider($id)
    {
        $provider = new QueueProvider('insert');
        $provider->queue_id = $id;
        $provider->provider_id = (int)$_POST['provider'];

        if($provider->save()) {
            $this->afterSave($provider);
        } else {
            var_dump($provider->getErrors());
        }

        return Yii::app()->end();
    }

    public function afterSave($model) {
        NetworkHelper::providerWorkerRestart();
    }


    public function actionSort()
    {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 1;
            foreach ($_POST['items'] as $item) {
                $provider = QueueProvider::model()->findByPk($item);
                $provider->position = $i;
                if(!$provider->save()) {
                    var_dump($provider->getErrors());
                }
                $i++;
            }
            $this->afterSave($provider);
        }

        return Yii::app()->end();
    }
}