<?php

class CommutationController extends Controller {
    public $modelName = 'Device';

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'setPortDevice', 'addCable', 'deleteCable'),
                'roles'=>array('admin'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'roles'=>array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'roles'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionAddCable()
    {
        $model = new Cable('insert');

        $this->modelName = 'Cable';

        $this->performAjaxValidation($model);

        if (isset($_POST['Cable'])) {
            $model->attributes=$_POST['Cable'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    public function actionDeleteCable($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id, 'Cable')->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionAdmin()
    {
        $dlink = Device::model()->findByPk(58);
        $videoPorts = DlinkClient::getPortsRawInfo($dlink->params);

        $router = Device::model()->findByPk(1);
        $mainPorts = Mikrotik::getPortsRawInfo($router->params, $router->params['portcount']->value);

        $switch = Device::model()->findByPk(2);
        $switchPorts = Mikrotik::getPortsRawInfo($switch->params, $switch->params['portcount']->value);

        $params = array(
            'videoPorts' => $videoPorts,
            'mainPorts' => $mainPorts,
            'switchPorts' => $switchPorts
        );

        $this->render('admin', $params);
    }


    public function actionSetPortDevice() {
        $deviceId = Yii::app()->request->getParam('value');
        $port = Yii::app()->request->getParam('pk');
        $main = Yii::app()->request->getParam('main');

        DeviceParam::updateInfo($main, "ports.{$port}.device", $deviceId);

    }
}