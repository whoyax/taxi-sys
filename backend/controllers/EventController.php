<?php

class EventController extends Controller {
    public $modelName = 'Event';

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'addEvent', 'addComment', 'admin'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'roles'=>array('admin'),

            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('delete'),
                'roles'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionAddEvent()
    {
        $model = new Event('insert');

        $model->user_id = Yii::app()->user->id;

        $this->performAjaxValidation($model);

        if (isset($_POST['Event'])) {
            $model->attributes=$_POST['Event'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

    public function actionAddComment($id)
    {
        $model = new EventComment('insert');

        $model->user_id = Yii::app()->user->id;
        $model->event_id = $id;

        $this->modelName = 'EventComment';

        $this->performAjaxValidation($model);

        if (isset($_POST['EventComment'])) {
            $model->attributes=$_POST['EventComment'];
            if (!$model->save()) {
                var_dump($model->getErrors());
                // $this->redirect(array('view','id'=>$model->id));
            }
        }
    }

}