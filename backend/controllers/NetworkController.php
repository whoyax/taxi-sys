<?php

class NetworkController extends Controller {
    public $modelName = 'Device';

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'wifi', 'admin'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'roles'=>array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('delete'),
                'roles'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    public function actionAdmin()
    {
        Yii::app()->getClientScript()->registerScriptFile('/p/js/functions/network.js', CClientScript::POS_END);

        $params = array(
            'provider' => new Provider('search'),
            'queue' => new Queue('search'),
            'dns' => new DnsSettings('search'),
        );

        if ( isset( $_GET['ajax'] )
            && $_GET['ajax'] == 'queue-grid'
        )
        {
            return $this->renderPartial('/dashboard/_queues', $params);
        }

        if ( isset( $_GET['ajax'] )
            && $_GET['ajax'] == 'dns-grid'
        )
        {
            return $this->renderPartial('/dashboard/_dns', $params);
        }

        $this->render('admin', $params);
    }

    public function actionWifi()
    {

        $this->render('wifi', $params);
    }
}