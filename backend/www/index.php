<?php

header('Content-type: text/html; charset=utf-8');
setlocale(LC_TIME, "ru_RU");

$debugMode = (
    file_exists(dirname(__FILE__) . '/../config/.debug')
    || (!empty($_GET['_dbg_']) && $_GET['_dbg_']==14)
    || (!empty($_COOKIE['_dbg_']) && $_COOKIE['_dbg_']==14)
);

defined('YII_DEBUG') or define('YII_DEBUG', $debugMode);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);


$yii=dirname(__FILE__).'/../../common/lib/vendor/yiisoft/yii/framework/YiiBase.php';
$config=dirname(__FILE__).'/../config/backend.php';

require('./../../common/lib/vendor/autoload.php');
require_once($yii);

class Yii extends YiiBase
{
    /**
     * @static
     * @return CWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}

Yii::createWebApplication($config)->run();