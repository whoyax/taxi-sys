//===== Sparklines =====//

//    $('#router-portload').sparkline(
//        'html', {type: 'bar', barColor: '#ef705b', height: '35px', barWidth: "5px", barSpacing: "2px", zeroAxis: "false"}
//    );



var options = {
    series: {
        lines: { show: true },
        points: { show: false }

    },
    grid: { hoverable: false, clickable: false },
    yaxis: {
        min: 0,
        max: 100,
    },
    xaxis: {
        //show: false,
        mode: 'time',
        minTickSize: [1, "minute"],
//            min: (new Date()).getTime()-5*60*1000,
//            max: (new Date()).getTime()+1*10*1000,
        twelveHourClock: false,
        autoscaleMargin: 0.02,
        position: "bottom",
        timeformat: "%H.%M",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 2,
        axisLabelFontFamily: 'Verdana, Arial'
    },

};

window.mikroPlots = [];
window.mikroPlots[1] = $.plot($("#mikrotik-1-load"), [ [], [] ], options);
window.mikroPlots[2] = $.plot($("#mikrotik-2-load"), [ [], [] ], options);

function prepareMikroData(data) {
    var res = [];
    for (var i = 0; i < data.length; ++i) {
        res.push([parseInt(data[i].stamp), parseInt(data[i].value)])
    }
    return res;
}

var mikroStatsQuery;

window.mikrotikStatsUpdate = function() {

    if(mikroStatsQuery && mikroStatsQuery.readyState != 4){
        mikroStatsQuery.abort();
    }

    mikroStatsQuery = $.ajax({
        'url': '/device/getMicroStats',
        'type': 'get',
        'data': {},
        'cache' : false,
        'success': function(data){
            var mikros = JSON.parse(data);

            mikros.forEach(function(mikr) {
                window.mikroPlots[mikr.id].setData(
                    [{ data: prepareMikroData(mikr.cpu)},
                        { data: prepareMikroData(mikr.mem)}]
                );
                window.mikroPlots[mikr.id].getOptions().xaxis.min = (new Date()).getTime()-5*60*1000;
                window.mikroPlots[mikr.id].getOptions().xaxis.max = (new Date()).getTime()+1*10*1000;
                window.mikroPlots[mikr.id].setupGrid();
//                    window.mikroPlots[mikr.id].triggerRedrawOverlay();
                window.mikroPlots[mikr.id].draw();
                $("#mikrotik-" + mikr.id + "-current").html(mikr.current + "%");

                var portData = [];
                var i=0;
                mikr.ports.forEach(function(port) {
                    portData[i] = [parseFloat(port.rxLoad), -parseFloat(port.txLoad)];
                    i++;
                });

                var portMap = mikr.ports;

                $("#mikrotik-" + mikr.id + "-portload").sparkline(
                    portData,
                    {
                        type: 'bar',
                        barColor: '#ef705b',
                        negBarColor: '#ef705b',
                        height: '50px',
                        barWidth: "10px",
                        barSpacing: "2px",
                        zeroAxis: "true",
                        disabledHiddenCheck: "true",
                        stackedBarColor: ["#91c950", "#ef705b", "#ef705b"],
                        'tooltipFormatter': function(a,b,c) {
                            var port = portMap[c[0].offset];
                            return "<b>" + port.value + "</b><br />" +
                                "<span style='color: #91c950'>rx: <b>" +
                                port.rx + "</b> Kbps</span> (" + port.rxLoad + "%)<br />" +
                                "<span style='color: #ef705b'>tx: <b>" +
                                port.tx + "</b> Kbps</span> (" + port.txLoad + "%)";
                        }
                    }
                );

            });

        },
        'error': function(request, status, error){
            console.log(error);
        }
    });

    setTimeout(mikrotikStatsUpdate, 5000);
}

mikrotikStatsUpdate();
