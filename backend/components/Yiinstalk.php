<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 1:06
 */

class Yiinstalk extends CApplicationComponent
{
    /**
     * Configuration for beanstalkd connections. This is an array containing configurations for
     * Beanstalkd instances that you want to connect to. The array items can have these properties:
     *
     * <ul>
     *   <li>`host`</li>
     *   <li>`port`</li>
     *   <li>`connectTimeout`</li>
     * </ul>
     *
     * Sample value:
     *
     * <code>
     * array(
     *   'default' => array(
     *     'host' => '127.0.0.1',
     *     'port' => 11300,
     *   ),
     *   'secondary' => array(
     *     'host' => '127.0.0.1',
     *     'port' => 11301,
     *   ),
     * ),
     * </code>
     *
     * @var array
     */
    public $connections;
    protected $_clients = array();
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (!is_array($this->connections))
            $this->connections = array();

//        if (!class_exists('Pheanstalk', false))
//            $this->registerAutoloader();
    }
    /**
     * @param string $connectionName
     * @return \Pheanstalk
     */
    public function getClient($connectionName = 'default')
    {
        if (!isset($this->_clients[$connectionName])) {
            if (!array_key_exists($connectionName, $this->connections))
                throw new CException('Invalid connection name.');

            $connection = $this->connections[$connectionName];
            if (!isset($connection['port']))
                $connection['port'] = Pheanstalk\Pheanstalk::DEFAULT_PORT;

            try
            {
                $client = new Pheanstalk\Pheanstalk($connection['host'], $connection['port'],
                    isset($connection['connectTimeout']) ? $connection['connectTimeout'] : null);

            } catch(Pheanstalk\Exception\ConnectionException $e) {
                $client = null;
            }

            $this->_clients[$connectionName] = $client;
        }

        return $this->_clients[$connectionName];
    }

    public function cleanTube($tube) {
        $count = 0;
        try
        {
            while($job = $this->peekReady($tube))
            {
                $this->getClient()->delete($job);
                $count++;
            }

        }
        catch(\Pheanstalk\Exception $e)
        {

        }
        return $count;
    }

    protected function registerAutoloader()
    {
        $classesPath = dirname(__FILE__) . '/../vendors/Pheanstalk/classes';
        require_once($classesPath . '/Pheanstalk/ClassLoader.php');
        Pheanstalk_ClassLoader::register($classesPath);
        // Unregister and register with Yii's autoloader so Yii's autoloader will be the last.
        $autoloader = array('Pheanstalk_ClassLoader', 'load');
        spl_autoload_unregister($autoloader);
        Yii::registerAutoloader($autoloader);
    }
}
