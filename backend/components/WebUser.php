<?php
class WebUser extends CWebUser {

        public function isAdmin() {
            if ($this->id ==='admin') {
                return true;
            }

            if (!empty($this->role) && $this->role ==='admin') {
                return true;
            }

            return false;
        }
}