<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.09.15
 * Time: 9:23
 */
use PEAR2\Net\RouterOS;

class Mikrotik extends CApplicationComponent {

    public $host;

    public $port;

    public $username;

    public $password;

    protected $client = null;

    public function init() {
        try
        {
            $this->client = new RouterOS\Client($this->host, $this->username, $this->password, $this->port);
        }
         catch (Exception $e) {
             throw $e;
            // die('Unable to connect to the router.');
            //Inspect $e if you want to know details about the failure.
        }
    }

    function __call($method, $args)
    {
        return call_user_func_array(array($this->client, $method), $args);
    }

    public static function getPortsRawInfo(Array $allParams, $portsCount = 10)
    {

        $params = array(
            4 => 'name',
            0 => 'device',
        );

        $ports = array();

        for ( $i = 0; $i <= $portsCount; $i++ )
        {
            $ports[$i]['n'] = $i;
            //$ports[$i]['device'] = 0;
            foreach ( $params as $param )
            {
                if(!empty($allParams["ports.{$i}.{$param}"])) {
                    $v = $allParams["ports.{$i}.{$param}"];
                    $ports[$i][$param] = $v->value;
                } elseif($param=='device') {
                    $ports[$i]['device'] = 0;
                }
            }
        }

        return $ports;
    }
} 