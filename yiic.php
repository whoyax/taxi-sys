<?php

setlocale(LC_TIME, "ru_RU");

$debugMode = (
    file_exists(dirname(__FILE__) . '/../config/.debug')
);

defined('YII_DEBUG') or define('YII_DEBUG', $debugMode);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$yii=dirname(__FILE__).'/common/lib/vendor/yiisoft/yii/framework/YiiBase.php';
$config=dirname(__FILE__).'/console/config/console.php';

require('./common/lib/vendor/autoload.php');
require_once($yii);

class Yii extends YiiBase
{
    /**
     * @static
     * @return CWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}


defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
$app = Yii::createConsoleApplication($config);
$app->commandRunner->addCommands('/../cli/commands');
$env = @getenv('YII_CONSOLE_COMMANDS');
if (!empty($env))
    $app->commandRunner->addCommands($env);

$app->run();